<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BerandaController extends Controller
{
    //
    
    public function index() {
    
        $data = [];
        $data['maskapai'] = DB::table('mbandara as a')->select('b.kodeProv','b.namaProv','a.kode','a.nama','a.idNegara')
                ->join('mwilayah as b','a.kodeProv','=','b.kodeProv')
                ->where('a.sta_berangkat_aktif','=','1')
                ->groupBy('b.kodeProv','b.namaProv','a.kode','a.nama','a.idNegara')->get();
        
        $data['jenispaket'] = DB::table('jenispaket')->orderBy('idJenisPaket')->get();
        
        $data['paketTerbaru'] = DB::table('paket')->select('*')
                ->where('paket.staPublish',1)
                ->join('travelagent','travelagent.idTravelAgent','=','paket.idTravelAgent')
                ->offset(0)
                ->limit(5)->orderBy('paket.created_at','desc')->get();
        
        $data['paketJadwal'] = DB::table('paket')->where('staPublish',1)
                ->where('tglBerangkat','>',date('Y-m-d'))
                ->orderBy('created_at','asc')->get();
        
        $data['berita'] = DB::table('content')->where('idContentCategory',2)
                ->where('highlight',1)->where('staPublish',1)->orderBy('created_at','desc')->limit(3)->get();
        $data['testi'] = DB::table('content')->where('idContentCategory',4)
                ->where('highlight',1)->where('staPublish',1)->orderBy('created_at','desc')->limit(3)->get();
        
        $data['travelagent'] = DB::table('travelagent')->orderBy('nama','asc')->where('highlight',1)->limit(10)->get();
        
        return view('public.beranda',$data);
    }
}
