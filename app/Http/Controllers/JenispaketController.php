<?php

namespace App\Http\Controllers;

use App\DataTables\JenispaketDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJenispaketRequest;
use App\Http\Requests\UpdateJenispaketRequest;
use App\Repositories\JenispaketRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class JenispaketController extends AppBaseController
{
    /** @var  JenispaketRepository */
    private $jenispaketRepository;

    public function __construct(JenispaketRepository $jenispaketRepo)
    {
        $this->jenispaketRepository = $jenispaketRepo;
    }

    /**
     * Display a listing of the Jenispaket.
     *
     * @param JenispaketDataTable $jenispaketDataTable
     * @return Response
     */
    public function index(JenispaketDataTable $jenispaketDataTable)
    {
        return $jenispaketDataTable->render('backend.jenispakets.index');
    }

    /**
     * Show the form for creating a new Jenispaket.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.jenispakets.create');
    }

    /**
     * Store a newly created Jenispaket in storage.
     *
     * @param CreateJenispaketRequest $request
     *
     * @return Response
     */
    public function store(CreateJenispaketRequest $request)
    {
        $input = $request->all();

        $jenispaket = $this->jenispaketRepository->create($input);

        Flash::success('Jenispaket saved successfully.');

        return redirect(route('backend.jenispakets.index'));
    }

    /**
     * Display the specified Jenispaket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            Flash::error('Jenispaket not found');

            return redirect(route('backend.jenispakets.index'));
        }

        return view('backend.jenispakets.show')->with('jenispaket', $jenispaket);
    }

    /**
     * Show the form for editing the specified Jenispaket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            Flash::error('Jenispaket not found');

            return redirect(route('backend.jenispakets.index'));
        }

        return view('backend.jenispakets.edit')->with('jenispaket', $jenispaket);
    }

    /**
     * Update the specified Jenispaket in storage.
     *
     * @param  int              $id
     * @param UpdateJenispaketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJenispaketRequest $request)
    {
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            Flash::error('Jenispaket not found');

            return redirect(route('backend.jenispakets.index'));
        }

        $jenispaket = $this->jenispaketRepository->update($request->all(), $id);

        Flash::success('Jenispaket updated successfully.');

        return redirect(route('backend.jenispakets.index'));
    }

    /**
     * Remove the specified Jenispaket from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            Flash::error('Jenispaket not found');

            return redirect(route('backend.jenispakets.index'));
        }

        $this->jenispaketRepository->delete($id);

        Flash::success('Jenispaket deleted successfully.');

        return redirect(route('backend.jenispakets.index'));
    }
}
