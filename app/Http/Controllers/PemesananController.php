<?php

namespace App\Http\Controllers;

use App\DataTables\PemesananDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePemesananRequest;
use App\Http\Requests\UpdatePemesananRequest;
use App\Repositories\PemesananRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PemesananController extends AppBaseController
{
    /** @var  PemesananRepository */
    private $pemesananRepository;

    public function __construct(PemesananRepository $pemesananRepo)
    {
        $this->pemesananRepository = $pemesananRepo;
    }

    /**
     * Display a listing of the Pemesanan.
     *
     * @param PemesananDataTable $pemesananDataTable
     * @return Response
     */
    public function index(PemesananDataTable $pemesananDataTable)
    {
        return $pemesananDataTable->render('backend.pemesanans.index');
    }

    /**
     * Show the form for creating a new Pemesanan.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.pemesanans.create');
    }

    /**
     * Store a newly created Pemesanan in storage.
     *
     * @param CreatePemesananRequest $request
     *
     * @return Response
     */
    public function store(CreatePemesananRequest $request)
    {
        $input = $request->all();

        $pemesanan = $this->pemesananRepository->create($input);

        Flash::success('Pemesanan saved successfully.');

        return redirect(route('backend.pemesanans.index'));
    }

    /**
     * Display the specified Pemesanan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            Flash::error('Pemesanan not found');

            return redirect(route('backend.pemesanans.index'));
        }

        return view('backend.pemesanans.show')->with('pemesanan', $pemesanan);
    }

    /**
     * Show the form for editing the specified Pemesanan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            Flash::error('Pemesanan not found');

            return redirect(route('backend.pemesanans.index'));
        }

        return view('backend.pemesanans.edit')->with('pemesanan', $pemesanan);
    }

    /**
     * Update the specified Pemesanan in storage.
     *
     * @param  int              $id
     * @param UpdatePemesananRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePemesananRequest $request)
    {
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            Flash::error('Pemesanan not found');

            return redirect(route('backend.pemesanans.index'));
        }

        $pemesanan = $this->pemesananRepository->update($request->all(), $id);

        Flash::success('Pemesanan updated successfully.');

        return redirect(route('backend.pemesanans.index'));
    }

    /**
     * Remove the specified Pemesanan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            Flash::error('Pemesanan not found');

            return redirect(route('backend.pemesanans.index'));
        }

        $this->pemesananRepository->delete($id);

        Flash::success('Pemesanan deleted successfully.');

        return redirect(route('backend.pemesanans.index'));
    }
}
