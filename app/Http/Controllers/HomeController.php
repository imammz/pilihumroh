<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Mrole;
use Illuminate\Support\Facades\Auth;
use App\Models\Travelagent;
use App\Models\Paket;
use App\Models\Pemesanan;
use App\Models\Jamaah;
use Illuminate\Support\Facades\Session;
use App\Models\PaketJamaah;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data = [];



        if (Auth::user()->idRole == 1) {
            $data['countTravelAgent'] = Travelagent::All()->count();
            $data['latestTravelAgent'] = Travelagent::limit(12)->get();
            $data['latestPemesanan'] = Pemesanan::orderBy('created_at','DESC')->limit(10)->get();
            $data['countPaket'] = Paket::All()->count();
            $data['countPemesanan'] = Pemesanan::All()->count();
            $data['countJamaah'] = Jamaah::All()->count();
            $data['jadwal'] = Paket::where('tglBerangkat','>',date('Y-m-d'))->get();
        } elseif (Auth::user()->idRole == 3) {
            $data['travel'] = Travelagent::where('idTravelAgent', Session::get('idTravelAgent'))->first();
            $data['latestPemesanan'] = Pemesanan::where('idTravelAgent', Session::get('idTravelAgent'))->orderBy('created_at','DESC')->limit(10)->get();
            $data['countPaket'] = Paket::where('idTravelAgent', Session::get('idTravelAgent'))->count();
            $data['countPemesanan'] = Pemesanan::where('idTravelAgent', Session::get('idTravelAgent'))->count();
            $data['countJamaah'] = PaketJamaah::where('idTravelAgent', Session::get('idTravelAgent'))->count();
            $data['jadwal'] = Paket::where('tglBerangkat','>',date('Y-m-d'))->where('idTravelAgent', Session::get('idTravelAgent'))->get();
        }






        return view('home', $data);
    }

}
