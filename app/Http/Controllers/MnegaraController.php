<?php

namespace App\Http\Controllers;

use App\DataTables\MnegaraDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMnegaraRequest;
use App\Http\Requests\UpdateMnegaraRequest;
use App\Repositories\MnegaraRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MnegaraController extends AppBaseController
{
    /** @var  MnegaraRepository */
    private $mnegaraRepository;

    public function __construct(MnegaraRepository $mnegaraRepo)
    {
        $this->mnegaraRepository = $mnegaraRepo;
    }

    /**
     * Display a listing of the Mnegara.
     *
     * @param MnegaraDataTable $mnegaraDataTable
     * @return Response
     */
    public function index(MnegaraDataTable $mnegaraDataTable)
    {
        return $mnegaraDataTable->render('backend.mnegaras.index');
    }

    /**
     * Show the form for creating a new Mnegara.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mnegaras.create');
    }

    /**
     * Store a newly created Mnegara in storage.
     *
     * @param CreateMnegaraRequest $request
     *
     * @return Response
     */
    public function store(CreateMnegaraRequest $request)
    {
        $input = $request->all();

        $mnegara = $this->mnegaraRepository->create($input);

        Flash::success('Mnegara saved successfully.');

        return redirect(route('backend.mnegaras.index'));
    }

    /**
     * Display the specified Mnegara.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            Flash::error('Mnegara not found');

            return redirect(route('backend.mnegaras.index'));
        }

        return view('backend.mnegaras.show')->with('mnegara', $mnegara);
    }

    /**
     * Show the form for editing the specified Mnegara.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            Flash::error('Mnegara not found');

            return redirect(route('backend.mnegaras.index'));
        }

        return view('backend.mnegaras.edit')->with('mnegara', $mnegara);
    }

    /**
     * Update the specified Mnegara in storage.
     *
     * @param  int              $id
     * @param UpdateMnegaraRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMnegaraRequest $request)
    {
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            Flash::error('Mnegara not found');

            return redirect(route('backend.mnegaras.index'));
        }

        $mnegara = $this->mnegaraRepository->update($request->all(), $id);

        Flash::success('Mnegara updated successfully.');

        return redirect(route('backend.mnegaras.index'));
    }

    /**
     * Remove the specified Mnegara from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            Flash::error('Mnegara not found');

            return redirect(route('backend.mnegaras.index'));
        }

        $this->mnegaraRepository->delete($id);

        Flash::success('Mnegara deleted successfully.');

        return redirect(route('backend.mnegaras.index'));
    }
}
