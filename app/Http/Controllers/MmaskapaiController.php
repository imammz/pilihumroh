<?php

namespace App\Http\Controllers;

use App\DataTables\MmaskapaiDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMmaskapaiRequest;
use App\Http\Requests\UpdateMmaskapaiRequest;
use App\Repositories\MmaskapaiRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MmaskapaiController extends AppBaseController
{
    /** @var  MmaskapaiRepository */
    private $mmaskapaiRepository;

    public function __construct(MmaskapaiRepository $mmaskapaiRepo)
    {
        $this->mmaskapaiRepository = $mmaskapaiRepo;
    }

    /**
     * Display a listing of the Mmaskapai.
     *
     * @param MmaskapaiDataTable $mmaskapaiDataTable
     * @return Response
     */
    public function index(MmaskapaiDataTable $mmaskapaiDataTable)
    {
        return $mmaskapaiDataTable->render('backend.mmaskapais.index');
    }

    /**
     * Show the form for creating a new Mmaskapai.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mmaskapais.create');
    }

    /**
     * Store a newly created Mmaskapai in storage.
     *
     * @param CreateMmaskapaiRequest $request
     *
     * @return Response
     */
    public function store(CreateMmaskapaiRequest $request)
    {
        $input = $request->all();

        $mmaskapai = $this->mmaskapaiRepository->create($input);

        Flash::success('Mmaskapai saved successfully.');

        return redirect(route('backend.mmaskapais.index'));
    }

    /**
     * Display the specified Mmaskapai.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            Flash::error('Mmaskapai not found');

            return redirect(route('backend.mmaskapais.index'));
        }

        return view('backend.mmaskapais.show')->with('mmaskapai', $mmaskapai);
    }

    /**
     * Show the form for editing the specified Mmaskapai.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            Flash::error('Mmaskapai not found');

            return redirect(route('backend.mmaskapais.index'));
        }

        return view('backend.mmaskapais.edit')->with('mmaskapai', $mmaskapai);
    }

    /**
     * Update the specified Mmaskapai in storage.
     *
     * @param  int              $id
     * @param UpdateMmaskapaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMmaskapaiRequest $request)
    {
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            Flash::error('Mmaskapai not found');

            return redirect(route('backend.mmaskapais.index'));
        }

        $mmaskapai = $this->mmaskapaiRepository->update($request->all(), $id);

        Flash::success('Mmaskapai updated successfully.');

        return redirect(route('backend.mmaskapais.index'));
    }

    /**
     * Remove the specified Mmaskapai from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            Flash::error('Mmaskapai not found');

            return redirect(route('backend.mmaskapais.index'));
        }

        $this->mmaskapaiRepository->delete($id);

        Flash::success('Mmaskapai deleted successfully.');

        return redirect(route('backend.mmaskapais.index'));
    }
}
