<?php

namespace App\Http\Controllers;

use App\DataTables\MstatuspemesananDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMstatuspemesananRequest;
use App\Http\Requests\UpdateMstatuspemesananRequest;
use App\Repositories\MstatuspemesananRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MstatuspemesananController extends AppBaseController
{
    /** @var  MstatuspemesananRepository */
    private $mstatuspemesananRepository;

    public function __construct(MstatuspemesananRepository $mstatuspemesananRepo)
    {
        $this->mstatuspemesananRepository = $mstatuspemesananRepo;
    }

    /**
     * Display a listing of the Mstatuspemesanan.
     *
     * @param MstatuspemesananDataTable $mstatuspemesananDataTable
     * @return Response
     */
    public function index(MstatuspemesananDataTable $mstatuspemesananDataTable)
    {
        return $mstatuspemesananDataTable->render('backend.mstatuspemesanans.index');
    }

    /**
     * Show the form for creating a new Mstatuspemesanan.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mstatuspemesanans.create');
    }

    /**
     * Store a newly created Mstatuspemesanan in storage.
     *
     * @param CreateMstatuspemesananRequest $request
     *
     * @return Response
     */
    public function store(CreateMstatuspemesananRequest $request)
    {
        $input = $request->all();

        $mstatuspemesanan = $this->mstatuspemesananRepository->create($input);

        Flash::success('Mstatuspemesanan saved successfully.');

        return redirect(route('backend.mstatuspemesanans.index'));
    }

    /**
     * Display the specified Mstatuspemesanan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            Flash::error('Mstatuspemesanan not found');

            return redirect(route('backend.mstatuspemesanans.index'));
        }

        return view('backend.mstatuspemesanans.show')->with('mstatuspemesanan', $mstatuspemesanan);
    }

    /**
     * Show the form for editing the specified Mstatuspemesanan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            Flash::error('Mstatuspemesanan not found');

            return redirect(route('backend.mstatuspemesanans.index'));
        }

        return view('backend.mstatuspemesanans.edit')->with('mstatuspemesanan', $mstatuspemesanan);
    }

    /**
     * Update the specified Mstatuspemesanan in storage.
     *
     * @param  int              $id
     * @param UpdateMstatuspemesananRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMstatuspemesananRequest $request)
    {
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            Flash::error('Mstatuspemesanan not found');

            return redirect(route('backend.mstatuspemesanans.index'));
        }

        $mstatuspemesanan = $this->mstatuspemesananRepository->update($request->all(), $id);

        Flash::success('Mstatuspemesanan updated successfully.');

        return redirect(route('backend.mstatuspemesanans.index'));
    }

    /**
     * Remove the specified Mstatuspemesanan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            Flash::error('Mstatuspemesanan not found');

            return redirect(route('backend.mstatuspemesanans.index'));
        }

        $this->mstatuspemesananRepository->delete($id);

        Flash::success('Mstatuspemesanan deleted successfully.');

        return redirect(route('backend.mstatuspemesanans.index'));
    }
}
