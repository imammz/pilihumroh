<?php

namespace App\Http\Controllers;

use App\DataTables\MpembimbingDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMpembimbingRequest;
use App\Http\Requests\UpdateMpembimbingRequest;
use App\Repositories\MpembimbingRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MpembimbingController extends AppBaseController
{
    /** @var  MpembimbingRepository */
    private $mpembimbingRepository;

    public function __construct(MpembimbingRepository $mpembimbingRepo)
    {
        $this->mpembimbingRepository = $mpembimbingRepo;
    }

    /**
     * Display a listing of the Mpembimbing.
     *
     * @param MpembimbingDataTable $mpembimbingDataTable
     * @return Response
     */
    public function index(MpembimbingDataTable $mpembimbingDataTable)
    {
        return $mpembimbingDataTable->render('backend.mpembimbings.index');
    }

    /**
     * Show the form for creating a new Mpembimbing.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mpembimbings.create');
    }

    /**
     * Store a newly created Mpembimbing in storage.
     *
     * @param CreateMpembimbingRequest $request
     *
     * @return Response
     */
    public function store(CreateMpembimbingRequest $request)
    {
        $input = $request->all();

        $mpembimbing = $this->mpembimbingRepository->create($input);

        Flash::success('Mpembimbing saved successfully.');

        return redirect(route('backend.mpembimbings.index'));
    }

    /**
     * Display the specified Mpembimbing.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            Flash::error('Mpembimbing not found');

            return redirect(route('backend.mpembimbings.index'));
        }

        return view('backend.mpembimbings.show')->with('mpembimbing', $mpembimbing);
    }

    /**
     * Show the form for editing the specified Mpembimbing.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            Flash::error('Mpembimbing not found');

            return redirect(route('backend.mpembimbings.index'));
        }

        return view('backend.mpembimbings.edit')->with('mpembimbing', $mpembimbing);
    }

    /**
     * Update the specified Mpembimbing in storage.
     *
     * @param  int              $id
     * @param UpdateMpembimbingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMpembimbingRequest $request)
    {
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            Flash::error('Mpembimbing not found');

            return redirect(route('backend.mpembimbings.index'));
        }

        $mpembimbing = $this->mpembimbingRepository->update($request->all(), $id);

        Flash::success('Mpembimbing updated successfully.');

        return redirect(route('backend.mpembimbings.index'));
    }

    /**
     * Remove the specified Mpembimbing from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            Flash::error('Mpembimbing not found');

            return redirect(route('backend.mpembimbings.index'));
        }

        $this->mpembimbingRepository->delete($id);

        Flash::success('Mpembimbing deleted successfully.');

        return redirect(route('backend.mpembimbings.index'));
    }
}
