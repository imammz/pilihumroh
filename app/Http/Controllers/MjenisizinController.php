<?php

namespace App\Http\Controllers;

use App\DataTables\MjenisizinDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMjenisizinRequest;
use App\Http\Requests\UpdateMjenisizinRequest;
use App\Repositories\MjenisizinRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MjenisizinController extends AppBaseController
{
    /** @var  MjenisizinRepository */
    private $mjenisizinRepository;

    public function __construct(MjenisizinRepository $mjenisizinRepo)
    {
        $this->mjenisizinRepository = $mjenisizinRepo;
    }

    /**
     * Display a listing of the Mjenisizin.
     *
     * @param MjenisizinDataTable $mjenisizinDataTable
     * @return Response
     */
    public function index(MjenisizinDataTable $mjenisizinDataTable)
    {
        return $mjenisizinDataTable->render('backend.mjenisizins.index');
    }

    /**
     * Show the form for creating a new Mjenisizin.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mjenisizins.create');
    }

    /**
     * Store a newly created Mjenisizin in storage.
     *
     * @param CreateMjenisizinRequest $request
     *
     * @return Response
     */
    public function store(CreateMjenisizinRequest $request)
    {
        $input = $request->all();

        $mjenisizin = $this->mjenisizinRepository->create($input);

        Flash::success('Mjenisizin saved successfully.');

        return redirect(route('backend.mjenisizins.index'));
    }

    /**
     * Display the specified Mjenisizin.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            Flash::error('Mjenisizin not found');

            return redirect(route('backend.mjenisizins.index'));
        }

        return view('backend.mjenisizins.show')->with('mjenisizin', $mjenisizin);
    }

    /**
     * Show the form for editing the specified Mjenisizin.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            Flash::error('Mjenisizin not found');

            return redirect(route('backend.mjenisizins.index'));
        }

        return view('backend.mjenisizins.edit')->with('mjenisizin', $mjenisizin);
    }

    /**
     * Update the specified Mjenisizin in storage.
     *
     * @param  int              $id
     * @param UpdateMjenisizinRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMjenisizinRequest $request)
    {
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            Flash::error('Mjenisizin not found');

            return redirect(route('backend.mjenisizins.index'));
        }

        $mjenisizin = $this->mjenisizinRepository->update($request->all(), $id);

        Flash::success('Mjenisizin updated successfully.');

        return redirect(route('backend.mjenisizins.index'));
    }

    /**
     * Remove the specified Mjenisizin from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            Flash::error('Mjenisizin not found');

            return redirect(route('backend.mjenisizins.index'));
        }

        $this->mjenisizinRepository->delete($id);

        Flash::success('Mjenisizin deleted successfully.');

        return redirect(route('backend.mjenisizins.index'));
    }
}
