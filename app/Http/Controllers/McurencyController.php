<?php

namespace App\Http\Controllers;

use App\DataTables\McurencyDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMcurencyRequest;
use App\Http\Requests\UpdateMcurencyRequest;
use App\Repositories\McurencyRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class McurencyController extends AppBaseController
{
    /** @var  McurencyRepository */
    private $mcurencyRepository;

    public function __construct(McurencyRepository $mcurencyRepo)
    {
        $this->mcurencyRepository = $mcurencyRepo;
    }

    /**
     * Display a listing of the Mcurency.
     *
     * @param McurencyDataTable $mcurencyDataTable
     * @return Response
     */
    public function index(McurencyDataTable $mcurencyDataTable)
    {
        return $mcurencyDataTable->render('backend.mcurencies.index');
    }

    /**
     * Show the form for creating a new Mcurency.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mcurencies.create');
    }

    /**
     * Store a newly created Mcurency in storage.
     *
     * @param CreateMcurencyRequest $request
     *
     * @return Response
     */
    public function store(CreateMcurencyRequest $request)
    {
        $input = $request->all();

        $mcurency = $this->mcurencyRepository->create($input);

        Flash::success('Mcurency saved successfully.');

        return redirect(route('backend.mcurencies.index'));
    }

    /**
     * Display the specified Mcurency.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            Flash::error('Mcurency not found');

            return redirect(route('backend.mcurencies.index'));
        }

        return view('backend.mcurencies.show')->with('mcurency', $mcurency);
    }

    /**
     * Show the form for editing the specified Mcurency.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            Flash::error('Mcurency not found');

            return redirect(route('backend.mcurencies.index'));
        }

        return view('backend.mcurencies.edit')->with('mcurency', $mcurency);
    }

    /**
     * Update the specified Mcurency in storage.
     *
     * @param  int              $id
     * @param UpdateMcurencyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMcurencyRequest $request)
    {
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            Flash::error('Mcurency not found');

            return redirect(route('backend.mcurencies.index'));
        }

        $mcurency = $this->mcurencyRepository->update($request->all(), $id);

        Flash::success('Mcurency updated successfully.');

        return redirect(route('backend.mcurencies.index'));
    }

    /**
     * Remove the specified Mcurency from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            Flash::error('Mcurency not found');

            return redirect(route('backend.mcurencies.index'));
        }

        $this->mcurencyRepository->delete($id);

        Flash::success('Mcurency deleted successfully.');

        return redirect(route('backend.mcurencies.index'));
    }
}
