<?php

namespace App\Http\Controllers;

use App\DataTables\MbandaraDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMbandaraRequest;
use App\Http\Requests\UpdateMbandaraRequest;
use App\Repositories\MbandaraRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MbandaraController extends AppBaseController
{
    /** @var  MbandaraRepository */
    private $mbandaraRepository;

    public function __construct(MbandaraRepository $mbandaraRepo)
    {
        $this->mbandaraRepository = $mbandaraRepo;
    }

    /**
     * Display a listing of the Mbandara.
     *
     * @param MbandaraDataTable $mbandaraDataTable
     * @return Response
     */
    public function index(MbandaraDataTable $mbandaraDataTable)
    {
        return $mbandaraDataTable->render('backend.mbandaras.index');
    }

    /**
     * Show the form for creating a new Mbandara.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mbandaras.create');
    }

    /**
     * Store a newly created Mbandara in storage.
     *
     * @param CreateMbandaraRequest $request
     *
     * @return Response
     */
    public function store(CreateMbandaraRequest $request)
    {
        $input = $request->all();

        $mbandara = $this->mbandaraRepository->create($input);

        Flash::success('Mbandara saved successfully.');

        return redirect(route('backend.mbandaras.index'));
    }

    /**
     * Display the specified Mbandara.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            Flash::error('Mbandara not found');

            return redirect(route('backend.mbandaras.index'));
        }

        return view('backend.mbandaras.show')->with('mbandara', $mbandara);
    }

    /**
     * Show the form for editing the specified Mbandara.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            Flash::error('Mbandara not found');

            return redirect(route('backend.mbandaras.index'));
        }

        return view('backend.mbandaras.edit')->with('mbandara', $mbandara);
    }

    /**
     * Update the specified Mbandara in storage.
     *
     * @param  int              $id
     * @param UpdateMbandaraRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMbandaraRequest $request)
    {
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            Flash::error('Mbandara not found');

            return redirect(route('backend.mbandaras.index'));
        }

        $mbandara = $this->mbandaraRepository->update($request->all(), $id);

        Flash::success('Mbandara updated successfully.');

        return redirect(route('backend.mbandaras.index'));
    }

    /**
     * Remove the specified Mbandara from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            Flash::error('Mbandara not found');

            return redirect(route('backend.mbandaras.index'));
        }

        $this->mbandaraRepository->delete($id);

        Flash::success('Mbandara deleted successfully.');

        return redirect(route('backend.mbandaras.index'));
    }
}
