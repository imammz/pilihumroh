<?php

namespace App\Http\Controllers;

use App\DataTables\TravelagentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTravelagentRequest;
use App\Http\Requests\UpdateTravelagentRequest;
use App\Repositories\TravelagentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TravelagentController extends AppBaseController
{
    /** @var  TravelagentRepository */
    private $travelagentRepository;

    public function __construct(TravelagentRepository $travelagentRepo)
    {
        $this->travelagentRepository = $travelagentRepo;
    }

    /**
     * Display a listing of the Travelagent.
     *
     * @param TravelagentDataTable $travelagentDataTable
     * @return Response
     */
    public function index(TravelagentDataTable $travelagentDataTable)
    {
        return $travelagentDataTable->render('backend.travelagents.index');
    }

    /**
     * Show the form for creating a new Travelagent.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.travelagents.create');
    }

    /**
     * Store a newly created Travelagent in storage.
     *
     * @param CreateTravelagentRequest $request
     *
     * @return Response
     */
    public function store(CreateTravelagentRequest $request)
    {
        $input = $request->all();

        $travelagent = $this->travelagentRepository->create($input);

        Flash::success('Travelagent saved successfully.');

        return redirect(route('backend.travelagents.index'));
    }

    /**
     * Display the specified Travelagent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            Flash::error('Travelagent not found');

            return redirect(route('backend.travelagents.index'));
        }

        return view('backend.travelagents.show')->with('travelagent', $travelagent);
    }

    /**
     * Show the form for editing the specified Travelagent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            Flash::error('Travelagent not found');

            return redirect(route('backend.travelagents.index'));
        }

        return view('backend.travelagents.edit')->with('travelagent', $travelagent);
    }

    /**
     * Update the specified Travelagent in storage.
     *
     * @param  int              $id
     * @param UpdateTravelagentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTravelagentRequest $request)
    {
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            Flash::error('Travelagent not found');

            return redirect(route('backend.travelagents.index'));
        }

        $travelagent = $this->travelagentRepository->update($request->all(), $id);

        Flash::success('Travelagent updated successfully.');

        return redirect(route('backend.travelagents.index'));
    }

    /**
     * Remove the specified Travelagent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            Flash::error('Travelagent not found');

            return redirect(route('backend.travelagents.index'));
        }

        $this->travelagentRepository->delete($id);

        Flash::success('Travelagent deleted successfully.');

        return redirect(route('backend.travelagents.index'));
    }
}
