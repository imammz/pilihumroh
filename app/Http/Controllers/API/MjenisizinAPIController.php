<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMjenisizinAPIRequest;
use App\Http\Requests\API\UpdateMjenisizinAPIRequest;
use App\Models\Mjenisizin;
use App\Repositories\MjenisizinRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MjenisizinController
 * @package App\Http\Controllers\API
 */

class MjenisizinAPIController extends AppBaseController
{
    /** @var  MjenisizinRepository */
    private $mjenisizinRepository;

    public function __construct(MjenisizinRepository $mjenisizinRepo)
    {
        $this->mjenisizinRepository = $mjenisizinRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mjenisizins",
     *      summary="Get a listing of the Mjenisizins.",
     *      tags={"Mjenisizin"},
     *      description="Get all Mjenisizins",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mjenisizin")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mjenisizinRepository->pushCriteria(new RequestCriteria($request));
        $this->mjenisizinRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mjenisizins = $this->mjenisizinRepository->all();

        return $this->sendResponse($mjenisizins->toArray(), 'Mjenisizins retrieved successfully');
    }

    /**
     * @param CreateMjenisizinAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mjenisizins",
     *      summary="Store a newly created Mjenisizin in storage",
     *      tags={"Mjenisizin"},
     *      description="Store Mjenisizin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mjenisizin that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mjenisizin")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mjenisizin"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMjenisizinAPIRequest $request)
    {
        $input = $request->all();

        $mjenisizins = $this->mjenisizinRepository->create($input);

        return $this->sendResponse($mjenisizins->toArray(), 'Mjenisizin saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mjenisizins/{id}",
     *      summary="Display the specified Mjenisizin",
     *      tags={"Mjenisizin"},
     *      description="Get Mjenisizin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mjenisizin",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mjenisizin"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mjenisizin $mjenisizin */
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            return $this->sendError('Mjenisizin not found');
        }

        return $this->sendResponse($mjenisizin->toArray(), 'Mjenisizin retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMjenisizinAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mjenisizins/{id}",
     *      summary="Update the specified Mjenisizin in storage",
     *      tags={"Mjenisizin"},
     *      description="Update Mjenisizin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mjenisizin",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mjenisizin that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mjenisizin")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mjenisizin"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMjenisizinAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mjenisizin $mjenisizin */
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            return $this->sendError('Mjenisizin not found');
        }

        $mjenisizin = $this->mjenisizinRepository->update($input, $id);

        return $this->sendResponse($mjenisizin->toArray(), 'Mjenisizin updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mjenisizins/{id}",
     *      summary="Remove the specified Mjenisizin from storage",
     *      tags={"Mjenisizin"},
     *      description="Delete Mjenisizin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mjenisizin",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mjenisizin $mjenisizin */
        $mjenisizin = $this->mjenisizinRepository->findWithoutFail($id);

        if (empty($mjenisizin)) {
            return $this->sendError('Mjenisizin not found');
        }

        $mjenisizin->delete();

        return $this->sendResponse($id, 'Mjenisizin deleted successfully');
    }
}
