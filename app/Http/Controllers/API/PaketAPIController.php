<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaketAPIRequest;
use App\Http\Requests\API\UpdatePaketAPIRequest;
use App\Models\Paket;
use App\Repositories\PaketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PaketController
 * @package App\Http\Controllers\API
 */

class PaketAPIController extends AppBaseController
{
    /** @var  PaketRepository */
    private $paketRepository;

    public function __construct(PaketRepository $paketRepo)
    {
        $this->paketRepository = $paketRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/pakets",
     *      summary="Get a listing of the Pakets.",
     *      tags={"Paket"},
     *      description="Get all Pakets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Paket")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->paketRepository->pushCriteria(new RequestCriteria($request));
        $this->paketRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pakets = $this->paketRepository->all();

        return $this->sendResponse($pakets->toArray(), 'Pakets retrieved successfully');
    }

    /**
     * @param CreatePaketAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/pakets",
     *      summary="Store a newly created Paket in storage",
     *      tags={"Paket"},
     *      description="Store Paket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Paket that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Paket")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Paket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePaketAPIRequest $request)
    {
        $input = $request->all();

        $pakets = $this->paketRepository->create($input);

        return $this->sendResponse($pakets->toArray(), 'Paket saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/pakets/{id}",
     *      summary="Display the specified Paket",
     *      tags={"Paket"},
     *      description="Get Paket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Paket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Paket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Paket $paket */
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            return $this->sendError('Paket not found');
        }

        return $this->sendResponse($paket->toArray(), 'Paket retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePaketAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/pakets/{id}",
     *      summary="Update the specified Paket in storage",
     *      tags={"Paket"},
     *      description="Update Paket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Paket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Paket that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Paket")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Paket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePaketAPIRequest $request)
    {
        $input = $request->all();

        /** @var Paket $paket */
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            return $this->sendError('Paket not found');
        }

        $paket = $this->paketRepository->update($input, $id);

        return $this->sendResponse($paket->toArray(), 'Paket updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/pakets/{id}",
     *      summary="Remove the specified Paket from storage",
     *      tags={"Paket"},
     *      description="Delete Paket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Paket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Paket $paket */
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            return $this->sendError('Paket not found');
        }

        $paket->delete();

        return $this->sendResponse($id, 'Paket deleted successfully');
    }
}
