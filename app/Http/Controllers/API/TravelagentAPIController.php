<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTravelagentAPIRequest;
use App\Http\Requests\API\UpdateTravelagentAPIRequest;
use App\Models\Travelagent;
use App\Repositories\TravelagentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TravelagentController
 * @package App\Http\Controllers\API
 */

class TravelagentAPIController extends AppBaseController
{
    /** @var  TravelagentRepository */
    private $travelagentRepository;

    public function __construct(TravelagentRepository $travelagentRepo)
    {
        $this->travelagentRepository = $travelagentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/travelagents",
     *      summary="Get a listing of the Travelagents.",
     *      tags={"Travelagent"},
     *      description="Get all Travelagents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Travelagent")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->travelagentRepository->pushCriteria(new RequestCriteria($request));
        $this->travelagentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $travelagents = $this->travelagentRepository->all();

        return $this->sendResponse($travelagents->toArray(), 'Travelagents retrieved successfully');
    }

    /**
     * @param CreateTravelagentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/travelagents",
     *      summary="Store a newly created Travelagent in storage",
     *      tags={"Travelagent"},
     *      description="Store Travelagent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Travelagent that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Travelagent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Travelagent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTravelagentAPIRequest $request)
    {
        $input = $request->all();

        $travelagents = $this->travelagentRepository->create($input);

        return $this->sendResponse($travelagents->toArray(), 'Travelagent saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/travelagents/{id}",
     *      summary="Display the specified Travelagent",
     *      tags={"Travelagent"},
     *      description="Get Travelagent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Travelagent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Travelagent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Travelagent $travelagent */
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            return $this->sendError('Travelagent not found');
        }

        return $this->sendResponse($travelagent->toArray(), 'Travelagent retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTravelagentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/travelagents/{id}",
     *      summary="Update the specified Travelagent in storage",
     *      tags={"Travelagent"},
     *      description="Update Travelagent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Travelagent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Travelagent that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Travelagent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Travelagent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTravelagentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Travelagent $travelagent */
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            return $this->sendError('Travelagent not found');
        }

        $travelagent = $this->travelagentRepository->update($input, $id);

        return $this->sendResponse($travelagent->toArray(), 'Travelagent updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/travelagents/{id}",
     *      summary="Remove the specified Travelagent from storage",
     *      tags={"Travelagent"},
     *      description="Delete Travelagent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Travelagent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Travelagent $travelagent */
        $travelagent = $this->travelagentRepository->findWithoutFail($id);

        if (empty($travelagent)) {
            return $this->sendError('Travelagent not found');
        }

        $travelagent->delete();

        return $this->sendResponse($id, 'Travelagent deleted successfully');
    }
}
