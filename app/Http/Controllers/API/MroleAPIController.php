<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMroleAPIRequest;
use App\Http\Requests\API\UpdateMroleAPIRequest;
use App\Models\Mrole;
use App\Repositories\MroleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MroleController
 * @package App\Http\Controllers\API
 */

class MroleAPIController extends AppBaseController
{
    /** @var  MroleRepository */
    private $mroleRepository;

    public function __construct(MroleRepository $mroleRepo)
    {
        $this->mroleRepository = $mroleRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mroles",
     *      summary="Get a listing of the Mroles.",
     *      tags={"Mrole"},
     *      description="Get all Mroles",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mrole")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mroleRepository->pushCriteria(new RequestCriteria($request));
        $this->mroleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mroles = $this->mroleRepository->all();

        return $this->sendResponse($mroles->toArray(), 'Mroles retrieved successfully');
    }

    /**
     * @param CreateMroleAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mroles",
     *      summary="Store a newly created Mrole in storage",
     *      tags={"Mrole"},
     *      description="Store Mrole",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mrole that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mrole")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mrole"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMroleAPIRequest $request)
    {
        $input = $request->all();

        $mroles = $this->mroleRepository->create($input);

        return $this->sendResponse($mroles->toArray(), 'Mrole saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mroles/{id}",
     *      summary="Display the specified Mrole",
     *      tags={"Mrole"},
     *      description="Get Mrole",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mrole",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mrole"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mrole $mrole */
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            return $this->sendError('Mrole not found');
        }

        return $this->sendResponse($mrole->toArray(), 'Mrole retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMroleAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mroles/{id}",
     *      summary="Update the specified Mrole in storage",
     *      tags={"Mrole"},
     *      description="Update Mrole",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mrole",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mrole that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mrole")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mrole"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMroleAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mrole $mrole */
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            return $this->sendError('Mrole not found');
        }

        $mrole = $this->mroleRepository->update($input, $id);

        return $this->sendResponse($mrole->toArray(), 'Mrole updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mroles/{id}",
     *      summary="Remove the specified Mrole from storage",
     *      tags={"Mrole"},
     *      description="Delete Mrole",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mrole",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mrole $mrole */
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            return $this->sendError('Mrole not found');
        }

        $mrole->delete();

        return $this->sendResponse($id, 'Mrole deleted successfully');
    }
}
