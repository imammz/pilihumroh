<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMstatuspemesananAPIRequest;
use App\Http\Requests\API\UpdateMstatuspemesananAPIRequest;
use App\Models\Mstatuspemesanan;
use App\Repositories\MstatuspemesananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MstatuspemesananController
 * @package App\Http\Controllers\API
 */

class MstatuspemesananAPIController extends AppBaseController
{
    /** @var  MstatuspemesananRepository */
    private $mstatuspemesananRepository;

    public function __construct(MstatuspemesananRepository $mstatuspemesananRepo)
    {
        $this->mstatuspemesananRepository = $mstatuspemesananRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mstatuspemesanans",
     *      summary="Get a listing of the Mstatuspemesanans.",
     *      tags={"Mstatuspemesanan"},
     *      description="Get all Mstatuspemesanans",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mstatuspemesanan")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mstatuspemesananRepository->pushCriteria(new RequestCriteria($request));
        $this->mstatuspemesananRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mstatuspemesanans = $this->mstatuspemesananRepository->all();

        return $this->sendResponse($mstatuspemesanans->toArray(), 'Mstatuspemesanans retrieved successfully');
    }

    /**
     * @param CreateMstatuspemesananAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mstatuspemesanans",
     *      summary="Store a newly created Mstatuspemesanan in storage",
     *      tags={"Mstatuspemesanan"},
     *      description="Store Mstatuspemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mstatuspemesanan that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mstatuspemesanan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mstatuspemesanan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMstatuspemesananAPIRequest $request)
    {
        $input = $request->all();

        $mstatuspemesanans = $this->mstatuspemesananRepository->create($input);

        return $this->sendResponse($mstatuspemesanans->toArray(), 'Mstatuspemesanan saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mstatuspemesanans/{id}",
     *      summary="Display the specified Mstatuspemesanan",
     *      tags={"Mstatuspemesanan"},
     *      description="Get Mstatuspemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mstatuspemesanan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mstatuspemesanan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mstatuspemesanan $mstatuspemesanan */
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            return $this->sendError('Mstatuspemesanan not found');
        }

        return $this->sendResponse($mstatuspemesanan->toArray(), 'Mstatuspemesanan retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMstatuspemesananAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mstatuspemesanans/{id}",
     *      summary="Update the specified Mstatuspemesanan in storage",
     *      tags={"Mstatuspemesanan"},
     *      description="Update Mstatuspemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mstatuspemesanan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mstatuspemesanan that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mstatuspemesanan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mstatuspemesanan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMstatuspemesananAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mstatuspemesanan $mstatuspemesanan */
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            return $this->sendError('Mstatuspemesanan not found');
        }

        $mstatuspemesanan = $this->mstatuspemesananRepository->update($input, $id);

        return $this->sendResponse($mstatuspemesanan->toArray(), 'Mstatuspemesanan updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mstatuspemesanans/{id}",
     *      summary="Remove the specified Mstatuspemesanan from storage",
     *      tags={"Mstatuspemesanan"},
     *      description="Delete Mstatuspemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mstatuspemesanan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mstatuspemesanan $mstatuspemesanan */
        $mstatuspemesanan = $this->mstatuspemesananRepository->findWithoutFail($id);

        if (empty($mstatuspemesanan)) {
            return $this->sendError('Mstatuspemesanan not found');
        }

        $mstatuspemesanan->delete();

        return $this->sendResponse($id, 'Mstatuspemesanan deleted successfully');
    }
}
