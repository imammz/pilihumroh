<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMcontentcategoryAPIRequest;
use App\Http\Requests\API\UpdateMcontentcategoryAPIRequest;
use App\Models\Mcontentcategory;
use App\Repositories\McontentcategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class McontentcategoryController
 * @package App\Http\Controllers\API
 */

class McontentcategoryAPIController extends AppBaseController
{
    /** @var  McontentcategoryRepository */
    private $mcontentcategoryRepository;

    public function __construct(McontentcategoryRepository $mcontentcategoryRepo)
    {
        $this->mcontentcategoryRepository = $mcontentcategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mcontentcategories",
     *      summary="Get a listing of the Mcontentcategories.",
     *      tags={"Mcontentcategory"},
     *      description="Get all Mcontentcategories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mcontentcategory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mcontentcategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->mcontentcategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mcontentcategories = $this->mcontentcategoryRepository->all();

        return $this->sendResponse($mcontentcategories->toArray(), 'Mcontentcategories retrieved successfully');
    }

    /**
     * @param CreateMcontentcategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mcontentcategories",
     *      summary="Store a newly created Mcontentcategory in storage",
     *      tags={"Mcontentcategory"},
     *      description="Store Mcontentcategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mcontentcategory that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mcontentcategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mcontentcategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMcontentcategoryAPIRequest $request)
    {
        $input = $request->all();

        $mcontentcategories = $this->mcontentcategoryRepository->create($input);

        return $this->sendResponse($mcontentcategories->toArray(), 'Mcontentcategory saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mcontentcategories/{id}",
     *      summary="Display the specified Mcontentcategory",
     *      tags={"Mcontentcategory"},
     *      description="Get Mcontentcategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mcontentcategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mcontentcategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mcontentcategory $mcontentcategory */
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            return $this->sendError('Mcontentcategory not found');
        }

        return $this->sendResponse($mcontentcategory->toArray(), 'Mcontentcategory retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMcontentcategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mcontentcategories/{id}",
     *      summary="Update the specified Mcontentcategory in storage",
     *      tags={"Mcontentcategory"},
     *      description="Update Mcontentcategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mcontentcategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mcontentcategory that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mcontentcategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mcontentcategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMcontentcategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mcontentcategory $mcontentcategory */
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            return $this->sendError('Mcontentcategory not found');
        }

        $mcontentcategory = $this->mcontentcategoryRepository->update($input, $id);

        return $this->sendResponse($mcontentcategory->toArray(), 'Mcontentcategory updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mcontentcategories/{id}",
     *      summary="Remove the specified Mcontentcategory from storage",
     *      tags={"Mcontentcategory"},
     *      description="Delete Mcontentcategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mcontentcategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mcontentcategory $mcontentcategory */
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            return $this->sendError('Mcontentcategory not found');
        }

        $mcontentcategory->delete();

        return $this->sendResponse($id, 'Mcontentcategory deleted successfully');
    }
}
