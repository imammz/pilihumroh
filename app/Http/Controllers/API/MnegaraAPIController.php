<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMnegaraAPIRequest;
use App\Http\Requests\API\UpdateMnegaraAPIRequest;
use App\Models\Mnegara;
use App\Repositories\MnegaraRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MnegaraController
 * @package App\Http\Controllers\API
 */

class MnegaraAPIController extends AppBaseController
{
    /** @var  MnegaraRepository */
    private $mnegaraRepository;

    public function __construct(MnegaraRepository $mnegaraRepo)
    {
        $this->mnegaraRepository = $mnegaraRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mnegaras",
     *      summary="Get a listing of the Mnegaras.",
     *      tags={"Mnegara"},
     *      description="Get all Mnegaras",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mnegara")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mnegaraRepository->pushCriteria(new RequestCriteria($request));
        $this->mnegaraRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mnegaras = $this->mnegaraRepository->all();

        return $this->sendResponse($mnegaras->toArray(), 'Mnegaras retrieved successfully');
    }

    /**
     * @param CreateMnegaraAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mnegaras",
     *      summary="Store a newly created Mnegara in storage",
     *      tags={"Mnegara"},
     *      description="Store Mnegara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mnegara that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mnegara")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mnegara"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMnegaraAPIRequest $request)
    {
        $input = $request->all();

        $mnegaras = $this->mnegaraRepository->create($input);

        return $this->sendResponse($mnegaras->toArray(), 'Mnegara saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mnegaras/{id}",
     *      summary="Display the specified Mnegara",
     *      tags={"Mnegara"},
     *      description="Get Mnegara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mnegara",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mnegara"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mnegara $mnegara */
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            return $this->sendError('Mnegara not found');
        }

        return $this->sendResponse($mnegara->toArray(), 'Mnegara retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMnegaraAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mnegaras/{id}",
     *      summary="Update the specified Mnegara in storage",
     *      tags={"Mnegara"},
     *      description="Update Mnegara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mnegara",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mnegara that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mnegara")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mnegara"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMnegaraAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mnegara $mnegara */
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            return $this->sendError('Mnegara not found');
        }

        $mnegara = $this->mnegaraRepository->update($input, $id);

        return $this->sendResponse($mnegara->toArray(), 'Mnegara updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mnegaras/{id}",
     *      summary="Remove the specified Mnegara from storage",
     *      tags={"Mnegara"},
     *      description="Delete Mnegara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mnegara",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mnegara $mnegara */
        $mnegara = $this->mnegaraRepository->findWithoutFail($id);

        if (empty($mnegara)) {
            return $this->sendError('Mnegara not found');
        }

        $mnegara->delete();

        return $this->sendResponse($id, 'Mnegara deleted successfully');
    }
}
