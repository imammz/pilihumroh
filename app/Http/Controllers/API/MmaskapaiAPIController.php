<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMmaskapaiAPIRequest;
use App\Http\Requests\API\UpdateMmaskapaiAPIRequest;
use App\Models\Mmaskapai;
use App\Repositories\MmaskapaiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MmaskapaiController
 * @package App\Http\Controllers\API
 */

class MmaskapaiAPIController extends AppBaseController
{
    /** @var  MmaskapaiRepository */
    private $mmaskapaiRepository;

    public function __construct(MmaskapaiRepository $mmaskapaiRepo)
    {
        $this->mmaskapaiRepository = $mmaskapaiRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mmaskapais",
     *      summary="Get a listing of the Mmaskapais.",
     *      tags={"Mmaskapai"},
     *      description="Get all Mmaskapais",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mmaskapai")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mmaskapaiRepository->pushCriteria(new RequestCriteria($request));
        $this->mmaskapaiRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mmaskapais = $this->mmaskapaiRepository->all();

        return $this->sendResponse($mmaskapais->toArray(), 'Mmaskapais retrieved successfully');
    }

    /**
     * @param CreateMmaskapaiAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mmaskapais",
     *      summary="Store a newly created Mmaskapai in storage",
     *      tags={"Mmaskapai"},
     *      description="Store Mmaskapai",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mmaskapai that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mmaskapai")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mmaskapai"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMmaskapaiAPIRequest $request)
    {
        $input = $request->all();

        $mmaskapais = $this->mmaskapaiRepository->create($input);

        return $this->sendResponse($mmaskapais->toArray(), 'Mmaskapai saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mmaskapais/{id}",
     *      summary="Display the specified Mmaskapai",
     *      tags={"Mmaskapai"},
     *      description="Get Mmaskapai",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mmaskapai",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mmaskapai"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mmaskapai $mmaskapai */
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            return $this->sendError('Mmaskapai not found');
        }

        return $this->sendResponse($mmaskapai->toArray(), 'Mmaskapai retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMmaskapaiAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mmaskapais/{id}",
     *      summary="Update the specified Mmaskapai in storage",
     *      tags={"Mmaskapai"},
     *      description="Update Mmaskapai",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mmaskapai",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mmaskapai that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mmaskapai")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mmaskapai"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMmaskapaiAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mmaskapai $mmaskapai */
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            return $this->sendError('Mmaskapai not found');
        }

        $mmaskapai = $this->mmaskapaiRepository->update($input, $id);

        return $this->sendResponse($mmaskapai->toArray(), 'Mmaskapai updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mmaskapais/{id}",
     *      summary="Remove the specified Mmaskapai from storage",
     *      tags={"Mmaskapai"},
     *      description="Delete Mmaskapai",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mmaskapai",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mmaskapai $mmaskapai */
        $mmaskapai = $this->mmaskapaiRepository->findWithoutFail($id);

        if (empty($mmaskapai)) {
            return $this->sendError('Mmaskapai not found');
        }

        $mmaskapai->delete();

        return $this->sendResponse($id, 'Mmaskapai deleted successfully');
    }
}
