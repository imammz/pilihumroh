<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePemesananAPIRequest;
use App\Http\Requests\API\UpdatePemesananAPIRequest;
use App\Models\Pemesanan;
use App\Repositories\PemesananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PemesananController
 * @package App\Http\Controllers\API
 */

class PemesananAPIController extends AppBaseController
{
    /** @var  PemesananRepository */
    private $pemesananRepository;

    public function __construct(PemesananRepository $pemesananRepo)
    {
        $this->pemesananRepository = $pemesananRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/pemesanans",
     *      summary="Get a listing of the Pemesanans.",
     *      tags={"Pemesanan"},
     *      description="Get all Pemesanans",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Pemesanan")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->pemesananRepository->pushCriteria(new RequestCriteria($request));
        $this->pemesananRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pemesanans = $this->pemesananRepository->all();

        return $this->sendResponse($pemesanans->toArray(), 'Pemesanans retrieved successfully');
    }

    /**
     * @param CreatePemesananAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/pemesanans",
     *      summary="Store a newly created Pemesanan in storage",
     *      tags={"Pemesanan"},
     *      description="Store Pemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pemesanan that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pemesanan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pemesanan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePemesananAPIRequest $request)
    {
        $input = $request->all();

        $pemesanans = $this->pemesananRepository->create($input);

        return $this->sendResponse($pemesanans->toArray(), 'Pemesanan saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/pemesanans/{id}",
     *      summary="Display the specified Pemesanan",
     *      tags={"Pemesanan"},
     *      description="Get Pemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pemesanan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pemesanan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Pemesanan $pemesanan */
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            return $this->sendError('Pemesanan not found');
        }

        return $this->sendResponse($pemesanan->toArray(), 'Pemesanan retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePemesananAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/pemesanans/{id}",
     *      summary="Update the specified Pemesanan in storage",
     *      tags={"Pemesanan"},
     *      description="Update Pemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pemesanan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pemesanan that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pemesanan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pemesanan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePemesananAPIRequest $request)
    {
        $input = $request->all();

        /** @var Pemesanan $pemesanan */
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            return $this->sendError('Pemesanan not found');
        }

        $pemesanan = $this->pemesananRepository->update($input, $id);

        return $this->sendResponse($pemesanan->toArray(), 'Pemesanan updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/pemesanans/{id}",
     *      summary="Remove the specified Pemesanan from storage",
     *      tags={"Pemesanan"},
     *      description="Delete Pemesanan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pemesanan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Pemesanan $pemesanan */
        $pemesanan = $this->pemesananRepository->findWithoutFail($id);

        if (empty($pemesanan)) {
            return $this->sendError('Pemesanan not found');
        }

        $pemesanan->delete();

        return $this->sendResponse($id, 'Pemesanan deleted successfully');
    }
}
