<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMhotelAPIRequest;
use App\Http\Requests\API\UpdateMhotelAPIRequest;
use App\Models\Mhotel;
use App\Repositories\MhotelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MhotelController
 * @package App\Http\Controllers\API
 */

class MhotelAPIController extends AppBaseController
{
    /** @var  MhotelRepository */
    private $mhotelRepository;

    public function __construct(MhotelRepository $mhotelRepo)
    {
        $this->mhotelRepository = $mhotelRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mhotels",
     *      summary="Get a listing of the Mhotels.",
     *      tags={"Mhotel"},
     *      description="Get all Mhotels",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mhotel")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mhotelRepository->pushCriteria(new RequestCriteria($request));
        $this->mhotelRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mhotels = $this->mhotelRepository->all();

        return $this->sendResponse($mhotels->toArray(), 'Mhotels retrieved successfully');
    }

    /**
     * @param CreateMhotelAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mhotels",
     *      summary="Store a newly created Mhotel in storage",
     *      tags={"Mhotel"},
     *      description="Store Mhotel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mhotel that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mhotel")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mhotel"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMhotelAPIRequest $request)
    {
        $input = $request->all();

        $mhotels = $this->mhotelRepository->create($input);

        return $this->sendResponse($mhotels->toArray(), 'Mhotel saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mhotels/{id}",
     *      summary="Display the specified Mhotel",
     *      tags={"Mhotel"},
     *      description="Get Mhotel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mhotel",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mhotel"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mhotel $mhotel */
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            return $this->sendError('Mhotel not found');
        }

        return $this->sendResponse($mhotel->toArray(), 'Mhotel retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMhotelAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mhotels/{id}",
     *      summary="Update the specified Mhotel in storage",
     *      tags={"Mhotel"},
     *      description="Update Mhotel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mhotel",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mhotel that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mhotel")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mhotel"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMhotelAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mhotel $mhotel */
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            return $this->sendError('Mhotel not found');
        }

        $mhotel = $this->mhotelRepository->update($input, $id);

        return $this->sendResponse($mhotel->toArray(), 'Mhotel updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mhotels/{id}",
     *      summary="Remove the specified Mhotel from storage",
     *      tags={"Mhotel"},
     *      description="Delete Mhotel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mhotel",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mhotel $mhotel */
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            return $this->sendError('Mhotel not found');
        }

        $mhotel->delete();

        return $this->sendResponse($id, 'Mhotel deleted successfully');
    }
}
