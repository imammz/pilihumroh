<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMcurencyAPIRequest;
use App\Http\Requests\API\UpdateMcurencyAPIRequest;
use App\Models\Mcurency;
use App\Repositories\McurencyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class McurencyController
 * @package App\Http\Controllers\API
 */

class McurencyAPIController extends AppBaseController
{
    /** @var  McurencyRepository */
    private $mcurencyRepository;

    public function __construct(McurencyRepository $mcurencyRepo)
    {
        $this->mcurencyRepository = $mcurencyRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mcurencies",
     *      summary="Get a listing of the Mcurencies.",
     *      tags={"Mcurency"},
     *      description="Get all Mcurencies",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mcurency")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mcurencyRepository->pushCriteria(new RequestCriteria($request));
        $this->mcurencyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mcurencies = $this->mcurencyRepository->all();

        return $this->sendResponse($mcurencies->toArray(), 'Mcurencies retrieved successfully');
    }

    /**
     * @param CreateMcurencyAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mcurencies",
     *      summary="Store a newly created Mcurency in storage",
     *      tags={"Mcurency"},
     *      description="Store Mcurency",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mcurency that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mcurency")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mcurency"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMcurencyAPIRequest $request)
    {
        $input = $request->all();

        $mcurencies = $this->mcurencyRepository->create($input);

        return $this->sendResponse($mcurencies->toArray(), 'Mcurency saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mcurencies/{id}",
     *      summary="Display the specified Mcurency",
     *      tags={"Mcurency"},
     *      description="Get Mcurency",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mcurency",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mcurency"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mcurency $mcurency */
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            return $this->sendError('Mcurency not found');
        }

        return $this->sendResponse($mcurency->toArray(), 'Mcurency retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMcurencyAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mcurencies/{id}",
     *      summary="Update the specified Mcurency in storage",
     *      tags={"Mcurency"},
     *      description="Update Mcurency",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mcurency",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mcurency that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mcurency")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mcurency"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMcurencyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mcurency $mcurency */
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            return $this->sendError('Mcurency not found');
        }

        $mcurency = $this->mcurencyRepository->update($input, $id);

        return $this->sendResponse($mcurency->toArray(), 'Mcurency updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mcurencies/{id}",
     *      summary="Remove the specified Mcurency from storage",
     *      tags={"Mcurency"},
     *      description="Delete Mcurency",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mcurency",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mcurency $mcurency */
        $mcurency = $this->mcurencyRepository->findWithoutFail($id);

        if (empty($mcurency)) {
            return $this->sendError('Mcurency not found');
        }

        $mcurency->delete();

        return $this->sendResponse($id, 'Mcurency deleted successfully');
    }
}
