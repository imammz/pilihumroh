<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMpembimbingAPIRequest;
use App\Http\Requests\API\UpdateMpembimbingAPIRequest;
use App\Models\Mpembimbing;
use App\Repositories\MpembimbingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MpembimbingController
 * @package App\Http\Controllers\API
 */

class MpembimbingAPIController extends AppBaseController
{
    /** @var  MpembimbingRepository */
    private $mpembimbingRepository;

    public function __construct(MpembimbingRepository $mpembimbingRepo)
    {
        $this->mpembimbingRepository = $mpembimbingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mpembimbings",
     *      summary="Get a listing of the Mpembimbings.",
     *      tags={"Mpembimbing"},
     *      description="Get all Mpembimbings",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mpembimbing")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mpembimbingRepository->pushCriteria(new RequestCriteria($request));
        $this->mpembimbingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mpembimbings = $this->mpembimbingRepository->all();

        return $this->sendResponse($mpembimbings->toArray(), 'Mpembimbings retrieved successfully');
    }

    /**
     * @param CreateMpembimbingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mpembimbings",
     *      summary="Store a newly created Mpembimbing in storage",
     *      tags={"Mpembimbing"},
     *      description="Store Mpembimbing",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mpembimbing that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mpembimbing")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mpembimbing"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMpembimbingAPIRequest $request)
    {
        $input = $request->all();

        $mpembimbings = $this->mpembimbingRepository->create($input);

        return $this->sendResponse($mpembimbings->toArray(), 'Mpembimbing saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mpembimbings/{id}",
     *      summary="Display the specified Mpembimbing",
     *      tags={"Mpembimbing"},
     *      description="Get Mpembimbing",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mpembimbing",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mpembimbing"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mpembimbing $mpembimbing */
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            return $this->sendError('Mpembimbing not found');
        }

        return $this->sendResponse($mpembimbing->toArray(), 'Mpembimbing retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMpembimbingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mpembimbings/{id}",
     *      summary="Update the specified Mpembimbing in storage",
     *      tags={"Mpembimbing"},
     *      description="Update Mpembimbing",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mpembimbing",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mpembimbing that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mpembimbing")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mpembimbing"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMpembimbingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mpembimbing $mpembimbing */
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            return $this->sendError('Mpembimbing not found');
        }

        $mpembimbing = $this->mpembimbingRepository->update($input, $id);

        return $this->sendResponse($mpembimbing->toArray(), 'Mpembimbing updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mpembimbings/{id}",
     *      summary="Remove the specified Mpembimbing from storage",
     *      tags={"Mpembimbing"},
     *      description="Delete Mpembimbing",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mpembimbing",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mpembimbing $mpembimbing */
        $mpembimbing = $this->mpembimbingRepository->findWithoutFail($id);

        if (empty($mpembimbing)) {
            return $this->sendError('Mpembimbing not found');
        }

        $mpembimbing->delete();

        return $this->sendResponse($id, 'Mpembimbing deleted successfully');
    }
}
