<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateJenispaketAPIRequest;
use App\Http\Requests\API\UpdateJenispaketAPIRequest;
use App\Models\Jenispaket;
use App\Repositories\JenispaketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class JenispaketController
 * @package App\Http\Controllers\API
 */

class JenispaketAPIController extends AppBaseController
{
    /** @var  JenispaketRepository */
    private $jenispaketRepository;

    public function __construct(JenispaketRepository $jenispaketRepo)
    {
        $this->jenispaketRepository = $jenispaketRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/jenispakets",
     *      summary="Get a listing of the Jenispakets.",
     *      tags={"Jenispaket"},
     *      description="Get all Jenispakets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Jenispaket")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->jenispaketRepository->pushCriteria(new RequestCriteria($request));
        $this->jenispaketRepository->pushCriteria(new LimitOffsetCriteria($request));
        $jenispakets = $this->jenispaketRepository->all();

        return $this->sendResponse($jenispakets->toArray(), 'Jenispakets retrieved successfully');
    }

    /**
     * @param CreateJenispaketAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/jenispakets",
     *      summary="Store a newly created Jenispaket in storage",
     *      tags={"Jenispaket"},
     *      description="Store Jenispaket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Jenispaket that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Jenispaket")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Jenispaket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateJenispaketAPIRequest $request)
    {
        $input = $request->all();

        $jenispakets = $this->jenispaketRepository->create($input);

        return $this->sendResponse($jenispakets->toArray(), 'Jenispaket saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/jenispakets/{id}",
     *      summary="Display the specified Jenispaket",
     *      tags={"Jenispaket"},
     *      description="Get Jenispaket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Jenispaket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Jenispaket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Jenispaket $jenispaket */
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            return $this->sendError('Jenispaket not found');
        }

        return $this->sendResponse($jenispaket->toArray(), 'Jenispaket retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateJenispaketAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/jenispakets/{id}",
     *      summary="Update the specified Jenispaket in storage",
     *      tags={"Jenispaket"},
     *      description="Update Jenispaket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Jenispaket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Jenispaket that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Jenispaket")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Jenispaket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateJenispaketAPIRequest $request)
    {
        $input = $request->all();

        /** @var Jenispaket $jenispaket */
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            return $this->sendError('Jenispaket not found');
        }

        $jenispaket = $this->jenispaketRepository->update($input, $id);

        return $this->sendResponse($jenispaket->toArray(), 'Jenispaket updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/jenispakets/{id}",
     *      summary="Remove the specified Jenispaket from storage",
     *      tags={"Jenispaket"},
     *      description="Delete Jenispaket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Jenispaket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Jenispaket $jenispaket */
        $jenispaket = $this->jenispaketRepository->findWithoutFail($id);

        if (empty($jenispaket)) {
            return $this->sendError('Jenispaket not found');
        }

        $jenispaket->delete();

        return $this->sendResponse($id, 'Jenispaket deleted successfully');
    }
}
