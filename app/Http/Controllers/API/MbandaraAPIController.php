<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMbandaraAPIRequest;
use App\Http\Requests\API\UpdateMbandaraAPIRequest;
use App\Models\Mbandara;
use App\Repositories\MbandaraRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MbandaraController
 * @package App\Http\Controllers\API
 */

class MbandaraAPIController extends AppBaseController
{
    /** @var  MbandaraRepository */
    private $mbandaraRepository;

    public function __construct(MbandaraRepository $mbandaraRepo)
    {
        $this->mbandaraRepository = $mbandaraRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mbandaras",
     *      summary="Get a listing of the Mbandaras.",
     *      tags={"Mbandara"},
     *      description="Get all Mbandaras",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mbandara")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mbandaraRepository->pushCriteria(new RequestCriteria($request));
        $this->mbandaraRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mbandaras = $this->mbandaraRepository->all();

        return $this->sendResponse($mbandaras->toArray(), 'Mbandaras retrieved successfully');
    }

    /**
     * @param CreateMbandaraAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mbandaras",
     *      summary="Store a newly created Mbandara in storage",
     *      tags={"Mbandara"},
     *      description="Store Mbandara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mbandara that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mbandara")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mbandara"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMbandaraAPIRequest $request)
    {
        $input = $request->all();

        $mbandaras = $this->mbandaraRepository->create($input);

        return $this->sendResponse($mbandaras->toArray(), 'Mbandara saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mbandaras/{id}",
     *      summary="Display the specified Mbandara",
     *      tags={"Mbandara"},
     *      description="Get Mbandara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mbandara",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mbandara"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mbandara $mbandara */
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            return $this->sendError('Mbandara not found');
        }

        return $this->sendResponse($mbandara->toArray(), 'Mbandara retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMbandaraAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mbandaras/{id}",
     *      summary="Update the specified Mbandara in storage",
     *      tags={"Mbandara"},
     *      description="Update Mbandara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mbandara",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mbandara that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mbandara")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mbandara"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMbandaraAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mbandara $mbandara */
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            return $this->sendError('Mbandara not found');
        }

        $mbandara = $this->mbandaraRepository->update($input, $id);

        return $this->sendResponse($mbandara->toArray(), 'Mbandara updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mbandaras/{id}",
     *      summary="Remove the specified Mbandara from storage",
     *      tags={"Mbandara"},
     *      description="Delete Mbandara",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mbandara",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mbandara $mbandara */
        $mbandara = $this->mbandaraRepository->findWithoutFail($id);

        if (empty($mbandara)) {
            return $this->sendError('Mbandara not found');
        }

        $mbandara->delete();

        return $this->sendResponse($id, 'Mbandara deleted successfully');
    }
}
