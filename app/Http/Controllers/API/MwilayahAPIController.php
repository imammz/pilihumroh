<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMwilayahAPIRequest;
use App\Http\Requests\API\UpdateMwilayahAPIRequest;
use App\Models\Mwilayah;
use App\Repositories\MwilayahRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MwilayahController
 * @package App\Http\Controllers\API
 */

class MwilayahAPIController extends AppBaseController
{
    /** @var  MwilayahRepository */
    private $mwilayahRepository;

    public function __construct(MwilayahRepository $mwilayahRepo)
    {
        $this->mwilayahRepository = $mwilayahRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mwilayahs",
     *      summary="Get a listing of the Mwilayahs.",
     *      tags={"Mwilayah"},
     *      description="Get all Mwilayahs",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mwilayah")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->mwilayahRepository->pushCriteria(new RequestCriteria($request));
        $this->mwilayahRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mwilayahs = $this->mwilayahRepository->all();

        return $this->sendResponse($mwilayahs->toArray(), 'Mwilayahs retrieved successfully');
    }

    /**
     * @param CreateMwilayahAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mwilayahs",
     *      summary="Store a newly created Mwilayah in storage",
     *      tags={"Mwilayah"},
     *      description="Store Mwilayah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mwilayah that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mwilayah")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mwilayah"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMwilayahAPIRequest $request)
    {
        $input = $request->all();

        $mwilayahs = $this->mwilayahRepository->create($input);

        return $this->sendResponse($mwilayahs->toArray(), 'Mwilayah saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mwilayahs/{id}",
     *      summary="Display the specified Mwilayah",
     *      tags={"Mwilayah"},
     *      description="Get Mwilayah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mwilayah",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mwilayah"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mwilayah $mwilayah */
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            return $this->sendError('Mwilayah not found');
        }

        return $this->sendResponse($mwilayah->toArray(), 'Mwilayah retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMwilayahAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mwilayahs/{id}",
     *      summary="Update the specified Mwilayah in storage",
     *      tags={"Mwilayah"},
     *      description="Update Mwilayah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mwilayah",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mwilayah that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mwilayah")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mwilayah"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMwilayahAPIRequest $request)
    {
        $input = $request->all();

        /** @var Mwilayah $mwilayah */
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            return $this->sendError('Mwilayah not found');
        }

        $mwilayah = $this->mwilayahRepository->update($input, $id);

        return $this->sendResponse($mwilayah->toArray(), 'Mwilayah updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mwilayahs/{id}",
     *      summary="Remove the specified Mwilayah from storage",
     *      tags={"Mwilayah"},
     *      description="Delete Mwilayah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mwilayah",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mwilayah $mwilayah */
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            return $this->sendError('Mwilayah not found');
        }

        $mwilayah->delete();

        return $this->sendResponse($id, 'Mwilayah deleted successfully');
    }
}
