<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateJamaahAPIRequest;
use App\Http\Requests\API\UpdateJamaahAPIRequest;
use App\Models\Jamaah;
use App\Repositories\JamaahRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class JamaahController
 * @package App\Http\Controllers\API
 */

class JamaahAPIController extends AppBaseController
{
    /** @var  JamaahRepository */
    private $jamaahRepository;

    public function __construct(JamaahRepository $jamaahRepo)
    {
        $this->jamaahRepository = $jamaahRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/jamaahs",
     *      summary="Get a listing of the Jamaahs.",
     *      tags={"Jamaah"},
     *      description="Get all Jamaahs",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Jamaah")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->jamaahRepository->pushCriteria(new RequestCriteria($request));
        $this->jamaahRepository->pushCriteria(new LimitOffsetCriteria($request));
        $jamaahs = $this->jamaahRepository->all();

        return $this->sendResponse($jamaahs->toArray(), 'Jamaahs retrieved successfully');
    }

    /**
     * @param CreateJamaahAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/jamaahs",
     *      summary="Store a newly created Jamaah in storage",
     *      tags={"Jamaah"},
     *      description="Store Jamaah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Jamaah that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Jamaah")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Jamaah"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateJamaahAPIRequest $request)
    {
        $input = $request->all();

        $jamaahs = $this->jamaahRepository->create($input);

        return $this->sendResponse($jamaahs->toArray(), 'Jamaah saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/jamaahs/{id}",
     *      summary="Display the specified Jamaah",
     *      tags={"Jamaah"},
     *      description="Get Jamaah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Jamaah",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Jamaah"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Jamaah $jamaah */
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            return $this->sendError('Jamaah not found');
        }

        return $this->sendResponse($jamaah->toArray(), 'Jamaah retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateJamaahAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/jamaahs/{id}",
     *      summary="Update the specified Jamaah in storage",
     *      tags={"Jamaah"},
     *      description="Update Jamaah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Jamaah",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Jamaah that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Jamaah")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Jamaah"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateJamaahAPIRequest $request)
    {
        $input = $request->all();

        /** @var Jamaah $jamaah */
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            return $this->sendError('Jamaah not found');
        }

        $jamaah = $this->jamaahRepository->update($input, $id);

        return $this->sendResponse($jamaah->toArray(), 'Jamaah updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/jamaahs/{id}",
     *      summary="Remove the specified Jamaah from storage",
     *      tags={"Jamaah"},
     *      description="Delete Jamaah",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Jamaah",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Jamaah $jamaah */
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            return $this->sendError('Jamaah not found');
        }

        $jamaah->delete();

        return $this->sendResponse($id, 'Jamaah deleted successfully');
    }
}
