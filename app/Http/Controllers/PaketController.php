<?php

namespace App\Http\Controllers;

use App\DataTables\PaketDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaketRequest;
use App\Http\Requests\UpdatePaketRequest;
use App\Repositories\PaketRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PaketController extends AppBaseController
{
    /** @var  PaketRepository */
    private $paketRepository;

    public function __construct(PaketRepository $paketRepo)
    {
        $this->paketRepository = $paketRepo;
    }

    /**
     * Display a listing of the Paket.
     *
     * @param PaketDataTable $paketDataTable
     * @return Response
     */
    public function index(PaketDataTable $paketDataTable)
    {
        return $paketDataTable->render('backend.pakets.index');
    }

    /**
     * Show the form for creating a new Paket.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.pakets.create');
    }

    /**
     * Store a newly created Paket in storage.
     *
     * @param CreatePaketRequest $request
     *
     * @return Response
     */
    public function store(CreatePaketRequest $request)
    {
        $input = $request->all();

        $paket = $this->paketRepository->create($input);

        Flash::success('Paket saved successfully.');

        return redirect(route('backend.pakets.index'));
    }

    /**
     * Display the specified Paket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            Flash::error('Paket not found');

            return redirect(route('backend.pakets.index'));
        }

        return view('backend.pakets.show')->with('paket', $paket);
    }

    /**
     * Show the form for editing the specified Paket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            Flash::error('Paket not found');

            return redirect(route('backend.pakets.index'));
        }

        return view('backend.pakets.edit')->with('paket', $paket);
    }

    /**
     * Update the specified Paket in storage.
     *
     * @param  int              $id
     * @param UpdatePaketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaketRequest $request)
    {
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            Flash::error('Paket not found');

            return redirect(route('backend.pakets.index'));
        }

        $paket = $this->paketRepository->update($request->all(), $id);

        Flash::success('Paket updated successfully.');

        return redirect(route('backend.pakets.index'));
    }

    /**
     * Remove the specified Paket from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paket = $this->paketRepository->findWithoutFail($id);

        if (empty($paket)) {
            Flash::error('Paket not found');

            return redirect(route('backend.pakets.index'));
        }

        $this->paketRepository->delete($id);

        Flash::success('Paket deleted successfully.');

        return redirect(route('backend.pakets.index'));
    }
}
