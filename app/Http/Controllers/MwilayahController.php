<?php

namespace App\Http\Controllers;

use App\DataTables\MwilayahDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMwilayahRequest;
use App\Http\Requests\UpdateMwilayahRequest;
use App\Repositories\MwilayahRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MwilayahController extends AppBaseController
{
    /** @var  MwilayahRepository */
    private $mwilayahRepository;

    public function __construct(MwilayahRepository $mwilayahRepo)
    {
        $this->mwilayahRepository = $mwilayahRepo;
    }

    /**
     * Display a listing of the Mwilayah.
     *
     * @param MwilayahDataTable $mwilayahDataTable
     * @return Response
     */
    public function index(MwilayahDataTable $mwilayahDataTable)
    {
        return $mwilayahDataTable->render('backend.mwilayahs.index');
    }

    /**
     * Show the form for creating a new Mwilayah.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mwilayahs.create');
    }

    /**
     * Store a newly created Mwilayah in storage.
     *
     * @param CreateMwilayahRequest $request
     *
     * @return Response
     */
    public function store(CreateMwilayahRequest $request)
    {
        $input = $request->all();

        $mwilayah = $this->mwilayahRepository->create($input);

        Flash::success('Mwilayah saved successfully.');

        return redirect(route('backend.mwilayahs.index'));
    }

    /**
     * Display the specified Mwilayah.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            Flash::error('Mwilayah not found');

            return redirect(route('backend.mwilayahs.index'));
        }

        return view('backend.mwilayahs.show')->with('mwilayah', $mwilayah);
    }

    /**
     * Show the form for editing the specified Mwilayah.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            Flash::error('Mwilayah not found');

            return redirect(route('backend.mwilayahs.index'));
        }

        return view('backend.mwilayahs.edit')->with('mwilayah', $mwilayah);
    }

    /**
     * Update the specified Mwilayah in storage.
     *
     * @param  int              $id
     * @param UpdateMwilayahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMwilayahRequest $request)
    {
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            Flash::error('Mwilayah not found');

            return redirect(route('backend.mwilayahs.index'));
        }

        $mwilayah = $this->mwilayahRepository->update($request->all(), $id);

        Flash::success('Mwilayah updated successfully.');

        return redirect(route('backend.mwilayahs.index'));
    }

    /**
     * Remove the specified Mwilayah from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mwilayah = $this->mwilayahRepository->findWithoutFail($id);

        if (empty($mwilayah)) {
            Flash::error('Mwilayah not found');

            return redirect(route('backend.mwilayahs.index'));
        }

        $this->mwilayahRepository->delete($id);

        Flash::success('Mwilayah deleted successfully.');

        return redirect(route('backend.mwilayahs.index'));
    }
}
