<?php

namespace App\Http\Controllers;

use App\DataTables\McontentcategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMcontentcategoryRequest;
use App\Http\Requests\UpdateMcontentcategoryRequest;
use App\Repositories\McontentcategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class McontentcategoryController extends AppBaseController
{
    /** @var  McontentcategoryRepository */
    private $mcontentcategoryRepository;

    public function __construct(McontentcategoryRepository $mcontentcategoryRepo)
    {
        $this->mcontentcategoryRepository = $mcontentcategoryRepo;
    }

    /**
     * Display a listing of the Mcontentcategory.
     *
     * @param McontentcategoryDataTable $mcontentcategoryDataTable
     * @return Response
     */
    public function index(McontentcategoryDataTable $mcontentcategoryDataTable)
    {
        return $mcontentcategoryDataTable->render('backend.mcontentcategories.index');
    }

    /**
     * Show the form for creating a new Mcontentcategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mcontentcategories.create');
    }

    /**
     * Store a newly created Mcontentcategory in storage.
     *
     * @param CreateMcontentcategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateMcontentcategoryRequest $request)
    {
        $input = $request->all();

        $mcontentcategory = $this->mcontentcategoryRepository->create($input);

        Flash::success('Mcontentcategory saved successfully.');

        return redirect(route('backend.mcontentcategories.index'));
    }

    /**
     * Display the specified Mcontentcategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            Flash::error('Mcontentcategory not found');

            return redirect(route('backend.mcontentcategories.index'));
        }

        return view('backend.mcontentcategories.show')->with('mcontentcategory', $mcontentcategory);
    }

    /**
     * Show the form for editing the specified Mcontentcategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            Flash::error('Mcontentcategory not found');

            return redirect(route('backend.mcontentcategories.index'));
        }

        return view('backend.mcontentcategories.edit')->with('mcontentcategory', $mcontentcategory);
    }

    /**
     * Update the specified Mcontentcategory in storage.
     *
     * @param  int              $id
     * @param UpdateMcontentcategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMcontentcategoryRequest $request)
    {
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            Flash::error('Mcontentcategory not found');

            return redirect(route('backend.mcontentcategories.index'));
        }

        $mcontentcategory = $this->mcontentcategoryRepository->update($request->all(), $id);

        Flash::success('Mcontentcategory updated successfully.');

        return redirect(route('backend.mcontentcategories.index'));
    }

    /**
     * Remove the specified Mcontentcategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mcontentcategory = $this->mcontentcategoryRepository->findWithoutFail($id);

        if (empty($mcontentcategory)) {
            Flash::error('Mcontentcategory not found');

            return redirect(route('backend.mcontentcategories.index'));
        }

        $this->mcontentcategoryRepository->delete($id);

        Flash::success('Mcontentcategory deleted successfully.');

        return redirect(route('backend.mcontentcategories.index'));
    }
}
