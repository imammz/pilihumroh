<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends Controller
{
    
    public function about() {
        
        $data = array();
        $data['title'] = 'Tentang Pilih Umroh';
        
        return view('public.about',$data);
        
    }
    
    public function faq() {
        
        $data = array();
        $data['title'] = 'FAQ';
        
        return view('public.faq',$data);
        
    }

    public function hubungikami() {
        
        $data = array();
        $data['title'] = 'Hubungi Kami';
        
        return view('public.contact',$data);
        
    }

    public function carapesan() {
        
        $data = array();
        $data['title'] = 'Cara Pemesanan';
        
        return view('public.carapesan',$data);
        
    }

    public function syarat() {
        
        $data = array();
        $data['title'] = 'Syarat & Ketentuan';
        
        return view('public.syarat',$data);
        
    }
    
    public function testi() {
        
        $data = array();
        $data['title'] = 'Apa Kata Mereka Tentang Pilih Umroh';
        
        return view('public.testi',$data);
        
    }
    
}
