<?php

namespace App\Http\Controllers;

use App\DataTables\MroleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMroleRequest;
use App\Http\Requests\UpdateMroleRequest;
use App\Repositories\MroleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MroleController extends AppBaseController
{
    /** @var  MroleRepository */
    private $mroleRepository;

    public function __construct(MroleRepository $mroleRepo)
    {
        $this->mroleRepository = $mroleRepo;
    }

    /**
     * Display a listing of the Mrole.
     *
     * @param MroleDataTable $mroleDataTable
     * @return Response
     */
    public function index(MroleDataTable $mroleDataTable)
    {
        return $mroleDataTable->render('backend.mroles.index');
    }

    /**
     * Show the form for creating a new Mrole.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mroles.create');
    }

    /**
     * Store a newly created Mrole in storage.
     *
     * @param CreateMroleRequest $request
     *
     * @return Response
     */
    public function store(CreateMroleRequest $request)
    {
        $input = $request->all();

        $mrole = $this->mroleRepository->create($input);

        Flash::success('Mrole saved successfully.');

        return redirect(route('backend.mroles.index'));
    }

    /**
     * Display the specified Mrole.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            Flash::error('Mrole not found');

            return redirect(route('backend.mroles.index'));
        }

        return view('backend.mroles.show')->with('mrole', $mrole);
    }

    /**
     * Show the form for editing the specified Mrole.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            Flash::error('Mrole not found');

            return redirect(route('backend.mroles.index'));
        }

        return view('backend.mroles.edit')->with('mrole', $mrole);
    }

    /**
     * Update the specified Mrole in storage.
     *
     * @param  int              $id
     * @param UpdateMroleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMroleRequest $request)
    {
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            Flash::error('Mrole not found');

            return redirect(route('backend.mroles.index'));
        }

        $mrole = $this->mroleRepository->update($request->all(), $id);

        Flash::success('Mrole updated successfully.');

        return redirect(route('backend.mroles.index'));
    }

    /**
     * Remove the specified Mrole from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mrole = $this->mroleRepository->findWithoutFail($id);

        if (empty($mrole)) {
            Flash::error('Mrole not found');

            return redirect(route('backend.mroles.index'));
        }

        $this->mroleRepository->delete($id);

        Flash::success('Mrole deleted successfully.');

        return redirect(route('backend.mroles.index'));
    }
}
