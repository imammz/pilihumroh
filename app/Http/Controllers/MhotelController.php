<?php

namespace App\Http\Controllers;

use App\DataTables\MhotelDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMhotelRequest;
use App\Http\Requests\UpdateMhotelRequest;
use App\Repositories\MhotelRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MhotelController extends AppBaseController
{
    /** @var  MhotelRepository */
    private $mhotelRepository;

    public function __construct(MhotelRepository $mhotelRepo)
    {
        $this->mhotelRepository = $mhotelRepo;
    }

    /**
     * Display a listing of the Mhotel.
     *
     * @param MhotelDataTable $mhotelDataTable
     * @return Response
     */
    public function index(MhotelDataTable $mhotelDataTable)
    {
        return $mhotelDataTable->render('backend.mhotels.index');
    }

    /**
     * Show the form for creating a new Mhotel.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.mhotels.create');
    }

    /**
     * Store a newly created Mhotel in storage.
     *
     * @param CreateMhotelRequest $request
     *
     * @return Response
     */
    public function store(CreateMhotelRequest $request)
    {
        $input = $request->all();

        $mhotel = $this->mhotelRepository->create($input);

        Flash::success('Mhotel saved successfully.');

        return redirect(route('backend.mhotels.index'));
    }

    /**
     * Display the specified Mhotel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            Flash::error('Mhotel not found');

            return redirect(route('backend.mhotels.index'));
        }

        return view('backend.mhotels.show')->with('mhotel', $mhotel);
    }

    /**
     * Show the form for editing the specified Mhotel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            Flash::error('Mhotel not found');

            return redirect(route('backend.mhotels.index'));
        }

        return view('backend.mhotels.edit')->with('mhotel', $mhotel);
    }

    /**
     * Update the specified Mhotel in storage.
     *
     * @param  int              $id
     * @param UpdateMhotelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMhotelRequest $request)
    {
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            Flash::error('Mhotel not found');

            return redirect(route('backend.mhotels.index'));
        }

        $mhotel = $this->mhotelRepository->update($request->all(), $id);

        Flash::success('Mhotel updated successfully.');

        return redirect(route('backend.mhotels.index'));
    }

    /**
     * Remove the specified Mhotel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mhotel = $this->mhotelRepository->findWithoutFail($id);

        if (empty($mhotel)) {
            Flash::error('Mhotel not found');

            return redirect(route('backend.mhotels.index'));
        }

        $this->mhotelRepository->delete($id);

        Flash::success('Mhotel deleted successfully.');

        return redirect(route('backend.mhotels.index'));
    }
}
