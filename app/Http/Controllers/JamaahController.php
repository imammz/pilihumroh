<?php

namespace App\Http\Controllers;

use App\DataTables\JamaahDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJamaahRequest;
use App\Http\Requests\UpdateJamaahRequest;
use App\Repositories\JamaahRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class JamaahController extends AppBaseController
{
    /** @var  JamaahRepository */
    private $jamaahRepository;

    public function __construct(JamaahRepository $jamaahRepo)
    {
        $this->jamaahRepository = $jamaahRepo;
    }

    /**
     * Display a listing of the Jamaah.
     *
     * @param JamaahDataTable $jamaahDataTable
     * @return Response
     */
    public function index(JamaahDataTable $jamaahDataTable)
    {
        return $jamaahDataTable->render('backend.jamaahs.index');
    }

    /**
     * Show the form for creating a new Jamaah.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.jamaahs.create');
    }

    /**
     * Store a newly created Jamaah in storage.
     *
     * @param CreateJamaahRequest $request
     *
     * @return Response
     */
    public function store(CreateJamaahRequest $request)
    {
        $input = $request->all();

        $jamaah = $this->jamaahRepository->create($input);

        Flash::success('Jamaah saved successfully.');

        return redirect(route('backend.jamaahs.index'));
    }

    /**
     * Display the specified Jamaah.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            Flash::error('Jamaah not found');

            return redirect(route('backend.jamaahs.index'));
        }

        return view('backend.jamaahs.show')->with('jamaah', $jamaah);
    }

    /**
     * Show the form for editing the specified Jamaah.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            Flash::error('Jamaah not found');

            return redirect(route('backend.jamaahs.index'));
        }

        return view('backend.jamaahs.edit')->with('jamaah', $jamaah);
    }

    /**
     * Update the specified Jamaah in storage.
     *
     * @param  int              $id
     * @param UpdateJamaahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJamaahRequest $request)
    {
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            Flash::error('Jamaah not found');

            return redirect(route('backend.jamaahs.index'));
        }

        $jamaah = $this->jamaahRepository->update($request->all(), $id);

        Flash::success('Jamaah updated successfully.');

        return redirect(route('backend.jamaahs.index'));
    }

    /**
     * Remove the specified Jamaah from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jamaah = $this->jamaahRepository->findWithoutFail($id);

        if (empty($jamaah)) {
            Flash::error('Jamaah not found');

            return redirect(route('backend.jamaahs.index'));
        }

        $this->jamaahRepository->delete($id);

        Flash::success('Jamaah deleted successfully.');

        return redirect(route('backend.jamaahs.index'));
    }
}
