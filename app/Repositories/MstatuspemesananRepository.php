<?php

namespace App\Repositories;

use App\Models\Mstatuspemesanan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MstatuspemesananRepository
 * @package App\Repositories
 * @version March 11, 2018, 11:02 am UTC
 *
 * @method Mstatuspemesanan findWithoutFail($id, $columns = ['*'])
 * @method Mstatuspemesanan find($id, $columns = ['*'])
 * @method Mstatuspemesanan first($columns = ['*'])
*/
class MstatuspemesananRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mstatuspemesanan::class;
    }
}
