<?php

namespace App\Repositories;

use App\Models\Pemesanan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PemesananRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:32 am UTC
 *
 * @method Pemesanan findWithoutFail($id, $columns = ['*'])
 * @method Pemesanan find($id, $columns = ['*'])
 * @method Pemesanan first($columns = ['*'])
*/
class PemesananRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tglPesan',
        'email',
        'idPaket',
        'expired',
        'idStatusPemesanan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pemesanan::class;
    }
}
