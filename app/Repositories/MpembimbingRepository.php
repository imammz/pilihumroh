<?php

namespace App\Repositories;

use App\Models\Mpembimbing;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MpembimbingRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:44 am UTC
 *
 * @method Mpembimbing findWithoutFail($id, $columns = ['*'])
 * @method Mpembimbing find($id, $columns = ['*'])
 * @method Mpembimbing first($columns = ['*'])
*/
class MpembimbingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis',
        'nama',
        'nik',
        'telp',
        'asalNegara',
        'keterangan',
        'idTravelAgent',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mpembimbing::class;
    }
}
