<?php

namespace App\Repositories;

use App\Models\Mcurency;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class McurencyRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:39 am UTC
 *
 * @method Mcurency findWithoutFail($id, $columns = ['*'])
 * @method Mcurency find($id, $columns = ['*'])
 * @method Mcurency first($columns = ['*'])
*/
class McurencyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'idNegara',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mcurency::class;
    }
}
