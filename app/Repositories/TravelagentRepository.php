<?php

namespace App\Repositories;

use App\Models\Travelagent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TravelagentRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:01 am UTC
 *
 * @method Travelagent findWithoutFail($id, $columns = ['*'])
 * @method Travelagent find($id, $columns = ['*'])
 * @method Travelagent first($columns = ['*'])
*/
class TravelagentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'alamat',
        'telp',
        'email',
        'website',
        'namaPj',
        'nikPj',
        'notelpPj',
        'alamatPj',
        'profil',
        'kodeProv',
        'kodeKab',
        'idJenisIzin',
        'created_by',
        'updated_by',
        'deleted_by',
        'highlight',
        'fileLogo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Travelagent::class;
    }
}
