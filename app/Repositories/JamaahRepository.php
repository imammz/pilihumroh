<?php

namespace App\Repositories;

use App\Models\Jamaah;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JamaahRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:31 am UTC
 *
 * @method Jamaah findWithoutFail($id, $columns = ['*'])
 * @method Jamaah find($id, $columns = ['*'])
 * @method Jamaah first($columns = ['*'])
*/
class JamaahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'namaLengkap',
        'gender',
        'tempatLahir',
        'tglLahir',
        'nik',
        'noKK',
        'noPassport',
        'staAbilitas',
        'alamat',
        'noTelp',
        'kodeProv',
        'kodeKab',
        'idPemesanan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jamaah::class;
    }
}
