<?php

namespace App\Repositories;

use App\Models\Mrole;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MroleRepository
 * @package App\Repositories
 * @version March 11, 2018, 11:05 am UTC
 *
 * @method Mrole findWithoutFail($id, $columns = ['*'])
 * @method Mrole find($id, $columns = ['*'])
 * @method Mrole first($columns = ['*'])
*/
class MroleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'role',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mrole::class;
    }
}
