<?php

namespace App\Repositories;

use App\Models\Mjenisizin;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MjenisizinRepository
 * @package App\Repositories
 * @version March 11, 2018, 11:03 am UTC
 *
 * @method Mjenisizin findWithoutFail($id, $columns = ['*'])
 * @method Mjenisizin find($id, $columns = ['*'])
 * @method Mjenisizin first($columns = ['*'])
*/
class MjenisizinRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'keterangan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mjenisizin::class;
    }
}
