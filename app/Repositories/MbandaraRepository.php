<?php

namespace App\Repositories;

use App\Models\Mbandara;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MbandaraRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:36 am UTC
 *
 * @method Mbandara findWithoutFail($id, $columns = ['*'])
 * @method Mbandara find($id, $columns = ['*'])
 * @method Mbandara first($columns = ['*'])
*/
class MbandaraRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'kodeProv',
        'kodeKab',
        'idNegara',
        'sta_berangkat_aktif',
        'sta_datang_aktif',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mbandara::class;
    }
}
