<?php

namespace App\Repositories;

use App\Models\Mmaskapai;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MmaskapaiRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:41 am UTC
 *
 * @method Mmaskapai findWithoutFail($id, $columns = ['*'])
 * @method Mmaskapai find($id, $columns = ['*'])
 * @method Mmaskapai first($columns = ['*'])
*/
class MmaskapaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'namaMaskapai',
        'keterangan',
        'kelas',
        'kodeProv',
        'kodeKab',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mmaskapai::class;
    }
}
