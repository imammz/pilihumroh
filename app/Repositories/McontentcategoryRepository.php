<?php

namespace App\Repositories;

use App\Models\Mcontentcategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class McontentcategoryRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:59 am UTC
 *
 * @method Mcontentcategory findWithoutFail($id, $columns = ['*'])
 * @method Mcontentcategory find($id, $columns = ['*'])
 * @method Mcontentcategory first($columns = ['*'])
*/
class McontentcategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category',
        'desc',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mcontentcategory::class;
    }
}
