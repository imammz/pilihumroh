<?php

namespace App\Repositories;

use App\Models\Paket;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaketRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:30 am UTC
 *
 * @method Paket findWithoutFail($id, $columns = ['*'])
 * @method Paket find($id, $columns = ['*'])
 * @method Paket first($columns = ['*'])
*/
class PaketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'namaPaket',
        'idTravelAgent',
        'kodeProv',
        'kodeKab',
        'idJenisPaket',
        'tglBerangkat',
        'tglPulang',
        'staPromo',
        'hargaUtama',
        'hargaPromo',
        'persenPromo',
        'idCurency',
        'staPublish',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Paket::class;
    }
}
