<?php

namespace App\Repositories;

use App\Models\Mhotel;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MhotelRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:40 am UTC
 *
 * @method Mhotel findWithoutFail($id, $columns = ['*'])
 * @method Mhotel find($id, $columns = ['*'])
 * @method Mhotel first($columns = ['*'])
*/
class MhotelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'keterangan',
        'bintang',
        'kodeProv',
        'kodeKab',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mhotel::class;
    }
}
