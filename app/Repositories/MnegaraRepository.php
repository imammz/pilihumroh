<?php

namespace App\Repositories;

use App\Models\Mnegara;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MnegaraRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:42 am UTC
 *
 * @method Mnegara findWithoutFail($id, $columns = ['*'])
 * @method Mnegara find($id, $columns = ['*'])
 * @method Mnegara first($columns = ['*'])
*/
class MnegaraRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mnegara::class;
    }
}
