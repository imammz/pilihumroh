<?php

namespace App\Repositories;

use App\Models\Mwilayah;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MwilayahRepository
 * @package App\Repositories
 * @version March 11, 2018, 10:45 am UTC
 *
 * @method Mwilayah findWithoutFail($id, $columns = ['*'])
 * @method Mwilayah find($id, $columns = ['*'])
 * @method Mwilayah first($columns = ['*'])
*/
class MwilayahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kodeKab',
        'idNegara',
        'namaProv',
        'namaKab',
        'latitude',
        'longitude',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mwilayah::class;
    }
}
