<?php

namespace App\Repositories;

use App\Models\Jenispaket;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JenispaketRepository
 * @package App\Repositories
 * @version March 11, 2018, 11:04 am UTC
 *
 * @method Jenispaket findWithoutFail($id, $columns = ['*'])
 * @method Jenispaket find($id, $columns = ['*'])
 * @method Jenispaket first($columns = ['*'])
*/
class JenispaketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'keterangan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jenispaket::class;
    }
}
