<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Paket",
 *      required={""},
 *      @SWG\Property(
 *          property="idPaket",
 *          description="idPaket",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="namaPaket",
 *          description="namaPaket",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idTravelAgent",
 *          description="idTravelAgent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeProv",
 *          description="kodeProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeKab",
 *          description="kodeKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idJenisPaket",
 *          description="idJenisPaket",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tglBerangkat",
 *          description="tglBerangkat",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="tglPulang",
 *          description="tglPulang",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="staPromo",
 *          description="staPromo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="hargaUtama",
 *          description="hargaUtama",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="hargaPromo",
 *          description="hargaPromo",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="persenPromo",
 *          description="persenPromo",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="idCurency",
 *          description="idCurency",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="staPublish",
 *          description="staPublish",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      )
 * )
 */
class Paket extends Model
{
    use SoftDeletes;

    public $table = 'paket';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idPaket';

    public $fillable = [
        'namaPaket',
        'idTravelAgent',
        'kodeProv',
        'kodeKab',
        'idJenisPaket',
        'tglBerangkat',
        'tglPulang',
        'staPromo',
        'hargaUtama',
        'hargaPromo',
        'persenPromo',
        'idCurency',
        'staPublish',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idPaket' => 'string',
        'namaPaket' => 'string',
        'idTravelAgent' => 'string',
        'kodeProv' => 'string',
        'kodeKab' => 'string',
        'idJenisPaket' => 'integer',
        'tglBerangkat' => 'date',
        'tglPulang' => 'date',
        'staPromo' => 'boolean',
        'hargaUtama' => 'float',
        'hargaPromo' => 'float',
        'persenPromo' => 'float',
        'idCurency' => 'string',
        'staPublish' => 'boolean',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function jenispaket()
    {
        return $this->belongsTo(\App\Models\Jenispaket::class,'idJenisPaket','idJenisPaket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mcurency()
    {
        return $this->belongsTo(\App\Models\Mcurency::class,'idCurency','idCurency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mwilayah()
    {
        return $this->belongsTo(\App\Models\Mwilayah::class,'kodeProv','kodeProv');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function travelagent()
    {
        return $this->belongsTo(\App\Models\Travelagent::class,'idTravelAgent','idTravelAgent');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function paketJamaah()
    {
        return $this->hasMany(\App\Models\PaketJamaah::class,'idPaket','idPaket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function paketMhotel()
    {
        return $this->hasMany(\App\Models\PaketMhotel::class,'idPaket','idPaket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function paketMmaskapai()
    {
        return $this->hasMany(\App\Models\PaketMmaskapai::class,'idPaket','idPaket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function paketMpembimbing()
    {
        return $this->hasMany(\App\Models\PaketMpembimbing::class,'idPaket','idPaket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pemesanans()
    {
        return $this->hasMany(\App\Models\Pemesanan::class,'idPaket','idPaket');
    }
}
