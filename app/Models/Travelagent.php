<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Travelagent",
 *      required={""},
 *      @SWG\Property(
 *          property="idTravelAgent",
 *          description="idTravelAgent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nama",
 *          description="nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="alamat",
 *          description="alamat",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telp",
 *          description="telp",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="website",
 *          description="website",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="namaPj",
 *          description="namaPj",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nikPj",
 *          description="nikPj",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="notelpPj",
 *          description="notelpPj",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="alamatPj",
 *          description="alamatPj",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="profil",
 *          description="profil",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeProv",
 *          description="kodeProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeKab",
 *          description="kodeKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idJenisIzin",
 *          description="idJenisIzin",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="highlight",
 *          description="highlight",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="fileLogo",
 *          description="fileLogo",
 *          type="string"
 *      )
 * )
 */
class Travelagent extends Model
{
    use SoftDeletes;

    public $table = 'travelagent';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idTravelAgent';

    public $fillable = [
        'nama',
        'alamat',
        'telp',
        'email',
        'website',
        'namaPj',
        'nikPj',
        'notelpPj',
        'alamatPj',
        'profil',
        'kodeProv',
        'kodeKab',
        'idJenisIzin',
        'created_by',
        'updated_by',
        'deleted_by',
        'highlight',
        'fileLogo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idTravelAgent' => 'string',
        'nama' => 'string',
        'alamat' => 'string',
        'telp' => 'string',
        'email' => 'string',
        'website' => 'string',
        'namaPj' => 'string',
        'nikPj' => 'string',
        'notelpPj' => 'string',
        'alamatPj' => 'string',
        'profil' => 'string',
        'kodeProv' => 'string',
        'kodeKab' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string',
        'highlight' => 'boolean',
        'fileLogo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mjenisizin()
    {
        return $this->belongsTo(\App\Models\Mjenisizin::class,'idJenisIzin','idJenisIzin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mwilayah()
    {
        return $this->belongsTo(\App\Models\Mwilayah::class,'kodeProv','kodeProv');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function mpembimbings()
    {
        return $this->hasMany(\App\Models\Mpembimbing::class,'idTravelAgent','idTravelAgent');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pakets()
    {
        return $this->hasMany(\App\Models\Paket::class,'idTravelAgent','idTravelAgent');
    }
}
