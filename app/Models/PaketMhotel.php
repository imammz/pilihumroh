<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $idPaket
 * @property int $idHotel
 * @property string $created_at
 * @property string $created_by
 * @property Mhotel $mhotel
 * @property Paket $paket
 */
class PaketMhotel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'paket_mhotel';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mhotel()
    {
        return $this->belongsToMany('App\Models\Mhotel', 'idHotel', 'idHotel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paket()
    {
        return $this->belongsToMany('App\Models\Paket', 'idPaket', 'idPaket');
    }
}
