<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $idPaket
 * @property string $idJamaah
 * @property string $created_at
 * @property string $created_by
 * @property Jamaah $jamaah
 * @property Paket $paket
 */
class PaketJamaah extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'paket_jamaah';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jamaah()
    {
        return $this->belongsToMany('App\Models\Jamaah', 'idJamaah', 'idJamaah');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paket()
    {
        return $this->belongsToMany('App\Models\Paket', 'idPaket', 'idPaket');
    }
}
