<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Mmaskapai",
 *      required={""},
 *      @SWG\Property(
 *          property="idMaskapai",
 *          description="idMaskapai",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="namaMaskapai",
 *          description="namaMaskapai",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="keterangan",
 *          description="keterangan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kelas",
 *          description="kelas",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeProv",
 *          description="kodeProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeKab",
 *          description="kodeKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      )
 * )
 */
class Mmaskapai extends Model
{
    use SoftDeletes;

    public $table = 'mmaskapai';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idMaskapai';

    public $fillable = [
        'namaMaskapai',
        'keterangan',
        'kelas',
        'kodeProv',
        'kodeKab',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idMaskapai' => 'integer',
        'namaMaskapai' => 'string',
        'keterangan' => 'string',
        'kelas' => 'string',
        'kodeProv' => 'string',
        'kodeKab' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mwilayah()
    {
        return $this->belongsTo(\App\Models\Mwilayah::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function paketMmaskapais()
    {
        return $this->hasMany(\App\Models\PaketMmaskapai::class);
    }
}
