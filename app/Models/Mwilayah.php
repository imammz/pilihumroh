<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Mwilayah",
 *      required={""},
 *      @SWG\Property(
 *          property="kodeProv",
 *          description="kodeProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeKab",
 *          description="kodeKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idNegara",
 *          description="idNegara",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="namaProv",
 *          description="namaProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="namaKab",
 *          description="namaKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      )
 * )
 */
class Mwilayah extends Model
{
    use SoftDeletes;

    public $table = 'mwilayah';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'kodeProv';

    public $fillable = [
        'kodeKab',
        'idNegara',
        'namaProv',
        'namaKab',
        'latitude',
        'longitude',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kodeProv' => 'string',
        'kodeKab' => 'string',
        'idNegara' => 'string',
        'namaProv' => 'string',
        'namaKab' => 'string',
        'latitude' => 'float',
        'longitude' => 'float',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mnegara()
    {
        return $this->belongsTo(\App\Models\Mnegara::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jamaahs()
    {
        return $this->hasMany(\App\Models\Jamaah::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function mhotels()
    {
        return $this->hasMany(\App\Models\Mhotel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function mmaskapais()
    {
        return $this->hasMany(\App\Models\Mmaskapai::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pakets()
    {
        return $this->hasMany(\App\Models\Paket::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function mjenisizins()
    {
        return $this->belongsToMany(\App\Models\Mjenisizin::class, 'travelagent');
    }
}
