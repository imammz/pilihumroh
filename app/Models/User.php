<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @SWG\Definition(
 *      definition="User",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fullname",
 *          description="fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idRole",
 *          description="idRole",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="fbToken",
 *          description="fbToken",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="twToken",
 *          description="twToken",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="googleToken",
 *          description="googleToken",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      )
 * )
 */
class User extends Authenticatable
{
    use SoftDeletes;
    	 use Notifiable;
    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    public $fillable = [
        'name',
        'email',
        'password',
        'remember_token',
        'deleted_by',
        'fullname',
        'idRole',
        'fbToken',
        'twToken',
        'googleToken',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'deleted_by' => 'string',
        'fullname' => 'string',
        'idRole' => 'boolean',
        'fbToken' => 'string',
        'twToken' => 'string',
        'googleToken' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mrole()
    {
        return $this->belongsTo(\App\Models\Mrole::class,'idRole','idRole');
    }
    
    

    
    }
