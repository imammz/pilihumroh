<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Content",
 *      required={""},
 *      @SWG\Property(
 *          property="idContent",
 *          description="idContent",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="judul",
 *          description="judul",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="isi",
 *          description="isi",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thumbnail",
 *          description="thumbnail",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="staPublish",
 *          description="staPublish",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="tglTerbit",
 *          description="tglTerbit",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="idContentCategory",
 *          description="idContentCategory",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="highlight",
 *          description="highlight",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="author",
 *          description="author",
 *          type="string"
 *      )
 * )
 */
class Content extends Model
{
    use SoftDeletes;

    public $table = 'content';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idContent';

    public $fillable = [
        'judul',
        'isi',
        'thumbnail',
        'staPublish',
        'tglTerbit',
        'idContentCategory',
        'created_by',
        'updated_by',
        'deleted_by',
        'highlight',
        'author'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idContent' => 'integer',
        'judul' => 'string',
        'isi' => 'string',
        'thumbnail' => 'string',
        'staPublish' => 'boolean',
        'tglTerbit' => 'date',
        'idContentCategory' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string',
        'highlight' => 'boolean',
        'author' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mcontentcategory()
    {
        return $this->belongsTo(\App\Models\Mcontentcategory::class,'idContentCategory','idContentCategory');
    }
}
