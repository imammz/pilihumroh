<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Mpembimbing",
 *      required={""},
 *      @SWG\Property(
 *          property="idPembimbing",
 *          description="idPembimbing",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="jenis",
 *          description="jenis",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nama",
 *          description="nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nik",
 *          description="nik",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telp",
 *          description="telp",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="asalNegara",
 *          description="asalNegara",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="keterangan",
 *          description="keterangan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idTravelAgent",
 *          description="idTravelAgent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      )
 * )
 */
class Mpembimbing extends Model
{
    use SoftDeletes;

    public $table = 'mpembimbing';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idPembimbing';

    public $fillable = [
        'jenis',
        'nama',
        'nik',
        'telp',
        'asalNegara',
        'keterangan',
        'idTravelAgent',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idPembimbing' => 'integer',
        'jenis' => 'string',
        'nama' => 'string',
        'nik' => 'string',
        'telp' => 'string',
        'asalNegara' => 'string',
        'keterangan' => 'string',
        'idTravelAgent' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function travelagent()
    {
        return $this->belongsTo(\App\Models\Travelagent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function paketMpembimbings()
    {
        return $this->hasMany(\App\Models\PaketMpembimbing::class);
    }
}
