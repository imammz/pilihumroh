<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Pemesanan",
 *      required={""},
 *      @SWG\Property(
 *          property="idPemesanan",
 *          description="idPemesanan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tglPesan",
 *          description="tglPesan",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idPaket",
 *          description="idPaket",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idStatusPemesanan",
 *          description="idStatusPemesanan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      )
 * )
 */
class Pemesanan extends Model
{
    use SoftDeletes;

    public $table = 'pemesanan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idPemesanan';

    public $fillable = [
        'tglPesan',
        'email',
        'idPaket',
        'expired',
        'idStatusPemesanan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idPemesanan' => 'string',
        'tglPesan' => 'date',
        'email' => 'string',
        'idPaket' => 'string',
        'idStatusPemesanan' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mstatuspemesanan()
    {
        return $this->belongsTo(\App\Models\Mstatuspemesanan::class,'idStatusPemesanan','idStatusPemesanan');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function User()
    {
        return $this->belongsTo(\App\Models\User::class,'email','email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paket()
    {
        return $this->belongsTo(\App\Models\Paket::class,'idPaket','idPaket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jamaahs()
    {
        return $this->hasMany(\App\Models\Jamaah::class,'idPemesanan','idPemesanan');
    }
}
