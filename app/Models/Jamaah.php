<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Jamaah",
 *      required={""},
 *      @SWG\Property(
 *          property="idJamaah",
 *          description="idJamaah",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="namaLengkap",
 *          description="namaLengkap",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gender",
 *          description="gender",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tempatLahir",
 *          description="tempatLahir",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tglLahir",
 *          description="tglLahir",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="nik",
 *          description="nik",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="noKK",
 *          description="noKK",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="noPassport",
 *          description="noPassport",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="staAbilitas",
 *          description="staAbilitas",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="alamat",
 *          description="alamat",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="noTelp",
 *          description="noTelp",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeProv",
 *          description="kodeProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeKab",
 *          description="kodeKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idPemesanan",
 *          description="idPemesanan",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="string"
 *      )
 * )
 */
class Jamaah extends Model
{
    use SoftDeletes;

    public $table = 'jamaah';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'idJamaah';

    public $fillable = [
        'email',
        'namaLengkap',
        'gender',
        'tempatLahir',
        'tglLahir',
        'nik',
        'noKK',
        'noPassport',
        'staAbilitas',
        'alamat',
        'noTelp',
        'kodeProv',
        'kodeKab',
        'idPemesanan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idJamaah' => 'string',
        'email' => 'string',
        'namaLengkap' => 'string',
        'gender' => 'string',
        'tempatLahir' => 'string',
        'tglLahir' => 'date',
        'nik' => 'string',
        'noKK' => 'string',
        'noPassport' => 'string',
        'staAbilitas' => 'boolean',
        'alamat' => 'string',
        'noTelp' => 'string',
        'kodeProv' => 'string',
        'kodeKab' => 'string',
        'idPemesanan' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function muser()
    {
        return $this->belongsTo(\App\Models\Muser::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mwilayah()
    {
        return $this->belongsTo(\App\Models\Mwilayah::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pemesanan()
    {
        return $this->belongsTo(\App\Models\Pemesanan::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function paketJamaahs()
    {
        return $this->hasMany(\App\Models\PaketJamaah::class);
    }
}
