<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $idPaket
 * @property int $idMaskapai
 * @property string $created_at
 * @property string $created_by
 * @property Mmaskapai $mmaskapai
 * @property Paket $paket
 */
class PaketMmaskapai extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'paket_mmaskapai';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mmaskapai()
    {
        return $this->belongsToMany('App\Models\Mmaskapai', 'idMaskapai', 'idMaskapai');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paket()
    {
        return $this->belongsToMany('App\Models\Paket', 'idPaket', 'idPaket');
    }
}
