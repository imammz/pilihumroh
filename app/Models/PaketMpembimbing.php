<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $idPaket
 * @property int $idPembimbing
 * @property string $created_at
 * @property string $created_by
 * @property Mpembimbing $mpembimbing
 * @property Paket $paket
 */
class PaketMpembimbing extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'paket_mpembimbing';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mpembimbing()
    {
        return $this->belongsToMany('App\Models\\Mpembimbing', 'idPembimbing', 'idPembimbing');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paket()
    {
        return $this->belongsToMany('App\Models\Paket', 'idPaket', 'idPaket');
    }
}
