<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Mbandara",
 *      required={""},
 *      @SWG\Property(
 *          property="kode",
 *          description="kode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nama",
 *          description="nama",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeProv",
 *          description="kodeProv",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="kodeKab",
 *          description="kodeKab",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idNegara",
 *          description="idNegara",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sta_berangkat_aktif",
 *          description="sta_berangkat_aktif",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="sta_datang_aktif",
 *          description="sta_datang_aktif",
 *          type="boolean"
 *      )
 * )
 */
class Mbandara extends Model
{
    use SoftDeletes;

    public $table = 'mbandara';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'kode';

    public $fillable = [
        'nama',
        'kodeProv',
        'kodeKab',
        'idNegara',
        'sta_berangkat_aktif',
        'sta_datang_aktif'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kode' => 'string',
        'nama' => 'string',
        'kodeProv' => 'string',
        'kodeKab' => 'string',
        'idNegara' => 'string',
        'sta_berangkat_aktif' => 'boolean',
        'sta_datang_aktif' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
