<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MnegaraApiTest extends TestCase
{
    use MakeMnegaraTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMnegara()
    {
        $mnegara = $this->fakeMnegaraData();
        $this->json('POST', '/api/v1/mnegaras', $mnegara);

        $this->assertApiResponse($mnegara);
    }

    /**
     * @test
     */
    public function testReadMnegara()
    {
        $mnegara = $this->makeMnegara();
        $this->json('GET', '/api/v1/mnegaras/'.$mnegara->idNegara);

        $this->assertApiResponse($mnegara->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMnegara()
    {
        $mnegara = $this->makeMnegara();
        $editedMnegara = $this->fakeMnegaraData();

        $this->json('PUT', '/api/v1/mnegaras/'.$mnegara->idNegara, $editedMnegara);

        $this->assertApiResponse($editedMnegara);
    }

    /**
     * @test
     */
    public function testDeleteMnegara()
    {
        $mnegara = $this->makeMnegara();
        $this->json('DELETE', '/api/v1/mnegaras/'.$mnegara->idNegara);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mnegaras/'.$mnegara->idNegara);

        $this->assertResponseStatus(404);
    }
}
