<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MhotelApiTest extends TestCase
{
    use MakeMhotelTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMhotel()
    {
        $mhotel = $this->fakeMhotelData();
        $this->json('POST', '/api/v1/mhotels', $mhotel);

        $this->assertApiResponse($mhotel);
    }

    /**
     * @test
     */
    public function testReadMhotel()
    {
        $mhotel = $this->makeMhotel();
        $this->json('GET', '/api/v1/mhotels/'.$mhotel->idHotel);

        $this->assertApiResponse($mhotel->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMhotel()
    {
        $mhotel = $this->makeMhotel();
        $editedMhotel = $this->fakeMhotelData();

        $this->json('PUT', '/api/v1/mhotels/'.$mhotel->idHotel, $editedMhotel);

        $this->assertApiResponse($editedMhotel);
    }

    /**
     * @test
     */
    public function testDeleteMhotel()
    {
        $mhotel = $this->makeMhotel();
        $this->json('DELETE', '/api/v1/mhotels/'.$mhotel->idHotel);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mhotels/'.$mhotel->idHotel);

        $this->assertResponseStatus(404);
    }
}
