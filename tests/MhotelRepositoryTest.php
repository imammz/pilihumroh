<?php

use App\Models\Mhotel;
use App\Repositories\MhotelRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MhotelRepositoryTest extends TestCase
{
    use MakeMhotelTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MhotelRepository
     */
    protected $mhotelRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mhotelRepo = App::make(MhotelRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMhotel()
    {
        $mhotel = $this->fakeMhotelData();
        $createdMhotel = $this->mhotelRepo->create($mhotel);
        $createdMhotel = $createdMhotel->toArray();
        $this->assertArrayHasKey('id', $createdMhotel);
        $this->assertNotNull($createdMhotel['id'], 'Created Mhotel must have id specified');
        $this->assertNotNull(Mhotel::find($createdMhotel['id']), 'Mhotel with given id must be in DB');
        $this->assertModelData($mhotel, $createdMhotel);
    }

    /**
     * @test read
     */
    public function testReadMhotel()
    {
        $mhotel = $this->makeMhotel();
        $dbMhotel = $this->mhotelRepo->find($mhotel->idHotel);
        $dbMhotel = $dbMhotel->toArray();
        $this->assertModelData($mhotel->toArray(), $dbMhotel);
    }

    /**
     * @test update
     */
    public function testUpdateMhotel()
    {
        $mhotel = $this->makeMhotel();
        $fakeMhotel = $this->fakeMhotelData();
        $updatedMhotel = $this->mhotelRepo->update($fakeMhotel, $mhotel->idHotel);
        $this->assertModelData($fakeMhotel, $updatedMhotel->toArray());
        $dbMhotel = $this->mhotelRepo->find($mhotel->idHotel);
        $this->assertModelData($fakeMhotel, $dbMhotel->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMhotel()
    {
        $mhotel = $this->makeMhotel();
        $resp = $this->mhotelRepo->delete($mhotel->idHotel);
        $this->assertTrue($resp);
        $this->assertNull(Mhotel::find($mhotel->idHotel), 'Mhotel should not exist in DB');
    }
}
