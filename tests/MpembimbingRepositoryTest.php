<?php

use App\Models\Mpembimbing;
use App\Repositories\MpembimbingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MpembimbingRepositoryTest extends TestCase
{
    use MakeMpembimbingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MpembimbingRepository
     */
    protected $mpembimbingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mpembimbingRepo = App::make(MpembimbingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMpembimbing()
    {
        $mpembimbing = $this->fakeMpembimbingData();
        $createdMpembimbing = $this->mpembimbingRepo->create($mpembimbing);
        $createdMpembimbing = $createdMpembimbing->toArray();
        $this->assertArrayHasKey('id', $createdMpembimbing);
        $this->assertNotNull($createdMpembimbing['id'], 'Created Mpembimbing must have id specified');
        $this->assertNotNull(Mpembimbing::find($createdMpembimbing['id']), 'Mpembimbing with given id must be in DB');
        $this->assertModelData($mpembimbing, $createdMpembimbing);
    }

    /**
     * @test read
     */
    public function testReadMpembimbing()
    {
        $mpembimbing = $this->makeMpembimbing();
        $dbMpembimbing = $this->mpembimbingRepo->find($mpembimbing->idPembimbing);
        $dbMpembimbing = $dbMpembimbing->toArray();
        $this->assertModelData($mpembimbing->toArray(), $dbMpembimbing);
    }

    /**
     * @test update
     */
    public function testUpdateMpembimbing()
    {
        $mpembimbing = $this->makeMpembimbing();
        $fakeMpembimbing = $this->fakeMpembimbingData();
        $updatedMpembimbing = $this->mpembimbingRepo->update($fakeMpembimbing, $mpembimbing->idPembimbing);
        $this->assertModelData($fakeMpembimbing, $updatedMpembimbing->toArray());
        $dbMpembimbing = $this->mpembimbingRepo->find($mpembimbing->idPembimbing);
        $this->assertModelData($fakeMpembimbing, $dbMpembimbing->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMpembimbing()
    {
        $mpembimbing = $this->makeMpembimbing();
        $resp = $this->mpembimbingRepo->delete($mpembimbing->idPembimbing);
        $this->assertTrue($resp);
        $this->assertNull(Mpembimbing::find($mpembimbing->idPembimbing), 'Mpembimbing should not exist in DB');
    }
}
