<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TravelagentApiTest extends TestCase
{
    use MakeTravelagentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTravelagent()
    {
        $travelagent = $this->fakeTravelagentData();
        $this->json('POST', '/api/v1/travelagents', $travelagent);

        $this->assertApiResponse($travelagent);
    }

    /**
     * @test
     */
    public function testReadTravelagent()
    {
        $travelagent = $this->makeTravelagent();
        $this->json('GET', '/api/v1/travelagents/'.$travelagent->idTravelAgent);

        $this->assertApiResponse($travelagent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTravelagent()
    {
        $travelagent = $this->makeTravelagent();
        $editedTravelagent = $this->fakeTravelagentData();

        $this->json('PUT', '/api/v1/travelagents/'.$travelagent->idTravelAgent, $editedTravelagent);

        $this->assertApiResponse($editedTravelagent);
    }

    /**
     * @test
     */
    public function testDeleteTravelagent()
    {
        $travelagent = $this->makeTravelagent();
        $this->json('DELETE', '/api/v1/travelagents/'.$travelagent->idTravelAgent);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/travelagents/'.$travelagent->idTravelAgent);

        $this->assertResponseStatus(404);
    }
}
