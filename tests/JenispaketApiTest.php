<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JenispaketApiTest extends TestCase
{
    use MakeJenispaketTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateJenispaket()
    {
        $jenispaket = $this->fakeJenispaketData();
        $this->json('POST', '/api/v1/jenispakets', $jenispaket);

        $this->assertApiResponse($jenispaket);
    }

    /**
     * @test
     */
    public function testReadJenispaket()
    {
        $jenispaket = $this->makeJenispaket();
        $this->json('GET', '/api/v1/jenispakets/'.$jenispaket->idJenisPaket);

        $this->assertApiResponse($jenispaket->toArray());
    }

    /**
     * @test
     */
    public function testUpdateJenispaket()
    {
        $jenispaket = $this->makeJenispaket();
        $editedJenispaket = $this->fakeJenispaketData();

        $this->json('PUT', '/api/v1/jenispakets/'.$jenispaket->idJenisPaket, $editedJenispaket);

        $this->assertApiResponse($editedJenispaket);
    }

    /**
     * @test
     */
    public function testDeleteJenispaket()
    {
        $jenispaket = $this->makeJenispaket();
        $this->json('DELETE', '/api/v1/jenispakets/'.$jenispaket->idJenisPaket);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/jenispakets/'.$jenispaket->idJenisPaket);

        $this->assertResponseStatus(404);
    }
}
