<?php

use Faker\Factory as Faker;
use App\Models\Travelagent;
use App\Repositories\TravelagentRepository;

trait MakeTravelagentTrait
{
    /**
     * Create fake instance of Travelagent and save it in database
     *
     * @param array $travelagentFields
     * @return Travelagent
     */
    public function makeTravelagent($travelagentFields = [])
    {
        /** @var TravelagentRepository $travelagentRepo */
        $travelagentRepo = App::make(TravelagentRepository::class);
        $theme = $this->fakeTravelagentData($travelagentFields);
        return $travelagentRepo->create($theme);
    }

    /**
     * Get fake instance of Travelagent
     *
     * @param array $travelagentFields
     * @return Travelagent
     */
    public function fakeTravelagent($travelagentFields = [])
    {
        return new Travelagent($this->fakeTravelagentData($travelagentFields));
    }

    /**
     * Get fake data of Travelagent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTravelagentData($travelagentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'alamat' => $fake->word,
            'telp' => $fake->word,
            'email' => $fake->word,
            'website' => $fake->word,
            'namaPj' => $fake->word,
            'nikPj' => $fake->word,
            'notelpPj' => $fake->word,
            'alamatPj' => $fake->word,
            'profil' => $fake->text,
            'kodeProv' => $fake->word,
            'kodeKab' => $fake->word,
            'idJenisIzin' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word,
            'highlight' => $fake->word,
            'fileLogo' => $fake->word
        ], $travelagentFields);
    }
}
