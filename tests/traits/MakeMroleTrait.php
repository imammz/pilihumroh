<?php

use Faker\Factory as Faker;
use App\Models\Mrole;
use App\Repositories\MroleRepository;

trait MakeMroleTrait
{
    /**
     * Create fake instance of Mrole and save it in database
     *
     * @param array $mroleFields
     * @return Mrole
     */
    public function makeMrole($mroleFields = [])
    {
        /** @var MroleRepository $mroleRepo */
        $mroleRepo = App::make(MroleRepository::class);
        $theme = $this->fakeMroleData($mroleFields);
        return $mroleRepo->create($theme);
    }

    /**
     * Get fake instance of Mrole
     *
     * @param array $mroleFields
     * @return Mrole
     */
    public function fakeMrole($mroleFields = [])
    {
        return new Mrole($this->fakeMroleData($mroleFields));
    }

    /**
     * Get fake data of Mrole
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMroleData($mroleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'role' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mroleFields);
    }
}
