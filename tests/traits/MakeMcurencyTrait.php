<?php

use Faker\Factory as Faker;
use App\Models\Mcurency;
use App\Repositories\McurencyRepository;

trait MakeMcurencyTrait
{
    /**
     * Create fake instance of Mcurency and save it in database
     *
     * @param array $mcurencyFields
     * @return Mcurency
     */
    public function makeMcurency($mcurencyFields = [])
    {
        /** @var McurencyRepository $mcurencyRepo */
        $mcurencyRepo = App::make(McurencyRepository::class);
        $theme = $this->fakeMcurencyData($mcurencyFields);
        return $mcurencyRepo->create($theme);
    }

    /**
     * Get fake instance of Mcurency
     *
     * @param array $mcurencyFields
     * @return Mcurency
     */
    public function fakeMcurency($mcurencyFields = [])
    {
        return new Mcurency($this->fakeMcurencyData($mcurencyFields));
    }

    /**
     * Get fake data of Mcurency
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMcurencyData($mcurencyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'idNegara' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mcurencyFields);
    }
}
