<?php

use Faker\Factory as Faker;
use App\Models\Jamaah;
use App\Repositories\JamaahRepository;

trait MakeJamaahTrait
{
    /**
     * Create fake instance of Jamaah and save it in database
     *
     * @param array $jamaahFields
     * @return Jamaah
     */
    public function makeJamaah($jamaahFields = [])
    {
        /** @var JamaahRepository $jamaahRepo */
        $jamaahRepo = App::make(JamaahRepository::class);
        $theme = $this->fakeJamaahData($jamaahFields);
        return $jamaahRepo->create($theme);
    }

    /**
     * Get fake instance of Jamaah
     *
     * @param array $jamaahFields
     * @return Jamaah
     */
    public function fakeJamaah($jamaahFields = [])
    {
        return new Jamaah($this->fakeJamaahData($jamaahFields));
    }

    /**
     * Get fake data of Jamaah
     *
     * @param array $postFields
     * @return array
     */
    public function fakeJamaahData($jamaahFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'email' => $fake->word,
            'namaLengkap' => $fake->word,
            'gender' => $fake->word,
            'tempatLahir' => $fake->word,
            'tglLahir' => $fake->word,
            'nik' => $fake->word,
            'noKK' => $fake->word,
            'noPassport' => $fake->word,
            'staAbilitas' => $fake->word,
            'alamat' => $fake->word,
            'noTelp' => $fake->word,
            'kodeProv' => $fake->word,
            'kodeKab' => $fake->word,
            'idPemesanan' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $jamaahFields);
    }
}
