<?php

use Faker\Factory as Faker;
use App\Models\Paket;
use App\Repositories\PaketRepository;

trait MakePaketTrait
{
    /**
     * Create fake instance of Paket and save it in database
     *
     * @param array $paketFields
     * @return Paket
     */
    public function makePaket($paketFields = [])
    {
        /** @var PaketRepository $paketRepo */
        $paketRepo = App::make(PaketRepository::class);
        $theme = $this->fakePaketData($paketFields);
        return $paketRepo->create($theme);
    }

    /**
     * Get fake instance of Paket
     *
     * @param array $paketFields
     * @return Paket
     */
    public function fakePaket($paketFields = [])
    {
        return new Paket($this->fakePaketData($paketFields));
    }

    /**
     * Get fake data of Paket
     *
     * @param array $postFields
     * @return array
     */
    public function fakePaketData($paketFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'namaPaket' => $fake->word,
            'idTravelAgent' => $fake->word,
            'kodeProv' => $fake->word,
            'kodeKab' => $fake->word,
            'idJenisPaket' => $fake->randomDigitNotNull,
            'tglBerangkat' => $fake->word,
            'tglPulang' => $fake->word,
            'staPromo' => $fake->word,
            'hargaUtama' => $fake->randomDigitNotNull,
            'hargaPromo' => $fake->randomDigitNotNull,
            'persenPromo' => $fake->randomDigitNotNull,
            'idCurency' => $fake->word,
            'staPublish' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $paketFields);
    }
}
