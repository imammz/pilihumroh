<?php

use Faker\Factory as Faker;
use App\Models\Jenispaket;
use App\Repositories\JenispaketRepository;

trait MakeJenispaketTrait
{
    /**
     * Create fake instance of Jenispaket and save it in database
     *
     * @param array $jenispaketFields
     * @return Jenispaket
     */
    public function makeJenispaket($jenispaketFields = [])
    {
        /** @var JenispaketRepository $jenispaketRepo */
        $jenispaketRepo = App::make(JenispaketRepository::class);
        $theme = $this->fakeJenispaketData($jenispaketFields);
        return $jenispaketRepo->create($theme);
    }

    /**
     * Get fake instance of Jenispaket
     *
     * @param array $jenispaketFields
     * @return Jenispaket
     */
    public function fakeJenispaket($jenispaketFields = [])
    {
        return new Jenispaket($this->fakeJenispaketData($jenispaketFields));
    }

    /**
     * Get fake data of Jenispaket
     *
     * @param array $postFields
     * @return array
     */
    public function fakeJenispaketData($jenispaketFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'keterangan' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $jenispaketFields);
    }
}
