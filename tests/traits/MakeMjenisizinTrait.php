<?php

use Faker\Factory as Faker;
use App\Models\Mjenisizin;
use App\Repositories\MjenisizinRepository;

trait MakeMjenisizinTrait
{
    /**
     * Create fake instance of Mjenisizin and save it in database
     *
     * @param array $mjenisizinFields
     * @return Mjenisizin
     */
    public function makeMjenisizin($mjenisizinFields = [])
    {
        /** @var MjenisizinRepository $mjenisizinRepo */
        $mjenisizinRepo = App::make(MjenisizinRepository::class);
        $theme = $this->fakeMjenisizinData($mjenisizinFields);
        return $mjenisizinRepo->create($theme);
    }

    /**
     * Get fake instance of Mjenisizin
     *
     * @param array $mjenisizinFields
     * @return Mjenisizin
     */
    public function fakeMjenisizin($mjenisizinFields = [])
    {
        return new Mjenisizin($this->fakeMjenisizinData($mjenisizinFields));
    }

    /**
     * Get fake data of Mjenisizin
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMjenisizinData($mjenisizinFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'keterangan' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mjenisizinFields);
    }
}
