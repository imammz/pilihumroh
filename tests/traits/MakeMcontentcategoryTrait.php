<?php

use Faker\Factory as Faker;
use App\Models\Mcontentcategory;
use App\Repositories\McontentcategoryRepository;

trait MakeMcontentcategoryTrait
{
    /**
     * Create fake instance of Mcontentcategory and save it in database
     *
     * @param array $mcontentcategoryFields
     * @return Mcontentcategory
     */
    public function makeMcontentcategory($mcontentcategoryFields = [])
    {
        /** @var McontentcategoryRepository $mcontentcategoryRepo */
        $mcontentcategoryRepo = App::make(McontentcategoryRepository::class);
        $theme = $this->fakeMcontentcategoryData($mcontentcategoryFields);
        return $mcontentcategoryRepo->create($theme);
    }

    /**
     * Get fake instance of Mcontentcategory
     *
     * @param array $mcontentcategoryFields
     * @return Mcontentcategory
     */
    public function fakeMcontentcategory($mcontentcategoryFields = [])
    {
        return new Mcontentcategory($this->fakeMcontentcategoryData($mcontentcategoryFields));
    }

    /**
     * Get fake data of Mcontentcategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMcontentcategoryData($mcontentcategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'category' => $fake->word,
            'desc' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mcontentcategoryFields);
    }
}
