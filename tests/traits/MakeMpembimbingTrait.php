<?php

use Faker\Factory as Faker;
use App\Models\Mpembimbing;
use App\Repositories\MpembimbingRepository;

trait MakeMpembimbingTrait
{
    /**
     * Create fake instance of Mpembimbing and save it in database
     *
     * @param array $mpembimbingFields
     * @return Mpembimbing
     */
    public function makeMpembimbing($mpembimbingFields = [])
    {
        /** @var MpembimbingRepository $mpembimbingRepo */
        $mpembimbingRepo = App::make(MpembimbingRepository::class);
        $theme = $this->fakeMpembimbingData($mpembimbingFields);
        return $mpembimbingRepo->create($theme);
    }

    /**
     * Get fake instance of Mpembimbing
     *
     * @param array $mpembimbingFields
     * @return Mpembimbing
     */
    public function fakeMpembimbing($mpembimbingFields = [])
    {
        return new Mpembimbing($this->fakeMpembimbingData($mpembimbingFields));
    }

    /**
     * Get fake data of Mpembimbing
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMpembimbingData($mpembimbingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'jenis' => $fake->word,
            'nama' => $fake->word,
            'nik' => $fake->word,
            'telp' => $fake->word,
            'asalNegara' => $fake->word,
            'keterangan' => $fake->word,
            'idTravelAgent' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mpembimbingFields);
    }
}
