<?php

use Faker\Factory as Faker;
use App\Models\Mwilayah;
use App\Repositories\MwilayahRepository;

trait MakeMwilayahTrait
{
    /**
     * Create fake instance of Mwilayah and save it in database
     *
     * @param array $mwilayahFields
     * @return Mwilayah
     */
    public function makeMwilayah($mwilayahFields = [])
    {
        /** @var MwilayahRepository $mwilayahRepo */
        $mwilayahRepo = App::make(MwilayahRepository::class);
        $theme = $this->fakeMwilayahData($mwilayahFields);
        return $mwilayahRepo->create($theme);
    }

    /**
     * Get fake instance of Mwilayah
     *
     * @param array $mwilayahFields
     * @return Mwilayah
     */
    public function fakeMwilayah($mwilayahFields = [])
    {
        return new Mwilayah($this->fakeMwilayahData($mwilayahFields));
    }

    /**
     * Get fake data of Mwilayah
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMwilayahData($mwilayahFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'kodeKab' => $fake->word,
            'idNegara' => $fake->word,
            'namaProv' => $fake->word,
            'namaKab' => $fake->word,
            'latitude' => $fake->randomDigitNotNull,
            'longitude' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mwilayahFields);
    }
}
