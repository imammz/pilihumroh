<?php

use Faker\Factory as Faker;
use App\Models\Mnegara;
use App\Repositories\MnegaraRepository;

trait MakeMnegaraTrait
{
    /**
     * Create fake instance of Mnegara and save it in database
     *
     * @param array $mnegaraFields
     * @return Mnegara
     */
    public function makeMnegara($mnegaraFields = [])
    {
        /** @var MnegaraRepository $mnegaraRepo */
        $mnegaraRepo = App::make(MnegaraRepository::class);
        $theme = $this->fakeMnegaraData($mnegaraFields);
        return $mnegaraRepo->create($theme);
    }

    /**
     * Get fake instance of Mnegara
     *
     * @param array $mnegaraFields
     * @return Mnegara
     */
    public function fakeMnegara($mnegaraFields = [])
    {
        return new Mnegara($this->fakeMnegaraData($mnegaraFields));
    }

    /**
     * Get fake data of Mnegara
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMnegaraData($mnegaraFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mnegaraFields);
    }
}
