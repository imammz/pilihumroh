<?php

use Faker\Factory as Faker;
use App\Models\Mstatuspemesanan;
use App\Repositories\MstatuspemesananRepository;

trait MakeMstatuspemesananTrait
{
    /**
     * Create fake instance of Mstatuspemesanan and save it in database
     *
     * @param array $mstatuspemesananFields
     * @return Mstatuspemesanan
     */
    public function makeMstatuspemesanan($mstatuspemesananFields = [])
    {
        /** @var MstatuspemesananRepository $mstatuspemesananRepo */
        $mstatuspemesananRepo = App::make(MstatuspemesananRepository::class);
        $theme = $this->fakeMstatuspemesananData($mstatuspemesananFields);
        return $mstatuspemesananRepo->create($theme);
    }

    /**
     * Get fake instance of Mstatuspemesanan
     *
     * @param array $mstatuspemesananFields
     * @return Mstatuspemesanan
     */
    public function fakeMstatuspemesanan($mstatuspemesananFields = [])
    {
        return new Mstatuspemesanan($this->fakeMstatuspemesananData($mstatuspemesananFields));
    }

    /**
     * Get fake data of Mstatuspemesanan
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMstatuspemesananData($mstatuspemesananFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mstatuspemesananFields);
    }
}
