<?php

use Faker\Factory as Faker;
use App\Models\Mbandara;
use App\Repositories\MbandaraRepository;

trait MakeMbandaraTrait
{
    /**
     * Create fake instance of Mbandara and save it in database
     *
     * @param array $mbandaraFields
     * @return Mbandara
     */
    public function makeMbandara($mbandaraFields = [])
    {
        /** @var MbandaraRepository $mbandaraRepo */
        $mbandaraRepo = App::make(MbandaraRepository::class);
        $theme = $this->fakeMbandaraData($mbandaraFields);
        return $mbandaraRepo->create($theme);
    }

    /**
     * Get fake instance of Mbandara
     *
     * @param array $mbandaraFields
     * @return Mbandara
     */
    public function fakeMbandara($mbandaraFields = [])
    {
        return new Mbandara($this->fakeMbandaraData($mbandaraFields));
    }

    /**
     * Get fake data of Mbandara
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMbandaraData($mbandaraFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'kodeProv' => $fake->word,
            'kodeKab' => $fake->word,
            'idNegara' => $fake->word,
            'sta_berangkat_aktif' => $fake->word,
            'sta_datang_aktif' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mbandaraFields);
    }
}
