<?php

use Faker\Factory as Faker;
use App\Models\Pemesanan;
use App\Repositories\PemesananRepository;

trait MakePemesananTrait
{
    /**
     * Create fake instance of Pemesanan and save it in database
     *
     * @param array $pemesananFields
     * @return Pemesanan
     */
    public function makePemesanan($pemesananFields = [])
    {
        /** @var PemesananRepository $pemesananRepo */
        $pemesananRepo = App::make(PemesananRepository::class);
        $theme = $this->fakePemesananData($pemesananFields);
        return $pemesananRepo->create($theme);
    }

    /**
     * Get fake instance of Pemesanan
     *
     * @param array $pemesananFields
     * @return Pemesanan
     */
    public function fakePemesanan($pemesananFields = [])
    {
        return new Pemesanan($this->fakePemesananData($pemesananFields));
    }

    /**
     * Get fake data of Pemesanan
     *
     * @param array $postFields
     * @return array
     */
    public function fakePemesananData($pemesananFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tglPesan' => $fake->word,
            'email' => $fake->word,
            'idPaket' => $fake->word,
            'expired' => $fake->date('Y-m-d H:i:s'),
            'idStatusPemesanan' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $pemesananFields);
    }
}
