<?php

use Faker\Factory as Faker;
use App\Models\Mhotel;
use App\Repositories\MhotelRepository;

trait MakeMhotelTrait
{
    /**
     * Create fake instance of Mhotel and save it in database
     *
     * @param array $mhotelFields
     * @return Mhotel
     */
    public function makeMhotel($mhotelFields = [])
    {
        /** @var MhotelRepository $mhotelRepo */
        $mhotelRepo = App::make(MhotelRepository::class);
        $theme = $this->fakeMhotelData($mhotelFields);
        return $mhotelRepo->create($theme);
    }

    /**
     * Get fake instance of Mhotel
     *
     * @param array $mhotelFields
     * @return Mhotel
     */
    public function fakeMhotel($mhotelFields = [])
    {
        return new Mhotel($this->fakeMhotelData($mhotelFields));
    }

    /**
     * Get fake data of Mhotel
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMhotelData($mhotelFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'keterangan' => $fake->text,
            'bintang' => $fake->word,
            'kodeProv' => $fake->word,
            'kodeKab' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mhotelFields);
    }
}
