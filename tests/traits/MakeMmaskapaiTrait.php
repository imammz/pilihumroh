<?php

use Faker\Factory as Faker;
use App\Models\Mmaskapai;
use App\Repositories\MmaskapaiRepository;

trait MakeMmaskapaiTrait
{
    /**
     * Create fake instance of Mmaskapai and save it in database
     *
     * @param array $mmaskapaiFields
     * @return Mmaskapai
     */
    public function makeMmaskapai($mmaskapaiFields = [])
    {
        /** @var MmaskapaiRepository $mmaskapaiRepo */
        $mmaskapaiRepo = App::make(MmaskapaiRepository::class);
        $theme = $this->fakeMmaskapaiData($mmaskapaiFields);
        return $mmaskapaiRepo->create($theme);
    }

    /**
     * Get fake instance of Mmaskapai
     *
     * @param array $mmaskapaiFields
     * @return Mmaskapai
     */
    public function fakeMmaskapai($mmaskapaiFields = [])
    {
        return new Mmaskapai($this->fakeMmaskapaiData($mmaskapaiFields));
    }

    /**
     * Get fake data of Mmaskapai
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMmaskapaiData($mmaskapaiFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'namaMaskapai' => $fake->word,
            'keterangan' => $fake->text,
            'kelas' => $fake->word,
            'kodeProv' => $fake->word,
            'kodeKab' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word
        ], $mmaskapaiFields);
    }
}
