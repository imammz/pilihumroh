<?php

use Faker\Factory as Faker;
use App\Models\Content;
use App\Repositories\ContentRepository;

trait MakeContentTrait
{
    /**
     * Create fake instance of Content and save it in database
     *
     * @param array $contentFields
     * @return Content
     */
    public function makeContent($contentFields = [])
    {
        /** @var ContentRepository $contentRepo */
        $contentRepo = App::make(ContentRepository::class);
        $theme = $this->fakeContentData($contentFields);
        return $contentRepo->create($theme);
    }

    /**
     * Get fake instance of Content
     *
     * @param array $contentFields
     * @return Content
     */
    public function fakeContent($contentFields = [])
    {
        return new Content($this->fakeContentData($contentFields));
    }

    /**
     * Get fake data of Content
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContentData($contentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'judul' => $fake->word,
            'isi' => $fake->text,
            'thumbnail' => $fake->word,
            'staPublish' => $fake->word,
            'tglTerbit' => $fake->word,
            'idContentCategory' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'created_by' => $fake->word,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'updated_by' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_by' => $fake->word,
            'highlight' => $fake->word,
            'author' => $fake->word
        ], $contentFields);
    }
}
