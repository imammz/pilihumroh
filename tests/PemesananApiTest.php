<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PemesananApiTest extends TestCase
{
    use MakePemesananTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePemesanan()
    {
        $pemesanan = $this->fakePemesananData();
        $this->json('POST', '/api/v1/pemesanans', $pemesanan);

        $this->assertApiResponse($pemesanan);
    }

    /**
     * @test
     */
    public function testReadPemesanan()
    {
        $pemesanan = $this->makePemesanan();
        $this->json('GET', '/api/v1/pemesanans/'.$pemesanan->idPemesanan);

        $this->assertApiResponse($pemesanan->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePemesanan()
    {
        $pemesanan = $this->makePemesanan();
        $editedPemesanan = $this->fakePemesananData();

        $this->json('PUT', '/api/v1/pemesanans/'.$pemesanan->idPemesanan, $editedPemesanan);

        $this->assertApiResponse($editedPemesanan);
    }

    /**
     * @test
     */
    public function testDeletePemesanan()
    {
        $pemesanan = $this->makePemesanan();
        $this->json('DELETE', '/api/v1/pemesanans/'.$pemesanan->idPemesanan);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pemesanans/'.$pemesanan->idPemesanan);

        $this->assertResponseStatus(404);
    }
}
