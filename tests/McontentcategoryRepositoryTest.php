<?php

use App\Models\Mcontentcategory;
use App\Repositories\McontentcategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class McontentcategoryRepositoryTest extends TestCase
{
    use MakeMcontentcategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var McontentcategoryRepository
     */
    protected $mcontentcategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mcontentcategoryRepo = App::make(McontentcategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMcontentcategory()
    {
        $mcontentcategory = $this->fakeMcontentcategoryData();
        $createdMcontentcategory = $this->mcontentcategoryRepo->create($mcontentcategory);
        $createdMcontentcategory = $createdMcontentcategory->toArray();
        $this->assertArrayHasKey('id', $createdMcontentcategory);
        $this->assertNotNull($createdMcontentcategory['id'], 'Created Mcontentcategory must have id specified');
        $this->assertNotNull(Mcontentcategory::find($createdMcontentcategory['id']), 'Mcontentcategory with given id must be in DB');
        $this->assertModelData($mcontentcategory, $createdMcontentcategory);
    }

    /**
     * @test read
     */
    public function testReadMcontentcategory()
    {
        $mcontentcategory = $this->makeMcontentcategory();
        $dbMcontentcategory = $this->mcontentcategoryRepo->find($mcontentcategory->idContentCategory);
        $dbMcontentcategory = $dbMcontentcategory->toArray();
        $this->assertModelData($mcontentcategory->toArray(), $dbMcontentcategory);
    }

    /**
     * @test update
     */
    public function testUpdateMcontentcategory()
    {
        $mcontentcategory = $this->makeMcontentcategory();
        $fakeMcontentcategory = $this->fakeMcontentcategoryData();
        $updatedMcontentcategory = $this->mcontentcategoryRepo->update($fakeMcontentcategory, $mcontentcategory->idContentCategory);
        $this->assertModelData($fakeMcontentcategory, $updatedMcontentcategory->toArray());
        $dbMcontentcategory = $this->mcontentcategoryRepo->find($mcontentcategory->idContentCategory);
        $this->assertModelData($fakeMcontentcategory, $dbMcontentcategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMcontentcategory()
    {
        $mcontentcategory = $this->makeMcontentcategory();
        $resp = $this->mcontentcategoryRepo->delete($mcontentcategory->idContentCategory);
        $this->assertTrue($resp);
        $this->assertNull(Mcontentcategory::find($mcontentcategory->idContentCategory), 'Mcontentcategory should not exist in DB');
    }
}
