<?php

use App\Models\Pemesanan;
use App\Repositories\PemesananRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PemesananRepositoryTest extends TestCase
{
    use MakePemesananTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PemesananRepository
     */
    protected $pemesananRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pemesananRepo = App::make(PemesananRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePemesanan()
    {
        $pemesanan = $this->fakePemesananData();
        $createdPemesanan = $this->pemesananRepo->create($pemesanan);
        $createdPemesanan = $createdPemesanan->toArray();
        $this->assertArrayHasKey('id', $createdPemesanan);
        $this->assertNotNull($createdPemesanan['id'], 'Created Pemesanan must have id specified');
        $this->assertNotNull(Pemesanan::find($createdPemesanan['id']), 'Pemesanan with given id must be in DB');
        $this->assertModelData($pemesanan, $createdPemesanan);
    }

    /**
     * @test read
     */
    public function testReadPemesanan()
    {
        $pemesanan = $this->makePemesanan();
        $dbPemesanan = $this->pemesananRepo->find($pemesanan->idPemesanan);
        $dbPemesanan = $dbPemesanan->toArray();
        $this->assertModelData($pemesanan->toArray(), $dbPemesanan);
    }

    /**
     * @test update
     */
    public function testUpdatePemesanan()
    {
        $pemesanan = $this->makePemesanan();
        $fakePemesanan = $this->fakePemesananData();
        $updatedPemesanan = $this->pemesananRepo->update($fakePemesanan, $pemesanan->idPemesanan);
        $this->assertModelData($fakePemesanan, $updatedPemesanan->toArray());
        $dbPemesanan = $this->pemesananRepo->find($pemesanan->idPemesanan);
        $this->assertModelData($fakePemesanan, $dbPemesanan->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePemesanan()
    {
        $pemesanan = $this->makePemesanan();
        $resp = $this->pemesananRepo->delete($pemesanan->idPemesanan);
        $this->assertTrue($resp);
        $this->assertNull(Pemesanan::find($pemesanan->idPemesanan), 'Pemesanan should not exist in DB');
    }
}
