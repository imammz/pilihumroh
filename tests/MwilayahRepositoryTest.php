<?php

use App\Models\Mwilayah;
use App\Repositories\MwilayahRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MwilayahRepositoryTest extends TestCase
{
    use MakeMwilayahTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MwilayahRepository
     */
    protected $mwilayahRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mwilayahRepo = App::make(MwilayahRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMwilayah()
    {
        $mwilayah = $this->fakeMwilayahData();
        $createdMwilayah = $this->mwilayahRepo->create($mwilayah);
        $createdMwilayah = $createdMwilayah->toArray();
        $this->assertArrayHasKey('id', $createdMwilayah);
        $this->assertNotNull($createdMwilayah['id'], 'Created Mwilayah must have id specified');
        $this->assertNotNull(Mwilayah::find($createdMwilayah['id']), 'Mwilayah with given id must be in DB');
        $this->assertModelData($mwilayah, $createdMwilayah);
    }

    /**
     * @test read
     */
    public function testReadMwilayah()
    {
        $mwilayah = $this->makeMwilayah();
        $dbMwilayah = $this->mwilayahRepo->find($mwilayah->kodeProv);
        $dbMwilayah = $dbMwilayah->toArray();
        $this->assertModelData($mwilayah->toArray(), $dbMwilayah);
    }

    /**
     * @test update
     */
    public function testUpdateMwilayah()
    {
        $mwilayah = $this->makeMwilayah();
        $fakeMwilayah = $this->fakeMwilayahData();
        $updatedMwilayah = $this->mwilayahRepo->update($fakeMwilayah, $mwilayah->kodeProv);
        $this->assertModelData($fakeMwilayah, $updatedMwilayah->toArray());
        $dbMwilayah = $this->mwilayahRepo->find($mwilayah->kodeProv);
        $this->assertModelData($fakeMwilayah, $dbMwilayah->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMwilayah()
    {
        $mwilayah = $this->makeMwilayah();
        $resp = $this->mwilayahRepo->delete($mwilayah->kodeProv);
        $this->assertTrue($resp);
        $this->assertNull(Mwilayah::find($mwilayah->kodeProv), 'Mwilayah should not exist in DB');
    }
}
