<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MwilayahApiTest extends TestCase
{
    use MakeMwilayahTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMwilayah()
    {
        $mwilayah = $this->fakeMwilayahData();
        $this->json('POST', '/api/v1/mwilayahs', $mwilayah);

        $this->assertApiResponse($mwilayah);
    }

    /**
     * @test
     */
    public function testReadMwilayah()
    {
        $mwilayah = $this->makeMwilayah();
        $this->json('GET', '/api/v1/mwilayahs/'.$mwilayah->kodeProv);

        $this->assertApiResponse($mwilayah->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMwilayah()
    {
        $mwilayah = $this->makeMwilayah();
        $editedMwilayah = $this->fakeMwilayahData();

        $this->json('PUT', '/api/v1/mwilayahs/'.$mwilayah->kodeProv, $editedMwilayah);

        $this->assertApiResponse($editedMwilayah);
    }

    /**
     * @test
     */
    public function testDeleteMwilayah()
    {
        $mwilayah = $this->makeMwilayah();
        $this->json('DELETE', '/api/v1/mwilayahs/'.$mwilayah->kodeProv);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mwilayahs/'.$mwilayah->kodeProv);

        $this->assertResponseStatus(404);
    }
}
