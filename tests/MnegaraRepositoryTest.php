<?php

use App\Models\Mnegara;
use App\Repositories\MnegaraRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MnegaraRepositoryTest extends TestCase
{
    use MakeMnegaraTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MnegaraRepository
     */
    protected $mnegaraRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mnegaraRepo = App::make(MnegaraRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMnegara()
    {
        $mnegara = $this->fakeMnegaraData();
        $createdMnegara = $this->mnegaraRepo->create($mnegara);
        $createdMnegara = $createdMnegara->toArray();
        $this->assertArrayHasKey('id', $createdMnegara);
        $this->assertNotNull($createdMnegara['id'], 'Created Mnegara must have id specified');
        $this->assertNotNull(Mnegara::find($createdMnegara['id']), 'Mnegara with given id must be in DB');
        $this->assertModelData($mnegara, $createdMnegara);
    }

    /**
     * @test read
     */
    public function testReadMnegara()
    {
        $mnegara = $this->makeMnegara();
        $dbMnegara = $this->mnegaraRepo->find($mnegara->idNegara);
        $dbMnegara = $dbMnegara->toArray();
        $this->assertModelData($mnegara->toArray(), $dbMnegara);
    }

    /**
     * @test update
     */
    public function testUpdateMnegara()
    {
        $mnegara = $this->makeMnegara();
        $fakeMnegara = $this->fakeMnegaraData();
        $updatedMnegara = $this->mnegaraRepo->update($fakeMnegara, $mnegara->idNegara);
        $this->assertModelData($fakeMnegara, $updatedMnegara->toArray());
        $dbMnegara = $this->mnegaraRepo->find($mnegara->idNegara);
        $this->assertModelData($fakeMnegara, $dbMnegara->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMnegara()
    {
        $mnegara = $this->makeMnegara();
        $resp = $this->mnegaraRepo->delete($mnegara->idNegara);
        $this->assertTrue($resp);
        $this->assertNull(Mnegara::find($mnegara->idNegara), 'Mnegara should not exist in DB');
    }
}
