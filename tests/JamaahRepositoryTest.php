<?php

use App\Models\Jamaah;
use App\Repositories\JamaahRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JamaahRepositoryTest extends TestCase
{
    use MakeJamaahTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var JamaahRepository
     */
    protected $jamaahRepo;

    public function setUp()
    {
        parent::setUp();
        $this->jamaahRepo = App::make(JamaahRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateJamaah()
    {
        $jamaah = $this->fakeJamaahData();
        $createdJamaah = $this->jamaahRepo->create($jamaah);
        $createdJamaah = $createdJamaah->toArray();
        $this->assertArrayHasKey('id', $createdJamaah);
        $this->assertNotNull($createdJamaah['id'], 'Created Jamaah must have id specified');
        $this->assertNotNull(Jamaah::find($createdJamaah['id']), 'Jamaah with given id must be in DB');
        $this->assertModelData($jamaah, $createdJamaah);
    }

    /**
     * @test read
     */
    public function testReadJamaah()
    {
        $jamaah = $this->makeJamaah();
        $dbJamaah = $this->jamaahRepo->find($jamaah->idJamaah);
        $dbJamaah = $dbJamaah->toArray();
        $this->assertModelData($jamaah->toArray(), $dbJamaah);
    }

    /**
     * @test update
     */
    public function testUpdateJamaah()
    {
        $jamaah = $this->makeJamaah();
        $fakeJamaah = $this->fakeJamaahData();
        $updatedJamaah = $this->jamaahRepo->update($fakeJamaah, $jamaah->idJamaah);
        $this->assertModelData($fakeJamaah, $updatedJamaah->toArray());
        $dbJamaah = $this->jamaahRepo->find($jamaah->idJamaah);
        $this->assertModelData($fakeJamaah, $dbJamaah->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteJamaah()
    {
        $jamaah = $this->makeJamaah();
        $resp = $this->jamaahRepo->delete($jamaah->idJamaah);
        $this->assertTrue($resp);
        $this->assertNull(Jamaah::find($jamaah->idJamaah), 'Jamaah should not exist in DB');
    }
}
