<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MjenisizinApiTest extends TestCase
{
    use MakeMjenisizinTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMjenisizin()
    {
        $mjenisizin = $this->fakeMjenisizinData();
        $this->json('POST', '/api/v1/mjenisizins', $mjenisizin);

        $this->assertApiResponse($mjenisizin);
    }

    /**
     * @test
     */
    public function testReadMjenisizin()
    {
        $mjenisizin = $this->makeMjenisizin();
        $this->json('GET', '/api/v1/mjenisizins/'.$mjenisizin->idJenisIzin);

        $this->assertApiResponse($mjenisizin->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMjenisizin()
    {
        $mjenisizin = $this->makeMjenisizin();
        $editedMjenisizin = $this->fakeMjenisizinData();

        $this->json('PUT', '/api/v1/mjenisizins/'.$mjenisizin->idJenisIzin, $editedMjenisizin);

        $this->assertApiResponse($editedMjenisizin);
    }

    /**
     * @test
     */
    public function testDeleteMjenisizin()
    {
        $mjenisizin = $this->makeMjenisizin();
        $this->json('DELETE', '/api/v1/mjenisizins/'.$mjenisizin->idJenisIzin);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mjenisizins/'.$mjenisizin->idJenisIzin);

        $this->assertResponseStatus(404);
    }
}
