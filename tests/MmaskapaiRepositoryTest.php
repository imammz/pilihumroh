<?php

use App\Models\Mmaskapai;
use App\Repositories\MmaskapaiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MmaskapaiRepositoryTest extends TestCase
{
    use MakeMmaskapaiTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MmaskapaiRepository
     */
    protected $mmaskapaiRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mmaskapaiRepo = App::make(MmaskapaiRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMmaskapai()
    {
        $mmaskapai = $this->fakeMmaskapaiData();
        $createdMmaskapai = $this->mmaskapaiRepo->create($mmaskapai);
        $createdMmaskapai = $createdMmaskapai->toArray();
        $this->assertArrayHasKey('id', $createdMmaskapai);
        $this->assertNotNull($createdMmaskapai['id'], 'Created Mmaskapai must have id specified');
        $this->assertNotNull(Mmaskapai::find($createdMmaskapai['id']), 'Mmaskapai with given id must be in DB');
        $this->assertModelData($mmaskapai, $createdMmaskapai);
    }

    /**
     * @test read
     */
    public function testReadMmaskapai()
    {
        $mmaskapai = $this->makeMmaskapai();
        $dbMmaskapai = $this->mmaskapaiRepo->find($mmaskapai->idMaskapai);
        $dbMmaskapai = $dbMmaskapai->toArray();
        $this->assertModelData($mmaskapai->toArray(), $dbMmaskapai);
    }

    /**
     * @test update
     */
    public function testUpdateMmaskapai()
    {
        $mmaskapai = $this->makeMmaskapai();
        $fakeMmaskapai = $this->fakeMmaskapaiData();
        $updatedMmaskapai = $this->mmaskapaiRepo->update($fakeMmaskapai, $mmaskapai->idMaskapai);
        $this->assertModelData($fakeMmaskapai, $updatedMmaskapai->toArray());
        $dbMmaskapai = $this->mmaskapaiRepo->find($mmaskapai->idMaskapai);
        $this->assertModelData($fakeMmaskapai, $dbMmaskapai->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMmaskapai()
    {
        $mmaskapai = $this->makeMmaskapai();
        $resp = $this->mmaskapaiRepo->delete($mmaskapai->idMaskapai);
        $this->assertTrue($resp);
        $this->assertNull(Mmaskapai::find($mmaskapai->idMaskapai), 'Mmaskapai should not exist in DB');
    }
}
