<?php

use App\Models\Paket;
use App\Repositories\PaketRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaketRepositoryTest extends TestCase
{
    use MakePaketTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaketRepository
     */
    protected $paketRepo;

    public function setUp()
    {
        parent::setUp();
        $this->paketRepo = App::make(PaketRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePaket()
    {
        $paket = $this->fakePaketData();
        $createdPaket = $this->paketRepo->create($paket);
        $createdPaket = $createdPaket->toArray();
        $this->assertArrayHasKey('id', $createdPaket);
        $this->assertNotNull($createdPaket['id'], 'Created Paket must have id specified');
        $this->assertNotNull(Paket::find($createdPaket['id']), 'Paket with given id must be in DB');
        $this->assertModelData($paket, $createdPaket);
    }

    /**
     * @test read
     */
    public function testReadPaket()
    {
        $paket = $this->makePaket();
        $dbPaket = $this->paketRepo->find($paket->idPaket);
        $dbPaket = $dbPaket->toArray();
        $this->assertModelData($paket->toArray(), $dbPaket);
    }

    /**
     * @test update
     */
    public function testUpdatePaket()
    {
        $paket = $this->makePaket();
        $fakePaket = $this->fakePaketData();
        $updatedPaket = $this->paketRepo->update($fakePaket, $paket->idPaket);
        $this->assertModelData($fakePaket, $updatedPaket->toArray());
        $dbPaket = $this->paketRepo->find($paket->idPaket);
        $this->assertModelData($fakePaket, $dbPaket->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePaket()
    {
        $paket = $this->makePaket();
        $resp = $this->paketRepo->delete($paket->idPaket);
        $this->assertTrue($resp);
        $this->assertNull(Paket::find($paket->idPaket), 'Paket should not exist in DB');
    }
}
