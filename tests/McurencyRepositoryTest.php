<?php

use App\Models\Mcurency;
use App\Repositories\McurencyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class McurencyRepositoryTest extends TestCase
{
    use MakeMcurencyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var McurencyRepository
     */
    protected $mcurencyRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mcurencyRepo = App::make(McurencyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMcurency()
    {
        $mcurency = $this->fakeMcurencyData();
        $createdMcurency = $this->mcurencyRepo->create($mcurency);
        $createdMcurency = $createdMcurency->toArray();
        $this->assertArrayHasKey('id', $createdMcurency);
        $this->assertNotNull($createdMcurency['id'], 'Created Mcurency must have id specified');
        $this->assertNotNull(Mcurency::find($createdMcurency['id']), 'Mcurency with given id must be in DB');
        $this->assertModelData($mcurency, $createdMcurency);
    }

    /**
     * @test read
     */
    public function testReadMcurency()
    {
        $mcurency = $this->makeMcurency();
        $dbMcurency = $this->mcurencyRepo->find($mcurency->idCurency);
        $dbMcurency = $dbMcurency->toArray();
        $this->assertModelData($mcurency->toArray(), $dbMcurency);
    }

    /**
     * @test update
     */
    public function testUpdateMcurency()
    {
        $mcurency = $this->makeMcurency();
        $fakeMcurency = $this->fakeMcurencyData();
        $updatedMcurency = $this->mcurencyRepo->update($fakeMcurency, $mcurency->idCurency);
        $this->assertModelData($fakeMcurency, $updatedMcurency->toArray());
        $dbMcurency = $this->mcurencyRepo->find($mcurency->idCurency);
        $this->assertModelData($fakeMcurency, $dbMcurency->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMcurency()
    {
        $mcurency = $this->makeMcurency();
        $resp = $this->mcurencyRepo->delete($mcurency->idCurency);
        $this->assertTrue($resp);
        $this->assertNull(Mcurency::find($mcurency->idCurency), 'Mcurency should not exist in DB');
    }
}
