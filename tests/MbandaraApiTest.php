<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MbandaraApiTest extends TestCase
{
    use MakeMbandaraTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMbandara()
    {
        $mbandara = $this->fakeMbandaraData();
        $this->json('POST', '/api/v1/mbandaras', $mbandara);

        $this->assertApiResponse($mbandara);
    }

    /**
     * @test
     */
    public function testReadMbandara()
    {
        $mbandara = $this->makeMbandara();
        $this->json('GET', '/api/v1/mbandaras/'.$mbandara->kode);

        $this->assertApiResponse($mbandara->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMbandara()
    {
        $mbandara = $this->makeMbandara();
        $editedMbandara = $this->fakeMbandaraData();

        $this->json('PUT', '/api/v1/mbandaras/'.$mbandara->kode, $editedMbandara);

        $this->assertApiResponse($editedMbandara);
    }

    /**
     * @test
     */
    public function testDeleteMbandara()
    {
        $mbandara = $this->makeMbandara();
        $this->json('DELETE', '/api/v1/mbandaras/'.$mbandara->kode);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mbandaras/'.$mbandara->kode);

        $this->assertResponseStatus(404);
    }
}
