<?php

use App\Models\Mjenisizin;
use App\Repositories\MjenisizinRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MjenisizinRepositoryTest extends TestCase
{
    use MakeMjenisizinTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MjenisizinRepository
     */
    protected $mjenisizinRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mjenisizinRepo = App::make(MjenisizinRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMjenisizin()
    {
        $mjenisizin = $this->fakeMjenisizinData();
        $createdMjenisizin = $this->mjenisizinRepo->create($mjenisizin);
        $createdMjenisizin = $createdMjenisizin->toArray();
        $this->assertArrayHasKey('id', $createdMjenisizin);
        $this->assertNotNull($createdMjenisizin['id'], 'Created Mjenisizin must have id specified');
        $this->assertNotNull(Mjenisizin::find($createdMjenisizin['id']), 'Mjenisizin with given id must be in DB');
        $this->assertModelData($mjenisizin, $createdMjenisizin);
    }

    /**
     * @test read
     */
    public function testReadMjenisizin()
    {
        $mjenisizin = $this->makeMjenisizin();
        $dbMjenisizin = $this->mjenisizinRepo->find($mjenisizin->idJenisIzin);
        $dbMjenisizin = $dbMjenisizin->toArray();
        $this->assertModelData($mjenisizin->toArray(), $dbMjenisizin);
    }

    /**
     * @test update
     */
    public function testUpdateMjenisizin()
    {
        $mjenisizin = $this->makeMjenisizin();
        $fakeMjenisizin = $this->fakeMjenisizinData();
        $updatedMjenisizin = $this->mjenisizinRepo->update($fakeMjenisizin, $mjenisizin->idJenisIzin);
        $this->assertModelData($fakeMjenisizin, $updatedMjenisizin->toArray());
        $dbMjenisizin = $this->mjenisizinRepo->find($mjenisizin->idJenisIzin);
        $this->assertModelData($fakeMjenisizin, $dbMjenisizin->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMjenisizin()
    {
        $mjenisizin = $this->makeMjenisizin();
        $resp = $this->mjenisizinRepo->delete($mjenisizin->idJenisIzin);
        $this->assertTrue($resp);
        $this->assertNull(Mjenisizin::find($mjenisizin->idJenisIzin), 'Mjenisizin should not exist in DB');
    }
}
