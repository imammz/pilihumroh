<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JamaahApiTest extends TestCase
{
    use MakeJamaahTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateJamaah()
    {
        $jamaah = $this->fakeJamaahData();
        $this->json('POST', '/api/v1/jamaahs', $jamaah);

        $this->assertApiResponse($jamaah);
    }

    /**
     * @test
     */
    public function testReadJamaah()
    {
        $jamaah = $this->makeJamaah();
        $this->json('GET', '/api/v1/jamaahs/'.$jamaah->idJamaah);

        $this->assertApiResponse($jamaah->toArray());
    }

    /**
     * @test
     */
    public function testUpdateJamaah()
    {
        $jamaah = $this->makeJamaah();
        $editedJamaah = $this->fakeJamaahData();

        $this->json('PUT', '/api/v1/jamaahs/'.$jamaah->idJamaah, $editedJamaah);

        $this->assertApiResponse($editedJamaah);
    }

    /**
     * @test
     */
    public function testDeleteJamaah()
    {
        $jamaah = $this->makeJamaah();
        $this->json('DELETE', '/api/v1/jamaahs/'.$jamaah->idJamaah);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/jamaahs/'.$jamaah->idJamaah);

        $this->assertResponseStatus(404);
    }
}
