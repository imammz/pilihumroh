<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MroleApiTest extends TestCase
{
    use MakeMroleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMrole()
    {
        $mrole = $this->fakeMroleData();
        $this->json('POST', '/api/v1/mroles', $mrole);

        $this->assertApiResponse($mrole);
    }

    /**
     * @test
     */
    public function testReadMrole()
    {
        $mrole = $this->makeMrole();
        $this->json('GET', '/api/v1/mroles/'.$mrole->idRole);

        $this->assertApiResponse($mrole->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMrole()
    {
        $mrole = $this->makeMrole();
        $editedMrole = $this->fakeMroleData();

        $this->json('PUT', '/api/v1/mroles/'.$mrole->idRole, $editedMrole);

        $this->assertApiResponse($editedMrole);
    }

    /**
     * @test
     */
    public function testDeleteMrole()
    {
        $mrole = $this->makeMrole();
        $this->json('DELETE', '/api/v1/mroles/'.$mrole->idRole);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mroles/'.$mrole->idRole);

        $this->assertResponseStatus(404);
    }
}
