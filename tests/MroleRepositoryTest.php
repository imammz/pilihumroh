<?php

use App\Models\Mrole;
use App\Repositories\MroleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MroleRepositoryTest extends TestCase
{
    use MakeMroleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MroleRepository
     */
    protected $mroleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mroleRepo = App::make(MroleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMrole()
    {
        $mrole = $this->fakeMroleData();
        $createdMrole = $this->mroleRepo->create($mrole);
        $createdMrole = $createdMrole->toArray();
        $this->assertArrayHasKey('id', $createdMrole);
        $this->assertNotNull($createdMrole['id'], 'Created Mrole must have id specified');
        $this->assertNotNull(Mrole::find($createdMrole['id']), 'Mrole with given id must be in DB');
        $this->assertModelData($mrole, $createdMrole);
    }

    /**
     * @test read
     */
    public function testReadMrole()
    {
        $mrole = $this->makeMrole();
        $dbMrole = $this->mroleRepo->find($mrole->idRole);
        $dbMrole = $dbMrole->toArray();
        $this->assertModelData($mrole->toArray(), $dbMrole);
    }

    /**
     * @test update
     */
    public function testUpdateMrole()
    {
        $mrole = $this->makeMrole();
        $fakeMrole = $this->fakeMroleData();
        $updatedMrole = $this->mroleRepo->update($fakeMrole, $mrole->idRole);
        $this->assertModelData($fakeMrole, $updatedMrole->toArray());
        $dbMrole = $this->mroleRepo->find($mrole->idRole);
        $this->assertModelData($fakeMrole, $dbMrole->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMrole()
    {
        $mrole = $this->makeMrole();
        $resp = $this->mroleRepo->delete($mrole->idRole);
        $this->assertTrue($resp);
        $this->assertNull(Mrole::find($mrole->idRole), 'Mrole should not exist in DB');
    }
}
