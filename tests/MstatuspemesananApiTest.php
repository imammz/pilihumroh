<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MstatuspemesananApiTest extends TestCase
{
    use MakeMstatuspemesananTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMstatuspemesanan()
    {
        $mstatuspemesanan = $this->fakeMstatuspemesananData();
        $this->json('POST', '/api/v1/mstatuspemesanans', $mstatuspemesanan);

        $this->assertApiResponse($mstatuspemesanan);
    }

    /**
     * @test
     */
    public function testReadMstatuspemesanan()
    {
        $mstatuspemesanan = $this->makeMstatuspemesanan();
        $this->json('GET', '/api/v1/mstatuspemesanans/'.$mstatuspemesanan->idStatusPemesanan);

        $this->assertApiResponse($mstatuspemesanan->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMstatuspemesanan()
    {
        $mstatuspemesanan = $this->makeMstatuspemesanan();
        $editedMstatuspemesanan = $this->fakeMstatuspemesananData();

        $this->json('PUT', '/api/v1/mstatuspemesanans/'.$mstatuspemesanan->idStatusPemesanan, $editedMstatuspemesanan);

        $this->assertApiResponse($editedMstatuspemesanan);
    }

    /**
     * @test
     */
    public function testDeleteMstatuspemesanan()
    {
        $mstatuspemesanan = $this->makeMstatuspemesanan();
        $this->json('DELETE', '/api/v1/mstatuspemesanans/'.$mstatuspemesanan->idStatusPemesanan);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mstatuspemesanans/'.$mstatuspemesanan->idStatusPemesanan);

        $this->assertResponseStatus(404);
    }
}
