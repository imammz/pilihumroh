<?php

use App\Models\Jenispaket;
use App\Repositories\JenispaketRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JenispaketRepositoryTest extends TestCase
{
    use MakeJenispaketTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var JenispaketRepository
     */
    protected $jenispaketRepo;

    public function setUp()
    {
        parent::setUp();
        $this->jenispaketRepo = App::make(JenispaketRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateJenispaket()
    {
        $jenispaket = $this->fakeJenispaketData();
        $createdJenispaket = $this->jenispaketRepo->create($jenispaket);
        $createdJenispaket = $createdJenispaket->toArray();
        $this->assertArrayHasKey('id', $createdJenispaket);
        $this->assertNotNull($createdJenispaket['id'], 'Created Jenispaket must have id specified');
        $this->assertNotNull(Jenispaket::find($createdJenispaket['id']), 'Jenispaket with given id must be in DB');
        $this->assertModelData($jenispaket, $createdJenispaket);
    }

    /**
     * @test read
     */
    public function testReadJenispaket()
    {
        $jenispaket = $this->makeJenispaket();
        $dbJenispaket = $this->jenispaketRepo->find($jenispaket->idJenisPaket);
        $dbJenispaket = $dbJenispaket->toArray();
        $this->assertModelData($jenispaket->toArray(), $dbJenispaket);
    }

    /**
     * @test update
     */
    public function testUpdateJenispaket()
    {
        $jenispaket = $this->makeJenispaket();
        $fakeJenispaket = $this->fakeJenispaketData();
        $updatedJenispaket = $this->jenispaketRepo->update($fakeJenispaket, $jenispaket->idJenisPaket);
        $this->assertModelData($fakeJenispaket, $updatedJenispaket->toArray());
        $dbJenispaket = $this->jenispaketRepo->find($jenispaket->idJenisPaket);
        $this->assertModelData($fakeJenispaket, $dbJenispaket->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteJenispaket()
    {
        $jenispaket = $this->makeJenispaket();
        $resp = $this->jenispaketRepo->delete($jenispaket->idJenisPaket);
        $this->assertTrue($resp);
        $this->assertNull(Jenispaket::find($jenispaket->idJenisPaket), 'Jenispaket should not exist in DB');
    }
}
