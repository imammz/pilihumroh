<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class McontentcategoryApiTest extends TestCase
{
    use MakeMcontentcategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMcontentcategory()
    {
        $mcontentcategory = $this->fakeMcontentcategoryData();
        $this->json('POST', '/api/v1/mcontentcategories', $mcontentcategory);

        $this->assertApiResponse($mcontentcategory);
    }

    /**
     * @test
     */
    public function testReadMcontentcategory()
    {
        $mcontentcategory = $this->makeMcontentcategory();
        $this->json('GET', '/api/v1/mcontentcategories/'.$mcontentcategory->idContentCategory);

        $this->assertApiResponse($mcontentcategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMcontentcategory()
    {
        $mcontentcategory = $this->makeMcontentcategory();
        $editedMcontentcategory = $this->fakeMcontentcategoryData();

        $this->json('PUT', '/api/v1/mcontentcategories/'.$mcontentcategory->idContentCategory, $editedMcontentcategory);

        $this->assertApiResponse($editedMcontentcategory);
    }

    /**
     * @test
     */
    public function testDeleteMcontentcategory()
    {
        $mcontentcategory = $this->makeMcontentcategory();
        $this->json('DELETE', '/api/v1/mcontentcategories/'.$mcontentcategory->idContentCategory);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mcontentcategories/'.$mcontentcategory->idContentCategory);

        $this->assertResponseStatus(404);
    }
}
