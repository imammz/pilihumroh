<?php

use App\Models\Mbandara;
use App\Repositories\MbandaraRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MbandaraRepositoryTest extends TestCase
{
    use MakeMbandaraTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MbandaraRepository
     */
    protected $mbandaraRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mbandaraRepo = App::make(MbandaraRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMbandara()
    {
        $mbandara = $this->fakeMbandaraData();
        $createdMbandara = $this->mbandaraRepo->create($mbandara);
        $createdMbandara = $createdMbandara->toArray();
        $this->assertArrayHasKey('id', $createdMbandara);
        $this->assertNotNull($createdMbandara['id'], 'Created Mbandara must have id specified');
        $this->assertNotNull(Mbandara::find($createdMbandara['id']), 'Mbandara with given id must be in DB');
        $this->assertModelData($mbandara, $createdMbandara);
    }

    /**
     * @test read
     */
    public function testReadMbandara()
    {
        $mbandara = $this->makeMbandara();
        $dbMbandara = $this->mbandaraRepo->find($mbandara->kode);
        $dbMbandara = $dbMbandara->toArray();
        $this->assertModelData($mbandara->toArray(), $dbMbandara);
    }

    /**
     * @test update
     */
    public function testUpdateMbandara()
    {
        $mbandara = $this->makeMbandara();
        $fakeMbandara = $this->fakeMbandaraData();
        $updatedMbandara = $this->mbandaraRepo->update($fakeMbandara, $mbandara->kode);
        $this->assertModelData($fakeMbandara, $updatedMbandara->toArray());
        $dbMbandara = $this->mbandaraRepo->find($mbandara->kode);
        $this->assertModelData($fakeMbandara, $dbMbandara->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMbandara()
    {
        $mbandara = $this->makeMbandara();
        $resp = $this->mbandaraRepo->delete($mbandara->kode);
        $this->assertTrue($resp);
        $this->assertNull(Mbandara::find($mbandara->kode), 'Mbandara should not exist in DB');
    }
}
