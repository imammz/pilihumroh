<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MmaskapaiApiTest extends TestCase
{
    use MakeMmaskapaiTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMmaskapai()
    {
        $mmaskapai = $this->fakeMmaskapaiData();
        $this->json('POST', '/api/v1/mmaskapais', $mmaskapai);

        $this->assertApiResponse($mmaskapai);
    }

    /**
     * @test
     */
    public function testReadMmaskapai()
    {
        $mmaskapai = $this->makeMmaskapai();
        $this->json('GET', '/api/v1/mmaskapais/'.$mmaskapai->idMaskapai);

        $this->assertApiResponse($mmaskapai->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMmaskapai()
    {
        $mmaskapai = $this->makeMmaskapai();
        $editedMmaskapai = $this->fakeMmaskapaiData();

        $this->json('PUT', '/api/v1/mmaskapais/'.$mmaskapai->idMaskapai, $editedMmaskapai);

        $this->assertApiResponse($editedMmaskapai);
    }

    /**
     * @test
     */
    public function testDeleteMmaskapai()
    {
        $mmaskapai = $this->makeMmaskapai();
        $this->json('DELETE', '/api/v1/mmaskapais/'.$mmaskapai->idMaskapai);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mmaskapais/'.$mmaskapai->idMaskapai);

        $this->assertResponseStatus(404);
    }
}
