<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaketApiTest extends TestCase
{
    use MakePaketTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePaket()
    {
        $paket = $this->fakePaketData();
        $this->json('POST', '/api/v1/pakets', $paket);

        $this->assertApiResponse($paket);
    }

    /**
     * @test
     */
    public function testReadPaket()
    {
        $paket = $this->makePaket();
        $this->json('GET', '/api/v1/pakets/'.$paket->idPaket);

        $this->assertApiResponse($paket->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePaket()
    {
        $paket = $this->makePaket();
        $editedPaket = $this->fakePaketData();

        $this->json('PUT', '/api/v1/pakets/'.$paket->idPaket, $editedPaket);

        $this->assertApiResponse($editedPaket);
    }

    /**
     * @test
     */
    public function testDeletePaket()
    {
        $paket = $this->makePaket();
        $this->json('DELETE', '/api/v1/pakets/'.$paket->idPaket);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pakets/'.$paket->idPaket);

        $this->assertResponseStatus(404);
    }
}
