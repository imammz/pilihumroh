<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MpembimbingApiTest extends TestCase
{
    use MakeMpembimbingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMpembimbing()
    {
        $mpembimbing = $this->fakeMpembimbingData();
        $this->json('POST', '/api/v1/mpembimbings', $mpembimbing);

        $this->assertApiResponse($mpembimbing);
    }

    /**
     * @test
     */
    public function testReadMpembimbing()
    {
        $mpembimbing = $this->makeMpembimbing();
        $this->json('GET', '/api/v1/mpembimbings/'.$mpembimbing->idPembimbing);

        $this->assertApiResponse($mpembimbing->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMpembimbing()
    {
        $mpembimbing = $this->makeMpembimbing();
        $editedMpembimbing = $this->fakeMpembimbingData();

        $this->json('PUT', '/api/v1/mpembimbings/'.$mpembimbing->idPembimbing, $editedMpembimbing);

        $this->assertApiResponse($editedMpembimbing);
    }

    /**
     * @test
     */
    public function testDeleteMpembimbing()
    {
        $mpembimbing = $this->makeMpembimbing();
        $this->json('DELETE', '/api/v1/mpembimbings/'.$mpembimbing->idPembimbing);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mpembimbings/'.$mpembimbing->idPembimbing);

        $this->assertResponseStatus(404);
    }
}
