<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class McurencyApiTest extends TestCase
{
    use MakeMcurencyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMcurency()
    {
        $mcurency = $this->fakeMcurencyData();
        $this->json('POST', '/api/v1/mcurencies', $mcurency);

        $this->assertApiResponse($mcurency);
    }

    /**
     * @test
     */
    public function testReadMcurency()
    {
        $mcurency = $this->makeMcurency();
        $this->json('GET', '/api/v1/mcurencies/'.$mcurency->idCurency);

        $this->assertApiResponse($mcurency->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMcurency()
    {
        $mcurency = $this->makeMcurency();
        $editedMcurency = $this->fakeMcurencyData();

        $this->json('PUT', '/api/v1/mcurencies/'.$mcurency->idCurency, $editedMcurency);

        $this->assertApiResponse($editedMcurency);
    }

    /**
     * @test
     */
    public function testDeleteMcurency()
    {
        $mcurency = $this->makeMcurency();
        $this->json('DELETE', '/api/v1/mcurencies/'.$mcurency->idCurency);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/mcurencies/'.$mcurency->idCurency);

        $this->assertResponseStatus(404);
    }
}
