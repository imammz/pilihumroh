<?php

use App\Models\Travelagent;
use App\Repositories\TravelagentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TravelagentRepositoryTest extends TestCase
{
    use MakeTravelagentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TravelagentRepository
     */
    protected $travelagentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->travelagentRepo = App::make(TravelagentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTravelagent()
    {
        $travelagent = $this->fakeTravelagentData();
        $createdTravelagent = $this->travelagentRepo->create($travelagent);
        $createdTravelagent = $createdTravelagent->toArray();
        $this->assertArrayHasKey('id', $createdTravelagent);
        $this->assertNotNull($createdTravelagent['id'], 'Created Travelagent must have id specified');
        $this->assertNotNull(Travelagent::find($createdTravelagent['id']), 'Travelagent with given id must be in DB');
        $this->assertModelData($travelagent, $createdTravelagent);
    }

    /**
     * @test read
     */
    public function testReadTravelagent()
    {
        $travelagent = $this->makeTravelagent();
        $dbTravelagent = $this->travelagentRepo->find($travelagent->idTravelAgent);
        $dbTravelagent = $dbTravelagent->toArray();
        $this->assertModelData($travelagent->toArray(), $dbTravelagent);
    }

    /**
     * @test update
     */
    public function testUpdateTravelagent()
    {
        $travelagent = $this->makeTravelagent();
        $fakeTravelagent = $this->fakeTravelagentData();
        $updatedTravelagent = $this->travelagentRepo->update($fakeTravelagent, $travelagent->idTravelAgent);
        $this->assertModelData($fakeTravelagent, $updatedTravelagent->toArray());
        $dbTravelagent = $this->travelagentRepo->find($travelagent->idTravelAgent);
        $this->assertModelData($fakeTravelagent, $dbTravelagent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTravelagent()
    {
        $travelagent = $this->makeTravelagent();
        $resp = $this->travelagentRepo->delete($travelagent->idTravelAgent);
        $this->assertTrue($resp);
        $this->assertNull(Travelagent::find($travelagent->idTravelAgent), 'Travelagent should not exist in DB');
    }
}
