<?php

use App\Models\Mstatuspemesanan;
use App\Repositories\MstatuspemesananRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MstatuspemesananRepositoryTest extends TestCase
{
    use MakeMstatuspemesananTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MstatuspemesananRepository
     */
    protected $mstatuspemesananRepo;

    public function setUp()
    {
        parent::setUp();
        $this->mstatuspemesananRepo = App::make(MstatuspemesananRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMstatuspemesanan()
    {
        $mstatuspemesanan = $this->fakeMstatuspemesananData();
        $createdMstatuspemesanan = $this->mstatuspemesananRepo->create($mstatuspemesanan);
        $createdMstatuspemesanan = $createdMstatuspemesanan->toArray();
        $this->assertArrayHasKey('id', $createdMstatuspemesanan);
        $this->assertNotNull($createdMstatuspemesanan['id'], 'Created Mstatuspemesanan must have id specified');
        $this->assertNotNull(Mstatuspemesanan::find($createdMstatuspemesanan['id']), 'Mstatuspemesanan with given id must be in DB');
        $this->assertModelData($mstatuspemesanan, $createdMstatuspemesanan);
    }

    /**
     * @test read
     */
    public function testReadMstatuspemesanan()
    {
        $mstatuspemesanan = $this->makeMstatuspemesanan();
        $dbMstatuspemesanan = $this->mstatuspemesananRepo->find($mstatuspemesanan->idStatusPemesanan);
        $dbMstatuspemesanan = $dbMstatuspemesanan->toArray();
        $this->assertModelData($mstatuspemesanan->toArray(), $dbMstatuspemesanan);
    }

    /**
     * @test update
     */
    public function testUpdateMstatuspemesanan()
    {
        $mstatuspemesanan = $this->makeMstatuspemesanan();
        $fakeMstatuspemesanan = $this->fakeMstatuspemesananData();
        $updatedMstatuspemesanan = $this->mstatuspemesananRepo->update($fakeMstatuspemesanan, $mstatuspemesanan->idStatusPemesanan);
        $this->assertModelData($fakeMstatuspemesanan, $updatedMstatuspemesanan->toArray());
        $dbMstatuspemesanan = $this->mstatuspemesananRepo->find($mstatuspemesanan->idStatusPemesanan);
        $this->assertModelData($fakeMstatuspemesanan, $dbMstatuspemesanan->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMstatuspemesanan()
    {
        $mstatuspemesanan = $this->makeMstatuspemesanan();
        $resp = $this->mstatuspemesananRepo->delete($mstatuspemesanan->idStatusPemesanan);
        $this->assertTrue($resp);
        $this->assertNull(Mstatuspemesanan::find($mstatuspemesanan->idStatusPemesanan), 'Mstatuspemesanan should not exist in DB');
    }
}
