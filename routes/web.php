<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'BerandaController@index');

Route::get('/order', 'OrderController@index');

Route::group(['prefix' => 'paket'], function() {

    Route::get('{$id}/detail', 'PaketController@detail');
    Route::get('list', 'PaketController@list');

    Route::resource('/', 'PaketController')->middleware('auth');
});

Route::group(['prefix' => 'travel'], function() {

    Route::get('{$id}/detail', 'TravelController@detail');
    Route::get('list', 'TravelController@list');

    Route::resource('/', 'TravelController')->middleware('auth');
});

Route::group(['prefix' => 'berita'], function() {

    Route::get('{$id}/detail', 'BeritaController@detail');
    Route::get('list', 'BeritaController@list');

    Route::resource('/', 'BeritaController')->middleware('auth');
});

Route::group(['prefix' => 'testi'], function() {

    Route::get('list', 'TestiController@list');
    Route::get('posting', 'TravelController@posting');
    Route::post('posting', 'TravelController@postingProccess');

    Route::get('/', 'TestiController@index')->middleware('auth');
    Route::put('/aktived', 'TestiController@aktived')->middleware('auth');
    Route::put('/inaktived', 'TestiController@inaktived')->middleware('auth');
    Route::delete('/deleted', 'TestiController@destroy')->middleware('auth');
});


Route::group(['prefix' => 'account'], function() {

    Route::get('/register', 'AccountController@register');
    Route::post('/register', 'AccountController@postRegister');
    Route::get('/login', 'AccountController@login');
    Route::post('/login', 'AccountController@postLogin');

    Route::get('/forget', 'AccountController@forget');
    Route::post('/forget', 'AccountController@postForget');
    Route::get('/verify/{code}', 'AccountController@verify');
    Route::post('/verify', 'AccountController@postVerify');
});


// home pages
Route::post('/subscribe', 'StaticController@subscribe');
Route::get('/cara-memesan', 'StaticController@carapesan');
Route::get('/hubungikami', 'StaticController@hubungikami');
Route::get('/faq', 'StaticController@faq');
Route::get('/about', 'StaticController@about');
Route::get('/syarat', 'StaticController@syarat');
Route::get('/testi', 'StaticController@testi');




Route::group(['middleware' => 'auth'], function() {

    Route::get('/konfirmasi-pembayaran', 'StaticController@konfirmasiPembayaran');
    Route::post('/konfirmasi-pembayaran', 'StaticController@postKonfirmasiPembayaran');
});




Route::group(['prefix' => 'backend'], function() {
// Login Routes
    Route::get('login', [
        'as' => 'login',
        'uses' => 'Auth\LoginController@showLoginForm'
    ]);
    Route::post('login', [
        'as' => '',
        'uses' => 'Auth\LoginController@login'
    ]);
    Route::post('logout', [
        'as' => 'logout',
        'uses' => 'Auth\LoginController@logout'
    ]);


// Registration Routes...
    Route::get('register', [
        'as' => 'register',
        'uses' => 'Auth\RegisterController@showRegistrationForm'
    ]);
    Route::post('register', [
        'as' => '',
        'uses' => 'Auth\RegisterController@register'
    ]);
});
// Password Reset Routes...
    Route::post('password/email', [
        'as' => 'password.email',
        'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
    ]);
    Route::get('password/reset', [
        'as' => 'password.request',
        'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
    ]);
    Route::post('password/reset', [
        'as' => '',
        'uses' => 'Auth\ResetPasswordController@reset'
    ]);
    Route::get('password/reset/{token}', [
        'as' => 'password.reset',
        'uses' => 'Auth\ResetPasswordController@showResetForm'
    ]);








Route::get('/backend/home', 'HomeController@index');





Route::get('backend/mroles', ['as'=> 'backend.mroles.index', 'uses' => 'MroleController@index']);
Route::post('backend/mroles', ['as'=> 'backend.mroles.store', 'uses' => 'MroleController@store']);
Route::get('backend/mroles/create', ['as'=> 'backend.mroles.create', 'uses' => 'MroleController@create']);
Route::put('backend/mroles/{mroles}', ['as'=> 'backend.mroles.update', 'uses' => 'MroleController@update']);
Route::patch('backend/mroles/{mroles}', ['as'=> 'backend.mroles.update', 'uses' => 'MroleController@update']);
Route::delete('backend/mroles/{mroles}', ['as'=> 'backend.mroles.destroy', 'uses' => 'MroleController@destroy']);
Route::get('backend/mroles/{mroles}', ['as'=> 'backend.mroles.show', 'uses' => 'MroleController@show']);
Route::get('backend/mroles/{mroles}/edit', ['as'=> 'backend.mroles.edit', 'uses' => 'MroleController@edit']);


Route::get('backend/users', ['as'=> 'backend.users.index', 'uses' => 'UserController@index']);
Route::post('backend/users', ['as'=> 'backend.users.store', 'uses' => 'UserController@store']);
Route::get('backend/users/create', ['as'=> 'backend.users.create', 'uses' => 'UserController@create']);
Route::put('backend/users/{users}', ['as'=> 'backend.users.update', 'uses' => 'UserController@update']);
Route::patch('backend/users/{users}', ['as'=> 'backend.users.update', 'uses' => 'UserController@update']);
Route::delete('backend/users/{users}', ['as'=> 'backend.users.destroy', 'uses' => 'UserController@destroy']);
Route::get('backend/users/{users}', ['as'=> 'backend.users.show', 'uses' => 'UserController@show']);
Route::get('backend/users/{users}/edit', ['as'=> 'backend.users.edit', 'uses' => 'UserController@edit']);


Route::get('backend/mcontentcategories', ['as'=> 'backend.mcontentcategories.index', 'uses' => 'McontentcategoryController@index']);
Route::post('backend/mcontentcategories', ['as'=> 'backend.mcontentcategories.store', 'uses' => 'McontentcategoryController@store']);
Route::get('backend/mcontentcategories/create', ['as'=> 'backend.mcontentcategories.create', 'uses' => 'McontentcategoryController@create']);
Route::put('backend/mcontentcategories/{mcontentcategories}', ['as'=> 'backend.mcontentcategories.update', 'uses' => 'McontentcategoryController@update']);
Route::patch('backend/mcontentcategories/{mcontentcategories}', ['as'=> 'backend.mcontentcategories.update', 'uses' => 'McontentcategoryController@update']);
Route::delete('backend/mcontentcategories/{mcontentcategories}', ['as'=> 'backend.mcontentcategories.destroy', 'uses' => 'McontentcategoryController@destroy']);
Route::get('backend/mcontentcategories/{mcontentcategories}', ['as'=> 'backend.mcontentcategories.show', 'uses' => 'McontentcategoryController@show']);
Route::get('backend/mcontentcategories/{mcontentcategories}/edit', ['as'=> 'backend.mcontentcategories.edit', 'uses' => 'McontentcategoryController@edit']);





Route::get('backend/mroles', ['as'=> 'backend.mroles.index', 'uses' => 'MroleController@index']);
Route::post('backend/mroles', ['as'=> 'backend.mroles.store', 'uses' => 'MroleController@store']);
Route::get('backend/mroles/create', ['as'=> 'backend.mroles.create', 'uses' => 'MroleController@create']);
Route::put('backend/mroles/{mroles}', ['as'=> 'backend.mroles.update', 'uses' => 'MroleController@update']);
Route::patch('backend/mroles/{mroles}', ['as'=> 'backend.mroles.update', 'uses' => 'MroleController@update']);
Route::delete('backend/mroles/{mroles}', ['as'=> 'backend.mroles.destroy', 'uses' => 'MroleController@destroy']);
Route::get('backend/mroles/{mroles}', ['as'=> 'backend.mroles.show', 'uses' => 'MroleController@show']);
Route::get('backend/mroles/{mroles}/edit', ['as'=> 'backend.mroles.edit', 'uses' => 'MroleController@edit']);




Route::get('backend/contents', ['as'=> 'backend.contents.index', 'uses' => 'ContentController@index']);
Route::post('backend/contents', ['as'=> 'backend.contents.store', 'uses' => 'ContentController@store']);
Route::get('backend/contents/create', ['as'=> 'backend.contents.create', 'uses' => 'ContentController@create']);
Route::put('backend/contents/{contents}', ['as'=> 'backend.contents.update', 'uses' => 'ContentController@update']);
Route::patch('backend/contents/{contents}', ['as'=> 'backend.contents.update', 'uses' => 'ContentController@update']);
Route::delete('backend/contents/{contents}', ['as'=> 'backend.contents.destroy', 'uses' => 'ContentController@destroy']);
Route::get('backend/contents/{contents}', ['as'=> 'backend.contents.show', 'uses' => 'ContentController@show']);
Route::get('backend/contents/{contents}/edit', ['as'=> 'backend.contents.edit', 'uses' => 'ContentController@edit']);


Route::get('backend/mbandaras', ['as'=> 'backend.mbandaras.index', 'uses' => 'MbandaraController@index']);
Route::post('backend/mbandaras', ['as'=> 'backend.mbandaras.store', 'uses' => 'MbandaraController@store']);
Route::get('backend/mbandaras/create', ['as'=> 'backend.mbandaras.create', 'uses' => 'MbandaraController@create']);
Route::put('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.update', 'uses' => 'MbandaraController@update']);
Route::patch('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.update', 'uses' => 'MbandaraController@update']);
Route::delete('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.destroy', 'uses' => 'MbandaraController@destroy']);
Route::get('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.show', 'uses' => 'MbandaraController@show']);
Route::get('backend/mbandaras/{mbandaras}/edit', ['as'=> 'backend.mbandaras.edit', 'uses' => 'MbandaraController@edit']);


Route::get('backend/mbandaras', ['as'=> 'backend.mbandaras.index', 'uses' => 'MbandaraController@index']);
Route::post('backend/mbandaras', ['as'=> 'backend.mbandaras.store', 'uses' => 'MbandaraController@store']);
Route::get('backend/mbandaras/create', ['as'=> 'backend.mbandaras.create', 'uses' => 'MbandaraController@create']);
Route::put('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.update', 'uses' => 'MbandaraController@update']);
Route::patch('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.update', 'uses' => 'MbandaraController@update']);
Route::delete('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.destroy', 'uses' => 'MbandaraController@destroy']);
Route::get('backend/mbandaras/{mbandaras}', ['as'=> 'backend.mbandaras.show', 'uses' => 'MbandaraController@show']);
Route::get('backend/mbandaras/{mbandaras}/edit', ['as'=> 'backend.mbandaras.edit', 'uses' => 'MbandaraController@edit']);


Route::get('backend/jamaahs', ['as'=> 'backend.jamaahs.index', 'uses' => 'JamaahController@index']);
Route::post('backend/jamaahs', ['as'=> 'backend.jamaahs.store', 'uses' => 'JamaahController@store']);
Route::get('backend/jamaahs/create', ['as'=> 'backend.jamaahs.create', 'uses' => 'JamaahController@create']);
Route::put('backend/jamaahs/{jamaahs}', ['as'=> 'backend.jamaahs.update', 'uses' => 'JamaahController@update']);
Route::patch('backend/jamaahs/{jamaahs}', ['as'=> 'backend.jamaahs.update', 'uses' => 'JamaahController@update']);
Route::delete('backend/jamaahs/{jamaahs}', ['as'=> 'backend.jamaahs.destroy', 'uses' => 'JamaahController@destroy']);
Route::get('backend/jamaahs/{jamaahs}', ['as'=> 'backend.jamaahs.show', 'uses' => 'JamaahController@show']);
Route::get('backend/jamaahs/{jamaahs}/edit', ['as'=> 'backend.jamaahs.edit', 'uses' => 'JamaahController@edit']);


Route::get('backend/jenispakets', ['as'=> 'backend.jenispakets.index', 'uses' => 'JenispaketController@index']);
Route::post('backend/jenispakets', ['as'=> 'backend.jenispakets.store', 'uses' => 'JenispaketController@store']);
Route::get('backend/jenispakets/create', ['as'=> 'backend.jenispakets.create', 'uses' => 'JenispaketController@create']);
Route::put('backend/jenispakets/{jenispakets}', ['as'=> 'backend.jenispakets.update', 'uses' => 'JenispaketController@update']);
Route::patch('backend/jenispakets/{jenispakets}', ['as'=> 'backend.jenispakets.update', 'uses' => 'JenispaketController@update']);
Route::delete('backend/jenispakets/{jenispakets}', ['as'=> 'backend.jenispakets.destroy', 'uses' => 'JenispaketController@destroy']);
Route::get('backend/jenispakets/{jenispakets}', ['as'=> 'backend.jenispakets.show', 'uses' => 'JenispaketController@show']);
Route::get('backend/jenispakets/{jenispakets}/edit', ['as'=> 'backend.jenispakets.edit', 'uses' => 'JenispaketController@edit']);




Route::get('backend/mcurencies', ['as'=> 'backend.mcurencies.index', 'uses' => 'McurencyController@index']);
Route::post('backend/mcurencies', ['as'=> 'backend.mcurencies.store', 'uses' => 'McurencyController@store']);
Route::get('backend/mcurencies/create', ['as'=> 'backend.mcurencies.create', 'uses' => 'McurencyController@create']);
Route::put('backend/mcurencies/{mcurencies}', ['as'=> 'backend.mcurencies.update', 'uses' => 'McurencyController@update']);
Route::patch('backend/mcurencies/{mcurencies}', ['as'=> 'backend.mcurencies.update', 'uses' => 'McurencyController@update']);
Route::delete('backend/mcurencies/{mcurencies}', ['as'=> 'backend.mcurencies.destroy', 'uses' => 'McurencyController@destroy']);
Route::get('backend/mcurencies/{mcurencies}', ['as'=> 'backend.mcurencies.show', 'uses' => 'McurencyController@show']);
Route::get('backend/mcurencies/{mcurencies}/edit', ['as'=> 'backend.mcurencies.edit', 'uses' => 'McurencyController@edit']);


Route::get('backend/mhotels', ['as'=> 'backend.mhotels.index', 'uses' => 'MhotelController@index']);
Route::post('backend/mhotels', ['as'=> 'backend.mhotels.store', 'uses' => 'MhotelController@store']);
Route::get('backend/mhotels/create', ['as'=> 'backend.mhotels.create', 'uses' => 'MhotelController@create']);
Route::put('backend/mhotels/{mhotels}', ['as'=> 'backend.mhotels.update', 'uses' => 'MhotelController@update']);
Route::patch('backend/mhotels/{mhotels}', ['as'=> 'backend.mhotels.update', 'uses' => 'MhotelController@update']);
Route::delete('backend/mhotels/{mhotels}', ['as'=> 'backend.mhotels.destroy', 'uses' => 'MhotelController@destroy']);
Route::get('backend/mhotels/{mhotels}', ['as'=> 'backend.mhotels.show', 'uses' => 'MhotelController@show']);
Route::get('backend/mhotels/{mhotels}/edit', ['as'=> 'backend.mhotels.edit', 'uses' => 'MhotelController@edit']);


Route::get('backend/mjenisizins', ['as'=> 'backend.mjenisizins.index', 'uses' => 'MjenisizinController@index']);
Route::post('backend/mjenisizins', ['as'=> 'backend.mjenisizins.store', 'uses' => 'MjenisizinController@store']);
Route::get('backend/mjenisizins/create', ['as'=> 'backend.mjenisizins.create', 'uses' => 'MjenisizinController@create']);
Route::put('backend/mjenisizins/{mjenisizins}', ['as'=> 'backend.mjenisizins.update', 'uses' => 'MjenisizinController@update']);
Route::patch('backend/mjenisizins/{mjenisizins}', ['as'=> 'backend.mjenisizins.update', 'uses' => 'MjenisizinController@update']);
Route::delete('backend/mjenisizins/{mjenisizins}', ['as'=> 'backend.mjenisizins.destroy', 'uses' => 'MjenisizinController@destroy']);
Route::get('backend/mjenisizins/{mjenisizins}', ['as'=> 'backend.mjenisizins.show', 'uses' => 'MjenisizinController@show']);
Route::get('backend/mjenisizins/{mjenisizins}/edit', ['as'=> 'backend.mjenisizins.edit', 'uses' => 'MjenisizinController@edit']);


Route::get('backend/mmaskapais', ['as'=> 'backend.mmaskapais.index', 'uses' => 'MmaskapaiController@index']);
Route::post('backend/mmaskapais', ['as'=> 'backend.mmaskapais.store', 'uses' => 'MmaskapaiController@store']);
Route::get('backend/mmaskapais/create', ['as'=> 'backend.mmaskapais.create', 'uses' => 'MmaskapaiController@create']);
Route::put('backend/mmaskapais/{mmaskapais}', ['as'=> 'backend.mmaskapais.update', 'uses' => 'MmaskapaiController@update']);
Route::patch('backend/mmaskapais/{mmaskapais}', ['as'=> 'backend.mmaskapais.update', 'uses' => 'MmaskapaiController@update']);
Route::delete('backend/mmaskapais/{mmaskapais}', ['as'=> 'backend.mmaskapais.destroy', 'uses' => 'MmaskapaiController@destroy']);
Route::get('backend/mmaskapais/{mmaskapais}', ['as'=> 'backend.mmaskapais.show', 'uses' => 'MmaskapaiController@show']);
Route::get('backend/mmaskapais/{mmaskapais}/edit', ['as'=> 'backend.mmaskapais.edit', 'uses' => 'MmaskapaiController@edit']);


Route::get('backend/mnegaras', ['as'=> 'backend.mnegaras.index', 'uses' => 'MnegaraController@index']);
Route::post('backend/mnegaras', ['as'=> 'backend.mnegaras.store', 'uses' => 'MnegaraController@store']);
Route::get('backend/mnegaras/create', ['as'=> 'backend.mnegaras.create', 'uses' => 'MnegaraController@create']);
Route::put('backend/mnegaras/{mnegaras}', ['as'=> 'backend.mnegaras.update', 'uses' => 'MnegaraController@update']);
Route::patch('backend/mnegaras/{mnegaras}', ['as'=> 'backend.mnegaras.update', 'uses' => 'MnegaraController@update']);
Route::delete('backend/mnegaras/{mnegaras}', ['as'=> 'backend.mnegaras.destroy', 'uses' => 'MnegaraController@destroy']);
Route::get('backend/mnegaras/{mnegaras}', ['as'=> 'backend.mnegaras.show', 'uses' => 'MnegaraController@show']);
Route::get('backend/mnegaras/{mnegaras}/edit', ['as'=> 'backend.mnegaras.edit', 'uses' => 'MnegaraController@edit']);


Route::get('backend/mpembimbings', ['as'=> 'backend.mpembimbings.index', 'uses' => 'MpembimbingController@index']);
Route::post('backend/mpembimbings', ['as'=> 'backend.mpembimbings.store', 'uses' => 'MpembimbingController@store']);
Route::get('backend/mpembimbings/create', ['as'=> 'backend.mpembimbings.create', 'uses' => 'MpembimbingController@create']);
Route::put('backend/mpembimbings/{mpembimbings}', ['as'=> 'backend.mpembimbings.update', 'uses' => 'MpembimbingController@update']);
Route::patch('backend/mpembimbings/{mpembimbings}', ['as'=> 'backend.mpembimbings.update', 'uses' => 'MpembimbingController@update']);
Route::delete('backend/mpembimbings/{mpembimbings}', ['as'=> 'backend.mpembimbings.destroy', 'uses' => 'MpembimbingController@destroy']);
Route::get('backend/mpembimbings/{mpembimbings}', ['as'=> 'backend.mpembimbings.show', 'uses' => 'MpembimbingController@show']);
Route::get('backend/mpembimbings/{mpembimbings}/edit', ['as'=> 'backend.mpembimbings.edit', 'uses' => 'MpembimbingController@edit']);


Route::get('backend/mstatuspemesanans', ['as'=> 'backend.mstatuspemesanans.index', 'uses' => 'MstatuspemesananController@index']);
Route::post('backend/mstatuspemesanans', ['as'=> 'backend.mstatuspemesanans.store', 'uses' => 'MstatuspemesananController@store']);
Route::get('backend/mstatuspemesanans/create', ['as'=> 'backend.mstatuspemesanans.create', 'uses' => 'MstatuspemesananController@create']);
Route::put('backend/mstatuspemesanans/{mstatuspemesanans}', ['as'=> 'backend.mstatuspemesanans.update', 'uses' => 'MstatuspemesananController@update']);
Route::patch('backend/mstatuspemesanans/{mstatuspemesanans}', ['as'=> 'backend.mstatuspemesanans.update', 'uses' => 'MstatuspemesananController@update']);
Route::delete('backend/mstatuspemesanans/{mstatuspemesanans}', ['as'=> 'backend.mstatuspemesanans.destroy', 'uses' => 'MstatuspemesananController@destroy']);
Route::get('backend/mstatuspemesanans/{mstatuspemesanans}', ['as'=> 'backend.mstatuspemesanans.show', 'uses' => 'MstatuspemesananController@show']);
Route::get('backend/mstatuspemesanans/{mstatuspemesanans}/edit', ['as'=> 'backend.mstatuspemesanans.edit', 'uses' => 'MstatuspemesananController@edit']);


Route::get('backend/mwilayahs', ['as'=> 'backend.mwilayahs.index', 'uses' => 'MwilayahController@index']);
Route::post('backend/mwilayahs', ['as'=> 'backend.mwilayahs.store', 'uses' => 'MwilayahController@store']);
Route::get('backend/mwilayahs/create', ['as'=> 'backend.mwilayahs.create', 'uses' => 'MwilayahController@create']);
Route::put('backend/mwilayahs/{mwilayahs}', ['as'=> 'backend.mwilayahs.update', 'uses' => 'MwilayahController@update']);
Route::patch('backend/mwilayahs/{mwilayahs}', ['as'=> 'backend.mwilayahs.update', 'uses' => 'MwilayahController@update']);
Route::delete('backend/mwilayahs/{mwilayahs}', ['as'=> 'backend.mwilayahs.destroy', 'uses' => 'MwilayahController@destroy']);
Route::get('backend/mwilayahs/{mwilayahs}', ['as'=> 'backend.mwilayahs.show', 'uses' => 'MwilayahController@show']);
Route::get('backend/mwilayahs/{mwilayahs}/edit', ['as'=> 'backend.mwilayahs.edit', 'uses' => 'MwilayahController@edit']);


Route::get('backend/pakets', ['as'=> 'backend.pakets.index', 'uses' => 'PaketController@index']);
Route::post('backend/pakets', ['as'=> 'backend.pakets.store', 'uses' => 'PaketController@store']);
Route::get('backend/pakets/create', ['as'=> 'backend.pakets.create', 'uses' => 'PaketController@create']);
Route::put('backend/pakets/{pakets}', ['as'=> 'backend.pakets.update', 'uses' => 'PaketController@update']);
Route::patch('backend/pakets/{pakets}', ['as'=> 'backend.pakets.update', 'uses' => 'PaketController@update']);
Route::delete('backend/pakets/{pakets}', ['as'=> 'backend.pakets.destroy', 'uses' => 'PaketController@destroy']);
Route::get('backend/pakets/{pakets}', ['as'=> 'backend.pakets.show', 'uses' => 'PaketController@show']);
Route::get('backend/pakets/{pakets}/edit', ['as'=> 'backend.pakets.edit', 'uses' => 'PaketController@edit']);


Route::get('backend/pemesanans', ['as'=> 'backend.pemesanans.index', 'uses' => 'PemesananController@index']);
Route::post('backend/pemesanans', ['as'=> 'backend.pemesanans.store', 'uses' => 'PemesananController@store']);
Route::get('backend/pemesanans/create', ['as'=> 'backend.pemesanans.create', 'uses' => 'PemesananController@create']);
Route::put('backend/pemesanans/{pemesanans}', ['as'=> 'backend.pemesanans.update', 'uses' => 'PemesananController@update']);
Route::patch('backend/pemesanans/{pemesanans}', ['as'=> 'backend.pemesanans.update', 'uses' => 'PemesananController@update']);
Route::delete('backend/pemesanans/{pemesanans}', ['as'=> 'backend.pemesanans.destroy', 'uses' => 'PemesananController@destroy']);
Route::get('backend/pemesanans/{pemesanans}', ['as'=> 'backend.pemesanans.show', 'uses' => 'PemesananController@show']);
Route::get('backend/pemesanans/{pemesanans}/edit', ['as'=> 'backend.pemesanans.edit', 'uses' => 'PemesananController@edit']);


Route::get('backend/travelagents', ['as'=> 'backend.travelagents.index', 'uses' => 'TravelagentController@index']);
Route::post('backend/travelagents', ['as'=> 'backend.travelagents.store', 'uses' => 'TravelagentController@store']);
Route::get('backend/travelagents/create', ['as'=> 'backend.travelagents.create', 'uses' => 'TravelagentController@create']);
Route::put('backend/travelagents/{travelagents}', ['as'=> 'backend.travelagents.update', 'uses' => 'TravelagentController@update']);
Route::patch('backend/travelagents/{travelagents}', ['as'=> 'backend.travelagents.update', 'uses' => 'TravelagentController@update']);
Route::delete('backend/travelagents/{travelagents}', ['as'=> 'backend.travelagents.destroy', 'uses' => 'TravelagentController@destroy']);
Route::get('backend/travelagents/{travelagents}', ['as'=> 'backend.travelagents.show', 'uses' => 'TravelagentController@show']);
Route::get('backend/travelagents/{travelagents}/edit', ['as'=> 'backend.travelagents.edit', 'uses' => 'TravelagentController@edit']);


