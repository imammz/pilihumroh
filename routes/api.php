<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('backend/mroles', 'MroleAPIController@index');
Route::post('backend/mroles', 'MroleAPIController@store');
Route::get('backend/mroles/{mroles}', 'MroleAPIController@show');
Route::put('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::patch('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::delete('backend/mroles{mroles}', 'MroleAPIController@destroy');

Route::get('backend/mroles', 'MroleAPIController@index');
Route::post('backend/mroles', 'MroleAPIController@store');
Route::get('backend/mroles/{mroles}', 'MroleAPIController@show');
Route::put('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::patch('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::delete('backend/mroles{mroles}', 'MroleAPIController@destroy');

Route::get('backend/mroles', 'MroleAPIController@index');
Route::post('backend/mroles', 'MroleAPIController@store');
Route::get('backend/mroles/{mroles}', 'MroleAPIController@show');
Route::put('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::patch('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::delete('backend/mroles{mroles}', 'MroleAPIController@destroy');

Route::get('backend/users', 'UserAPIController@index');
Route::post('backend/users', 'UserAPIController@store');
Route::get('backend/users/{users}', 'UserAPIController@show');
Route::put('backend/users/{users}', 'UserAPIController@update');
Route::patch('backend/users/{users}', 'UserAPIController@update');
Route::delete('backend/users{users}', 'UserAPIController@destroy');

Route::get('backend/mcontentcategories', 'McontentcategoryAPIController@index');
Route::post('backend/mcontentcategories', 'McontentcategoryAPIController@store');
Route::get('backend/mcontentcategories/{mcontentcategories}', 'McontentcategoryAPIController@show');
Route::put('backend/mcontentcategories/{mcontentcategories}', 'McontentcategoryAPIController@update');
Route::patch('backend/mcontentcategories/{mcontentcategories}', 'McontentcategoryAPIController@update');
Route::delete('backend/mcontentcategories{mcontentcategories}', 'McontentcategoryAPIController@destroy');

Route::get('backend/users', 'UserAPIController@index');
Route::post('backend/users', 'UserAPIController@store');
Route::get('backend/users/{users}', 'UserAPIController@show');
Route::put('backend/users/{users}', 'UserAPIController@update');
Route::patch('backend/users/{users}', 'UserAPIController@update');
Route::delete('backend/users{users}', 'UserAPIController@destroy');

Route::get('backend/mroles', 'MroleAPIController@index');
Route::post('backend/mroles', 'MroleAPIController@store');
Route::get('backend/mroles/{mroles}', 'MroleAPIController@show');
Route::put('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::patch('backend/mroles/{mroles}', 'MroleAPIController@update');
Route::delete('backend/mroles{mroles}', 'MroleAPIController@destroy');

Route::get('backend/users', 'UserAPIController@index');
Route::post('backend/users', 'UserAPIController@store');
Route::get('backend/users/{users}', 'UserAPIController@show');
Route::put('backend/users/{users}', 'UserAPIController@update');
Route::patch('backend/users/{users}', 'UserAPIController@update');
Route::delete('backend/users{users}', 'UserAPIController@destroy');

Route::get('backend/contents', 'ContentAPIController@index');
Route::post('backend/contents', 'ContentAPIController@store');
Route::get('backend/contents/{contents}', 'ContentAPIController@show');
Route::put('backend/contents/{contents}', 'ContentAPIController@update');
Route::patch('backend/contents/{contents}', 'ContentAPIController@update');
Route::delete('backend/contents{contents}', 'ContentAPIController@destroy');

Route::get('backend/mbandaras', 'MbandaraAPIController@index');
Route::post('backend/mbandaras', 'MbandaraAPIController@store');
Route::get('backend/mbandaras/{mbandaras}', 'MbandaraAPIController@show');
Route::put('backend/mbandaras/{mbandaras}', 'MbandaraAPIController@update');
Route::patch('backend/mbandaras/{mbandaras}', 'MbandaraAPIController@update');
Route::delete('backend/mbandaras{mbandaras}', 'MbandaraAPIController@destroy');

Route::get('backend/mbandaras', 'MbandaraAPIController@index');
Route::post('backend/mbandaras', 'MbandaraAPIController@store');
Route::get('backend/mbandaras/{mbandaras}', 'MbandaraAPIController@show');
Route::put('backend/mbandaras/{mbandaras}', 'MbandaraAPIController@update');
Route::patch('backend/mbandaras/{mbandaras}', 'MbandaraAPIController@update');
Route::delete('backend/mbandaras{mbandaras}', 'MbandaraAPIController@destroy');

Route::get('backend/jamaahs', 'JamaahAPIController@index');
Route::post('backend/jamaahs', 'JamaahAPIController@store');
Route::get('backend/jamaahs/{jamaahs}', 'JamaahAPIController@show');
Route::put('backend/jamaahs/{jamaahs}', 'JamaahAPIController@update');
Route::patch('backend/jamaahs/{jamaahs}', 'JamaahAPIController@update');
Route::delete('backend/jamaahs{jamaahs}', 'JamaahAPIController@destroy');

Route::get('backend/jenispakets', 'JenispaketAPIController@index');
Route::post('backend/jenispakets', 'JenispaketAPIController@store');
Route::get('backend/jenispakets/{jenispakets}', 'JenispaketAPIController@show');
Route::put('backend/jenispakets/{jenispakets}', 'JenispaketAPIController@update');
Route::patch('backend/jenispakets/{jenispakets}', 'JenispaketAPIController@update');
Route::delete('backend/jenispakets{jenispakets}', 'JenispaketAPIController@destroy');

Route::get('backend/jenispakets', 'JenispaketAPIController@index');
Route::post('backend/jenispakets', 'JenispaketAPIController@store');
Route::get('backend/jenispakets/{jenispakets}', 'JenispaketAPIController@show');
Route::put('backend/jenispakets/{jenispakets}', 'JenispaketAPIController@update');
Route::patch('backend/jenispakets/{jenispakets}', 'JenispaketAPIController@update');
Route::delete('backend/jenispakets{jenispakets}', 'JenispaketAPIController@destroy');

Route::get('backend/mcurencies', 'McurencyAPIController@index');
Route::post('backend/mcurencies', 'McurencyAPIController@store');
Route::get('backend/mcurencies/{mcurencies}', 'McurencyAPIController@show');
Route::put('backend/mcurencies/{mcurencies}', 'McurencyAPIController@update');
Route::patch('backend/mcurencies/{mcurencies}', 'McurencyAPIController@update');
Route::delete('backend/mcurencies{mcurencies}', 'McurencyAPIController@destroy');

Route::get('backend/mhotels', 'MhotelAPIController@index');
Route::post('backend/mhotels', 'MhotelAPIController@store');
Route::get('backend/mhotels/{mhotels}', 'MhotelAPIController@show');
Route::put('backend/mhotels/{mhotels}', 'MhotelAPIController@update');
Route::patch('backend/mhotels/{mhotels}', 'MhotelAPIController@update');
Route::delete('backend/mhotels{mhotels}', 'MhotelAPIController@destroy');

Route::get('backend/mjenisizins', 'MjenisizinAPIController@index');
Route::post('backend/mjenisizins', 'MjenisizinAPIController@store');
Route::get('backend/mjenisizins/{mjenisizins}', 'MjenisizinAPIController@show');
Route::put('backend/mjenisizins/{mjenisizins}', 'MjenisizinAPIController@update');
Route::patch('backend/mjenisizins/{mjenisizins}', 'MjenisizinAPIController@update');
Route::delete('backend/mjenisizins{mjenisizins}', 'MjenisizinAPIController@destroy');

Route::get('backend/mmaskapais', 'MmaskapaiAPIController@index');
Route::post('backend/mmaskapais', 'MmaskapaiAPIController@store');
Route::get('backend/mmaskapais/{mmaskapais}', 'MmaskapaiAPIController@show');
Route::put('backend/mmaskapais/{mmaskapais}', 'MmaskapaiAPIController@update');
Route::patch('backend/mmaskapais/{mmaskapais}', 'MmaskapaiAPIController@update');
Route::delete('backend/mmaskapais{mmaskapais}', 'MmaskapaiAPIController@destroy');

Route::get('backend/mnegaras', 'MnegaraAPIController@index');
Route::post('backend/mnegaras', 'MnegaraAPIController@store');
Route::get('backend/mnegaras/{mnegaras}', 'MnegaraAPIController@show');
Route::put('backend/mnegaras/{mnegaras}', 'MnegaraAPIController@update');
Route::patch('backend/mnegaras/{mnegaras}', 'MnegaraAPIController@update');
Route::delete('backend/mnegaras{mnegaras}', 'MnegaraAPIController@destroy');

Route::get('backend/mpembimbings', 'MpembimbingAPIController@index');
Route::post('backend/mpembimbings', 'MpembimbingAPIController@store');
Route::get('backend/mpembimbings/{mpembimbings}', 'MpembimbingAPIController@show');
Route::put('backend/mpembimbings/{mpembimbings}', 'MpembimbingAPIController@update');
Route::patch('backend/mpembimbings/{mpembimbings}', 'MpembimbingAPIController@update');
Route::delete('backend/mpembimbings{mpembimbings}', 'MpembimbingAPIController@destroy');

Route::get('backend/mstatuspemesanans', 'MstatuspemesananAPIController@index');
Route::post('backend/mstatuspemesanans', 'MstatuspemesananAPIController@store');
Route::get('backend/mstatuspemesanans/{mstatuspemesanans}', 'MstatuspemesananAPIController@show');
Route::put('backend/mstatuspemesanans/{mstatuspemesanans}', 'MstatuspemesananAPIController@update');
Route::patch('backend/mstatuspemesanans/{mstatuspemesanans}', 'MstatuspemesananAPIController@update');
Route::delete('backend/mstatuspemesanans{mstatuspemesanans}', 'MstatuspemesananAPIController@destroy');

Route::get('backend/mwilayahs', 'MwilayahAPIController@index');
Route::post('backend/mwilayahs', 'MwilayahAPIController@store');
Route::get('backend/mwilayahs/{mwilayahs}', 'MwilayahAPIController@show');
Route::put('backend/mwilayahs/{mwilayahs}', 'MwilayahAPIController@update');
Route::patch('backend/mwilayahs/{mwilayahs}', 'MwilayahAPIController@update');
Route::delete('backend/mwilayahs{mwilayahs}', 'MwilayahAPIController@destroy');

Route::get('backend/pakets', 'PaketAPIController@index');
Route::post('backend/pakets', 'PaketAPIController@store');
Route::get('backend/pakets/{pakets}', 'PaketAPIController@show');
Route::put('backend/pakets/{pakets}', 'PaketAPIController@update');
Route::patch('backend/pakets/{pakets}', 'PaketAPIController@update');
Route::delete('backend/pakets{pakets}', 'PaketAPIController@destroy');

Route::get('backend/pemesanans', 'PemesananAPIController@index');
Route::post('backend/pemesanans', 'PemesananAPIController@store');
Route::get('backend/pemesanans/{pemesanans}', 'PemesananAPIController@show');
Route::put('backend/pemesanans/{pemesanans}', 'PemesananAPIController@update');
Route::patch('backend/pemesanans/{pemesanans}', 'PemesananAPIController@update');
Route::delete('backend/pemesanans{pemesanans}', 'PemesananAPIController@destroy');

Route::get('backend/travelagents', 'TravelagentAPIController@index');
Route::post('backend/travelagents', 'TravelagentAPIController@store');
Route::get('backend/travelagents/{travelagents}', 'TravelagentAPIController@show');
Route::put('backend/travelagents/{travelagents}', 'TravelagentAPIController@update');
Route::patch('backend/travelagents/{travelagents}', 'TravelagentAPIController@update');
Route::delete('backend/travelagents{travelagents}', 'TravelagentAPIController@destroy');

Route::get('backend/travelagents', 'TravelagentAPIController@index');
Route::post('backend/travelagents', 'TravelagentAPIController@store');
Route::get('backend/travelagents/{travelagents}', 'TravelagentAPIController@show');
Route::put('backend/travelagents/{travelagents}', 'TravelagentAPIController@update');
Route::patch('backend/travelagents/{travelagents}', 'TravelagentAPIController@update');
Route::delete('backend/travelagents{travelagents}', 'TravelagentAPIController@destroy');

Route::get('backend/users', 'UserAPIController@index');
Route::post('backend/users', 'UserAPIController@store');
Route::get('backend/users/{users}', 'UserAPIController@show');
Route::put('backend/users/{users}', 'UserAPIController@update');
Route::patch('backend/users/{users}', 'UserAPIController@update');
Route::delete('backend/users{users}', 'UserAPIController@destroy');