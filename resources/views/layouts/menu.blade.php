   <li class="header">MENU UTAMA</li>

   <li class="{{ Request::is('backend/home') ? 'active' : '' }}">
    <a href="{!! url('/backend/home') !!}"><i class="fa fa-edit"></i><span>Kelola Data Umroh</span></a>
</li>
<li class="{{ Request::is('backend/travelagents*') ? 'active' : '' }}">
    <a href="{!! route('backend.travelagents.index') !!}"><i class="fa fa-edit"></i><span>Travel Agent</span></a>
</li>
<li class="{{ Request::is('backend/pakets*') ? 'active' : '' }}">
    <a href="{!! route('backend.pakets.index') !!}"><i class="fa fa-edit"></i><span>Paket</span></a>
</li>
<li class="{{ Request::is('backend/jamaahs*') ? 'active' : '' }}">
    <a href="{!! route('backend.jamaahs.index') !!}"><i class="fa fa-edit"></i><span>Data Jamaah</span></a>
</li>
<li class="{{ Request::is('backend/pemesanans*') ? 'active' : '' }}">
    <a href="{!! route('backend.pemesanans.index') !!}"><i class="fa fa-edit"></i><span>Pemesanan</span></a>
</li>
<li class="{{ Request::is('backend/mpembimbings*') ? 'active' : '' }}">
    <a href="{!! route('backend.mpembimbings.index') !!}"><i class="fa fa-edit"></i><span>Data Pembimbing</span></a>
</li>

 @if(Auth::user()->idRole == 1 or  Auth::user()->idRole == 2) 

   <li class="header">KONTEN WEB</li>
<li class="{{ Request::is('backend/backend/berita*') ? 'active' : '' }}">
    <a href="{!! route('backend.contents.index') !!}"><i class="fa fa-edit"></i><span>Berita</span></a>
</li>
<li class="{{ Request::is('backend/backend/about*') ? 'active' : '' }}">
    <a href="{!! route('backend.contents.index') !!}"><i class="fa fa-edit"></i><span>Tentang</span></a>
</li>
<li class="{{ Request::is('backend/backend/faq*') ? 'active' : '' }}">
    <a href="{!! route('backend.contents.index') !!}"><i class="fa fa-edit"></i><span>FAQ</span></a>
</li>
<li class="{{ Request::is('backend/backend/slider*') ? 'active' : '' }}">
    <a href="{!! route('backend.contents.index') !!}"><i class="fa fa-edit"></i><span>Slider</span></a>
</li>
@endif
 @if(Auth::user()->idRole == 1) 

<li class="header">MASTER DATA</li>


<li class="{{ Request::is('backend/mbandaras*') ? 'active' : '' }}">
    <a href="{!! route('backend.mbandaras.index') !!}"><i class="fa fa-edit"></i><span>Bandara</span></a>
</li>

<li class="{{ Request::is('backend/mcurencies*') ? 'active' : '' }}">
    <a href="{!! route('backend.mcurencies.index') !!}"><i class="fa fa-edit"></i><span>Mata Uang</span></a>
</li>

<li class="{{ Request::is('backend/mhotels*') ? 'active' : '' }}">
    <a href="{!! route('backend.mhotels.index') !!}"><i class="fa fa-edit"></i><span>Hotel</span></a>
</li>



<li class="{{ Request::is('backend/mmaskapais*') ? 'active' : '' }}">
    <a href="{!! route('backend.mmaskapais.index') !!}"><i class="fa fa-edit"></i><span>Maskapai</span></a>
</li>

<li class="{{ Request::is('backend/mnegaras*') ? 'active' : '' }}">
    <a href="{!! route('backend.mnegaras.index') !!}"><i class="fa fa-edit"></i><span>Negara</span></a>
</li>





<li class="{{ Request::is('backend/mwilayahs*') ? 'active' : '' }}">
    <a href="{!! route('backend.mwilayahs.index') !!}"><i class="fa fa-edit"></i><span>Wilayah</span></a>
</li>

<li class="header"> &nbsp; </li>



<li class="{{ Request::is('backend/mcontentcategories*') ? 'active' : '' }}">
    <a href="{!! route('backend.mcontentcategories.index') !!}"><i class="fa fa-edit"></i><span>Kategori Konten</span></a>
</li>
<li class="{{ Request::is('backend/mstatuspemesanans*') ? 'active' : '' }}">
    <a href="{!! route('backend.mstatuspemesanans.index') !!}"><i class="fa fa-edit"></i><span>Status Pemesanan</span></a>
</li>
<li class="{{ Request::is('backend/mjenisizins*') ? 'active' : '' }}">
    <a href="{!! route('backend.mjenisizins.index') !!}"><i class="fa fa-edit"></i><span>Jenis Izin</span></a>
</li>
<li class="{{ Request::is('backend/jenispakets*') ? 'active' : '' }}">
    <a href="{!! route('backend.jenispakets.index') !!}"><i class="fa fa-edit"></i><span>Jenis Paket</span></a>
</li>


<li class="header">User Manajemen</li>

<li class="{{ Request::is('backend/mroles*') ? 'active' : '' }}">
    <a href="{!! route('backend.mroles.index') !!}"><i class="fa fa-edit"></i><span>Data Roles</span></a>
</li>


<li class="{{ Request::is('backend/users*') ? 'active' : '' }}">
    <a href="{!! route('backend.users.index') !!}"><i class="fa fa-edit"></i><span>Data User</span></a>
</li>

@endif


