<!-- Idcurency Field -->
<div class="form-group">
    {!! Form::label('idCurency', 'Idcurency:') !!}
    <p>{!! $mcurency->idCurency !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $mcurency->nama !!}</p>
</div>

<!-- Idnegara Field -->
<div class="form-group">
    {!! Form::label('idNegara', 'Idnegara:') !!}
    <p>{!! $mcurency->idNegara !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mcurency->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $mcurency->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mcurency->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $mcurency->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mcurency->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mcurency->deleted_by !!}</p>
</div>

