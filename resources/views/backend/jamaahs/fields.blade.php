<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Namalengkap Field -->
<div class="form-group col-sm-6">
    {!! Form::label('namaLengkap', 'Namalengkap:') !!}
    {!! Form::text('namaLengkap', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Tempatlahir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempatLahir', 'Tempatlahir:') !!}
    {!! Form::text('tempatLahir', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgllahir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tglLahir', 'Tgllahir:') !!}
    {!! Form::date('tglLahir', null, ['class' => 'form-control']) !!}
</div>

<!-- Nik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nik', 'Nik:') !!}
    {!! Form::text('nik', null, ['class' => 'form-control']) !!}
</div>

<!-- Nokk Field -->
<div class="form-group col-sm-6">
    {!! Form::label('noKK', 'Nokk:') !!}
    {!! Form::text('noKK', null, ['class' => 'form-control']) !!}
</div>

<!-- Nopassport Field -->
<div class="form-group col-sm-6">
    {!! Form::label('noPassport', 'Nopassport:') !!}
    {!! Form::text('noPassport', null, ['class' => 'form-control']) !!}
</div>

<!-- Staabilitas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('staAbilitas', 'Staabilitas:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('staAbilitas', false) !!}
        {!! Form::checkbox('staAbilitas', '1', null) !!} 1
    </label>
</div>

<!-- Alamat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
</div>

<!-- Notelp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('noTelp', 'Notelp:') !!}
    {!! Form::text('noTelp', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodeprov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    {!! Form::text('kodeProv', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodekab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    {!! Form::text('kodeKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Idpemesanan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idPemesanan', 'Idpemesanan:') !!}
    {!! Form::text('idPemesanan', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.jamaahs.index') !!}" class="btn btn-default">Cancel</a>
</div>
