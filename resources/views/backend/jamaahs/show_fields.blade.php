<!-- Idjamaah Field -->
<div class="form-group">
    {!! Form::label('idJamaah', 'Idjamaah:') !!}
    <p>{!! $jamaah->idJamaah !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $jamaah->email !!}</p>
</div>

<!-- Namalengkap Field -->
<div class="form-group">
    {!! Form::label('namaLengkap', 'Namalengkap:') !!}
    <p>{!! $jamaah->namaLengkap !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $jamaah->gender !!}</p>
</div>

<!-- Tempatlahir Field -->
<div class="form-group">
    {!! Form::label('tempatLahir', 'Tempatlahir:') !!}
    <p>{!! $jamaah->tempatLahir !!}</p>
</div>

<!-- Tgllahir Field -->
<div class="form-group">
    {!! Form::label('tglLahir', 'Tgllahir:') !!}
    <p>{!! $jamaah->tglLahir !!}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{!! $jamaah->nik !!}</p>
</div>

<!-- Nokk Field -->
<div class="form-group">
    {!! Form::label('noKK', 'Nokk:') !!}
    <p>{!! $jamaah->noKK !!}</p>
</div>

<!-- Nopassport Field -->
<div class="form-group">
    {!! Form::label('noPassport', 'Nopassport:') !!}
    <p>{!! $jamaah->noPassport !!}</p>
</div>

<!-- Staabilitas Field -->
<div class="form-group">
    {!! Form::label('staAbilitas', 'Staabilitas:') !!}
    <p>{!! $jamaah->staAbilitas !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $jamaah->alamat !!}</p>
</div>

<!-- Notelp Field -->
<div class="form-group">
    {!! Form::label('noTelp', 'Notelp:') !!}
    <p>{!! $jamaah->noTelp !!}</p>
</div>

<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $jamaah->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $jamaah->kodeKab !!}</p>
</div>

<!-- Idpemesanan Field -->
<div class="form-group">
    {!! Form::label('idPemesanan', 'Idpemesanan:') !!}
    <p>{!! $jamaah->idPemesanan !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $jamaah->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $jamaah->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $jamaah->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $jamaah->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $jamaah->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $jamaah->deleted_by !!}</p>
</div>

