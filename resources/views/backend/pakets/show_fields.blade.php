<!-- Idpaket Field -->
<div class="form-group">
    {!! Form::label('idPaket', 'Idpaket:') !!}
    <p>{!! $paket->idPaket !!}</p>
</div>

<!-- Namapaket Field -->
<div class="form-group">
    {!! Form::label('namaPaket', 'Namapaket:') !!}
    <p>{!! $paket->namaPaket !!}</p>
</div>

<!-- Idtravelagent Field -->
<div class="form-group">
    {!! Form::label('idTravelAgent', 'Idtravelagent:') !!}
    <p>{!! $paket->idTravelAgent !!}</p>
</div>

<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $paket->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $paket->kodeKab !!}</p>
</div>

<!-- Idjenispaket Field -->
<div class="form-group">
    {!! Form::label('idJenisPaket', 'Idjenispaket:') !!}
    <p>{!! $paket->idJenisPaket !!}</p>
</div>

<!-- Tglberangkat Field -->
<div class="form-group">
    {!! Form::label('tglBerangkat', 'Tglberangkat:') !!}
    <p>{!! $paket->tglBerangkat !!}</p>
</div>

<!-- Tglpulang Field -->
<div class="form-group">
    {!! Form::label('tglPulang', 'Tglpulang:') !!}
    <p>{!! $paket->tglPulang !!}</p>
</div>

<!-- Stapromo Field -->
<div class="form-group">
    {!! Form::label('staPromo', 'Stapromo:') !!}
    <p>{!! $paket->staPromo !!}</p>
</div>

<!-- Hargautama Field -->
<div class="form-group">
    {!! Form::label('hargaUtama', 'Hargautama:') !!}
    <p>{!! $paket->hargaUtama !!}</p>
</div>

<!-- Hargapromo Field -->
<div class="form-group">
    {!! Form::label('hargaPromo', 'Hargapromo:') !!}
    <p>{!! $paket->hargaPromo !!}</p>
</div>

<!-- Persenpromo Field -->
<div class="form-group">
    {!! Form::label('persenPromo', 'Persenpromo:') !!}
    <p>{!! $paket->persenPromo !!}</p>
</div>

<!-- Idcurency Field -->
<div class="form-group">
    {!! Form::label('idCurency', 'Idcurency:') !!}
    <p>{!! $paket->idCurency !!}</p>
</div>

<!-- Stapublish Field -->
<div class="form-group">
    {!! Form::label('staPublish', 'Stapublish:') !!}
    <p>{!! $paket->staPublish !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $paket->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $paket->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $paket->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $paket->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $paket->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $paket->deleted_by !!}</p>
</div>

