<!-- Namapaket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('namaPaket', 'Namapaket:') !!}
    {!! Form::text('namaPaket', null, ['class' => 'form-control']) !!}
</div>

<!-- Idtravelagent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idTravelAgent', 'Idtravelagent:') !!}
    {!! Form::text('idTravelAgent', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodeprov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    {!! Form::text('kodeProv', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodekab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    {!! Form::text('kodeKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Idjenispaket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idJenisPaket', 'Idjenispaket:') !!}
    {!! Form::number('idJenisPaket', null, ['class' => 'form-control']) !!}
</div>

<!-- Tglberangkat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tglBerangkat', 'Tglberangkat:') !!}
    {!! Form::date('tglBerangkat', null, ['class' => 'form-control']) !!}
</div>

<!-- Tglpulang Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tglPulang', 'Tglpulang:') !!}
    {!! Form::date('tglPulang', null, ['class' => 'form-control']) !!}
</div>

<!-- Stapromo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('staPromo', 'Stapromo:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('staPromo', false) !!}
        {!! Form::checkbox('staPromo', '1', null) !!} 1
    </label>
</div>

<!-- Hargautama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hargaUtama', 'Hargautama:') !!}
    {!! Form::number('hargaUtama', null, ['class' => 'form-control']) !!}
</div>

<!-- Hargapromo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hargaPromo', 'Hargapromo:') !!}
    {!! Form::number('hargaPromo', null, ['class' => 'form-control']) !!}
</div>

<!-- Persenpromo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('persenPromo', 'Persenpromo:') !!}
    {!! Form::number('persenPromo', null, ['class' => 'form-control']) !!}
</div>

<!-- Idcurency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idCurency', 'Idcurency:') !!}
    {!! Form::text('idCurency', null, ['class' => 'form-control']) !!}
</div>

<!-- Stapublish Field -->
<div class="form-group col-sm-6">
    {!! Form::label('staPublish', 'Stapublish:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('staPublish', false) !!}
        {!! Form::checkbox('staPublish', '1', null) !!} 1
    </label>
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.pakets.index') !!}" class="btn btn-default">Cancel</a>
</div>
