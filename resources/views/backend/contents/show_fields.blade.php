<!-- Idcontent Field -->
<div class="form-group">
    {!! Form::label('idContent', 'Idcontent:') !!}
    <p>{!! $content->idContent !!}</p>
</div>

<!-- Judul Field -->
<div class="form-group">
    {!! Form::label('judul', 'Judul:') !!}
    <p>{!! $content->judul !!}</p>
</div>

<!-- Isi Field -->
<div class="form-group">
    {!! Form::label('isi', 'Isi:') !!}
    <p>{!! $content->isi !!}</p>
</div>

<!-- Thumbnail Field -->
<div class="form-group">
    {!! Form::label('thumbnail', 'Thumbnail:') !!}
    <p>{!! $content->thumbnail !!}</p>
</div>

<!-- Stapublish Field -->
<div class="form-group">
    {!! Form::label('staPublish', 'Stapublish:') !!}
    <p>{!! $content->staPublish !!}</p>
</div>

<!-- Tglterbit Field -->
<div class="form-group">
    {!! Form::label('tglTerbit', 'Tglterbit:') !!}
    <p>{!! $content->tglTerbit !!}</p>
</div>

<!-- Idcontentcategory Field -->
<div class="form-group">
    {!! Form::label('idContentCategory', 'Idcontentcategory:') !!}
    <p>{!! $content->idContentCategory !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $content->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $content->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $content->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $content->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $content->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $content->deleted_by !!}</p>
</div>

<!-- Highlight Field -->
<div class="form-group">
    {!! Form::label('highlight', 'Highlight:') !!}
    <p>{!! $content->highlight !!}</p>
</div>

<!-- Author Field -->
<div class="form-group">
    {!! Form::label('author', 'Author:') !!}
    <p>{!! $content->author !!}</p>
</div>

