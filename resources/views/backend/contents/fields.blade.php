<!-- Judul Field -->
<div class="form-group col-sm-6">
    {!! Form::label('judul', 'Judul:') !!}
    {!! Form::text('judul', null, ['class' => 'form-control']) !!}
</div>

<!-- Isi Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('isi', 'Isi:') !!}
    {!! Form::textarea('isi', null, ['class' => 'form-control']) !!}
</div>

<!-- Thumbnail Field -->
<div class="form-group col-sm-6">
    {!! Form::label('thumbnail', 'Thumbnail:') !!}
    {!! Form::text('thumbnail', null, ['class' => 'form-control']) !!}
</div>

<!-- Stapublish Field -->
<div class="form-group col-sm-6">
    {!! Form::label('staPublish', 'Stapublish:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('staPublish', false) !!}
        {!! Form::checkbox('staPublish', '1', null) !!} 1
    </label>
</div>

<!-- Tglterbit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tglTerbit', 'Tglterbit:') !!}
    {!! Form::date('tglTerbit', null, ['class' => 'form-control']) !!}
</div>

<!-- Idcontentcategory Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idContentCategory', 'Idcontentcategory:') !!}
    {!! Form::number('idContentCategory', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Highlight Field -->
<div class="form-group col-sm-6">
    {!! Form::label('highlight', 'Highlight:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('highlight', false) !!}
        {!! Form::checkbox('highlight', '1', null) !!} 1
    </label>
</div>

<!-- Author Field -->
<div class="form-group col-sm-6">
    {!! Form::label('author', 'Author:') !!}
    {!! Form::text('author', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.contents.index') !!}" class="btn btn-default">Cancel</a>
</div>
