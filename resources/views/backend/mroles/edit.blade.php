@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mrole
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mrole, ['route' => ['backend.mroles.update', $mrole->idRole], 'method' => 'patch']) !!}

                        @include('backend.mroles.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection