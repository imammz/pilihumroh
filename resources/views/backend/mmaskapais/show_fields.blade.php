<!-- Idmaskapai Field -->
<div class="form-group">
    {!! Form::label('idMaskapai', 'Idmaskapai:') !!}
    <p>{!! $mmaskapai->idMaskapai !!}</p>
</div>

<!-- Namamaskapai Field -->
<div class="form-group">
    {!! Form::label('namaMaskapai', 'Namamaskapai:') !!}
    <p>{!! $mmaskapai->namaMaskapai !!}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{!! $mmaskapai->keterangan !!}</p>
</div>

<!-- Kelas Field -->
<div class="form-group">
    {!! Form::label('kelas', 'Kelas:') !!}
    <p>{!! $mmaskapai->kelas !!}</p>
</div>

<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $mmaskapai->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $mmaskapai->kodeKab !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mmaskapai->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $mmaskapai->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mmaskapai->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $mmaskapai->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mmaskapai->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mmaskapai->deleted_by !!}</p>
</div>

