@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mmaskapai
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mmaskapai, ['route' => ['backend.mmaskapais.update', $mmaskapai->idMaskapai], 'method' => 'patch']) !!}

                        @include('backend.mmaskapais.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection