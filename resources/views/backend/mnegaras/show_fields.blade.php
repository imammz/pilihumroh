<!-- Idnegara Field -->
<div class="form-group">
    {!! Form::label('idNegara', 'Idnegara:') !!}
    <p>{!! $mnegara->idNegara !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $mnegara->nama !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mnegara->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $mnegara->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mnegara->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $mnegara->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mnegara->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mnegara->deleted_by !!}</p>
</div>

