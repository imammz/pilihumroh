{!! Form::open(['route' => ['backend.mnegaras.destroy', $idNegara], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('backend.mnegaras.show', $idNegara) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('backend.mnegaras.edit', $idNegara) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
</div>
{!! Form::close() !!}
