<!-- Kode Field -->
<div class="form-group">
    {!! Form::label('kode', 'Kode:') !!}
    <p>{!! $mbandara->kode !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $mbandara->nama !!}</p>
</div>

<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $mbandara->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $mbandara->kodeKab !!}</p>
</div>

<!-- Idnegara Field -->
<div class="form-group">
    {!! Form::label('idNegara', 'Idnegara:') !!}
    <p>{!! $mbandara->idNegara !!}</p>
</div>

<!-- Sta Berangkat Aktif Field -->
<div class="form-group">
    {!! Form::label('sta_berangkat_aktif', 'Sta Berangkat Aktif:') !!}
    <p>{!! $mbandara->sta_berangkat_aktif !!}</p>
</div>

<!-- Sta Datang Aktif Field -->
<div class="form-group">
    {!! Form::label('sta_datang_aktif', 'Sta Datang Aktif:') !!}
    <p>{!! $mbandara->sta_datang_aktif !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mbandara->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mbandara->deleted_by !!}</p>
</div>

