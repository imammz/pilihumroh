<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodeprov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    {!! Form::text('kodeProv', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodekab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    {!! Form::text('kodeKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Idnegara Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idNegara', 'Idnegara:') !!}
    {!! Form::text('idNegara', null, ['class' => 'form-control']) !!}
</div>

<!-- Sta Berangkat Aktif Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sta_berangkat_aktif', 'Sta Berangkat Aktif:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('sta_berangkat_aktif', false) !!}
        {!! Form::checkbox('sta_berangkat_aktif', '1', null) !!} 1
    </label>
</div>

<!-- Sta Datang Aktif Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sta_datang_aktif', 'Sta Datang Aktif:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('sta_datang_aktif', false) !!}
        {!! Form::checkbox('sta_datang_aktif', '1', null) !!} 1
    </label>
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.mbandaras.index') !!}" class="btn btn-default">Cancel</a>
</div>
