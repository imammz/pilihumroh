@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mstatuspemesanan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mstatuspemesanan, ['route' => ['backend.mstatuspemesanans.update', $mstatuspemesanan->idStatusPemesanan], 'method' => 'patch']) !!}

                        @include('backend.mstatuspemesanans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection