@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mjenisizin
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mjenisizin, ['route' => ['backend.mjenisizins.update', $mjenisizin->idJenisIzin], 'method' => 'patch']) !!}

                        @include('backend.mjenisizins.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection