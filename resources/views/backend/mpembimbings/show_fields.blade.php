<!-- Idpembimbing Field -->
<div class="form-group">
    {!! Form::label('idPembimbing', 'Idpembimbing:') !!}
    <p>{!! $mpembimbing->idPembimbing !!}</p>
</div>

<!-- Jenis Field -->
<div class="form-group">
    {!! Form::label('jenis', 'Jenis:') !!}
    <p>{!! $mpembimbing->jenis !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $mpembimbing->nama !!}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{!! $mpembimbing->nik !!}</p>
</div>

<!-- Telp Field -->
<div class="form-group">
    {!! Form::label('telp', 'Telp:') !!}
    <p>{!! $mpembimbing->telp !!}</p>
</div>

<!-- Asalnegara Field -->
<div class="form-group">
    {!! Form::label('asalNegara', 'Asalnegara:') !!}
    <p>{!! $mpembimbing->asalNegara !!}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{!! $mpembimbing->keterangan !!}</p>
</div>

<!-- Idtravelagent Field -->
<div class="form-group">
    {!! Form::label('idTravelAgent', 'Idtravelagent:') !!}
    <p>{!! $mpembimbing->idTravelAgent !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mpembimbing->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $mpembimbing->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mpembimbing->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $mpembimbing->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mpembimbing->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mpembimbing->deleted_by !!}</p>
</div>

