@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mstatus Pemesanan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mstatusPemesanan, ['route' => ['backend.mstatusPemesanans.update', $mstatusPemesanan->idStatusPemesanan], 'method' => 'patch']) !!}

                        @include('backend.mstatus_pemesanans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection