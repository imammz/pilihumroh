@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Travelagent
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($travelagent, ['route' => ['backend.travelagents.update', $travelagent->idTravelAgent], 'method' => 'patch']) !!}

                        @include('backend.travelagents.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection