<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
</div>

<!-- Telp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telp', 'Telp:') !!}
    {!! Form::text('telp', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Namapj Field -->
<div class="form-group col-sm-6">
    {!! Form::label('namaPj', 'Namapj:') !!}
    {!! Form::text('namaPj', null, ['class' => 'form-control']) !!}
</div>

<!-- Nikpj Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nikPj', 'Nikpj:') !!}
    {!! Form::text('nikPj', null, ['class' => 'form-control']) !!}
</div>

<!-- Notelppj Field -->
<div class="form-group col-sm-6">
    {!! Form::label('notelpPj', 'Notelppj:') !!}
    {!! Form::text('notelpPj', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamatpj Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alamatPj', 'Alamatpj:') !!}
    {!! Form::text('alamatPj', null, ['class' => 'form-control']) !!}
</div>

<!-- Profil Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('profil', 'Profil:') !!}
    {!! Form::textarea('profil', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodeprov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    {!! Form::text('kodeProv', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodekab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    {!! Form::text('kodeKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Idjenisizin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idJenisIzin', 'Idjenisizin:') !!}
    {!! Form::number('idJenisIzin', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Highlight Field -->
<div class="form-group col-sm-6">
    {!! Form::label('highlight', 'Highlight:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('highlight', false) !!}
        {!! Form::checkbox('highlight', '1', null) !!} 1
    </label>
</div>

<!-- Filelogo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fileLogo', 'Filelogo:') !!}
    {!! Form::text('fileLogo', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.travelagents.index') !!}" class="btn btn-default">Cancel</a>
</div>
