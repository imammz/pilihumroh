<!-- Idtravelagent Field -->
<div class="form-group">
    {!! Form::label('idTravelAgent', 'Idtravelagent:') !!}
    <p>{!! $travelagent->idTravelAgent !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $travelagent->nama !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $travelagent->alamat !!}</p>
</div>

<!-- Telp Field -->
<div class="form-group">
    {!! Form::label('telp', 'Telp:') !!}
    <p>{!! $travelagent->telp !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $travelagent->email !!}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{!! $travelagent->website !!}</p>
</div>

<!-- Namapj Field -->
<div class="form-group">
    {!! Form::label('namaPj', 'Namapj:') !!}
    <p>{!! $travelagent->namaPj !!}</p>
</div>

<!-- Nikpj Field -->
<div class="form-group">
    {!! Form::label('nikPj', 'Nikpj:') !!}
    <p>{!! $travelagent->nikPj !!}</p>
</div>

<!-- Notelppj Field -->
<div class="form-group">
    {!! Form::label('notelpPj', 'Notelppj:') !!}
    <p>{!! $travelagent->notelpPj !!}</p>
</div>

<!-- Alamatpj Field -->
<div class="form-group">
    {!! Form::label('alamatPj', 'Alamatpj:') !!}
    <p>{!! $travelagent->alamatPj !!}</p>
</div>

<!-- Profil Field -->
<div class="form-group">
    {!! Form::label('profil', 'Profil:') !!}
    <p>{!! $travelagent->profil !!}</p>
</div>

<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $travelagent->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $travelagent->kodeKab !!}</p>
</div>

<!-- Idjenisizin Field -->
<div class="form-group">
    {!! Form::label('idJenisIzin', 'Idjenisizin:') !!}
    <p>{!! $travelagent->idJenisIzin !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $travelagent->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $travelagent->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $travelagent->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $travelagent->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $travelagent->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $travelagent->deleted_by !!}</p>
</div>

<!-- Highlight Field -->
<div class="form-group">
    {!! Form::label('highlight', 'Highlight:') !!}
    <p>{!! $travelagent->highlight !!}</p>
</div>

<!-- Filelogo Field -->
<div class="form-group">
    {!! Form::label('fileLogo', 'Filelogo:') !!}
    <p>{!! $travelagent->fileLogo !!}</p>
</div>

