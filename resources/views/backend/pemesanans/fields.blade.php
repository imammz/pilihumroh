<!-- Tglpesan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tglPesan', 'Tglpesan:') !!}
    {!! Form::date('tglPesan', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Idpaket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idPaket', 'Idpaket:') !!}
    {!! Form::text('idPaket', null, ['class' => 'form-control']) !!}
</div>

<!-- Expired Field -->
<div class="form-group col-sm-6">
    {!! Form::label('expired', 'Expired:') !!}
    {!! Form::date('expired', null, ['class' => 'form-control']) !!}
</div>

<!-- Idstatuspemesanan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idStatusPemesanan', 'Idstatuspemesanan:') !!}
    {!! Form::text('idStatusPemesanan', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.pemesanans.index') !!}" class="btn btn-default">Cancel</a>
</div>
