@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pemesanan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pemesanan, ['route' => ['backend.pemesanans.update', $pemesanan->idPemesanan], 'method' => 'patch']) !!}

                        @include('backend.pemesanans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection