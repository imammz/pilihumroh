<!-- Idpemesanan Field -->
<div class="form-group">
    {!! Form::label('idPemesanan', 'Idpemesanan:') !!}
    <p>{!! $pemesanan->idPemesanan !!}</p>
</div>

<!-- Tglpesan Field -->
<div class="form-group">
    {!! Form::label('tglPesan', 'Tglpesan:') !!}
    <p>{!! $pemesanan->tglPesan !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $pemesanan->email !!}</p>
</div>

<!-- Idpaket Field -->
<div class="form-group">
    {!! Form::label('idPaket', 'Idpaket:') !!}
    <p>{!! $pemesanan->idPaket !!}</p>
</div>

<!-- Expired Field -->
<div class="form-group">
    {!! Form::label('expired', 'Expired:') !!}
    <p>{!! $pemesanan->expired !!}</p>
</div>

<!-- Idstatuspemesanan Field -->
<div class="form-group">
    {!! Form::label('idStatusPemesanan', 'Idstatuspemesanan:') !!}
    <p>{!! $pemesanan->idStatusPemesanan !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $pemesanan->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $pemesanan->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $pemesanan->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $pemesanan->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $pemesanan->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $pemesanan->deleted_by !!}</p>
</div>

