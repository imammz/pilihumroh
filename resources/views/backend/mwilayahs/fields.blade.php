<!-- Kodekab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    {!! Form::text('kodeKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Idnegara Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idNegara', 'Idnegara:') !!}
    {!! Form::text('idNegara', null, ['class' => 'form-control']) !!}
</div>

<!-- Namaprov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('namaProv', 'Namaprov:') !!}
    {!! Form::text('namaProv', null, ['class' => 'form-control']) !!}
</div>

<!-- Namakab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('namaKab', 'Namakab:') !!}
    {!! Form::text('namaKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.mwilayahs.index') !!}" class="btn btn-default">Cancel</a>
</div>
