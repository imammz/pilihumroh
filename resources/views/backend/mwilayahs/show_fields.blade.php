<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $mwilayah->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $mwilayah->kodeKab !!}</p>
</div>

<!-- Idnegara Field -->
<div class="form-group">
    {!! Form::label('idNegara', 'Idnegara:') !!}
    <p>{!! $mwilayah->idNegara !!}</p>
</div>

<!-- Namaprov Field -->
<div class="form-group">
    {!! Form::label('namaProv', 'Namaprov:') !!}
    <p>{!! $mwilayah->namaProv !!}</p>
</div>

<!-- Namakab Field -->
<div class="form-group">
    {!! Form::label('namaKab', 'Namakab:') !!}
    <p>{!! $mwilayah->namaKab !!}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $mwilayah->latitude !!}</p>
</div>

<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $mwilayah->longitude !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mwilayah->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $mwilayah->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mwilayah->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $mwilayah->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mwilayah->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mwilayah->deleted_by !!}</p>
</div>

