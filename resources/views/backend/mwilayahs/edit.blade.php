@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mwilayah
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mwilayah, ['route' => ['backend.mwilayahs.update', $mwilayah->kodeProv], 'method' => 'patch']) !!}

                        @include('backend.mwilayahs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection