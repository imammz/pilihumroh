@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jenispaket
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($jenispaket, ['route' => ['backend.jenispakets.update', $jenispaket->idJenisPaket], 'method' => 'patch']) !!}

                        @include('backend.jenispakets.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection