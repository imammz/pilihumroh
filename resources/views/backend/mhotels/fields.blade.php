<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Keterangan Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::textarea('keterangan', null, ['class' => 'form-control']) !!}
</div>

<!-- Bintang Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bintang', 'Bintang:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('bintang', false) !!}
        {!! Form::checkbox('bintang', '1', null) !!} 1
    </label>
</div>

<!-- Kodeprov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    {!! Form::text('kodeProv', null, ['class' => 'form-control']) !!}
</div>

<!-- Kodekab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    {!! Form::text('kodeKab', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.mhotels.index') !!}" class="btn btn-default">Cancel</a>
</div>
