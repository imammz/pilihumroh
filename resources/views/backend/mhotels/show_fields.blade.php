<!-- Idhotel Field -->
<div class="form-group">
    {!! Form::label('idHotel', 'Idhotel:') !!}
    <p>{!! $mhotel->idHotel !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $mhotel->nama !!}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{!! $mhotel->keterangan !!}</p>
</div>

<!-- Bintang Field -->
<div class="form-group">
    {!! Form::label('bintang', 'Bintang:') !!}
    <p>{!! $mhotel->bintang !!}</p>
</div>

<!-- Kodeprov Field -->
<div class="form-group">
    {!! Form::label('kodeProv', 'Kodeprov:') !!}
    <p>{!! $mhotel->kodeProv !!}</p>
</div>

<!-- Kodekab Field -->
<div class="form-group">
    {!! Form::label('kodeKab', 'Kodekab:') !!}
    <p>{!! $mhotel->kodeKab !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mhotel->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $mhotel->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mhotel->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $mhotel->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $mhotel->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $mhotel->deleted_by !!}</p>
</div>

