<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $user->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $user->password !!}</p>
</div>

<!-- Remember Token Field -->
<div class="form-group">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    <p>{!! $user->remember_token !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $user->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $user->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $user->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
     
    <p>{!! $user->deleted_by !!}</p>
</div>

<!-- Fullname Field -->
<div class="form-group">
    {!! Form::label('fullname', 'Fullname:') !!}
    <p>{!! $user->fullname !!}</p>
</div>

<!-- Idrole Field -->
<div class="form-group">
    {!! Form::label('idRole', 'Idrole:') !!}
    <p>{!! $user->idRole !!}</p>
</div>

<!-- Fbtoken Field -->
<div class="form-group">
    {!! Form::label('fbToken', 'Fbtoken:') !!}
    <p>{!! $user->fbToken !!}</p>
</div>

<!-- Twtoken Field -->
<div class="form-group">
    {!! Form::label('twToken', 'Twtoken:') !!}
    <p>{!! $user->twToken !!}</p>
</div>

<!-- Googletoken Field -->
<div class="form-group">
    {!! Form::label('googleToken', 'Googletoken:') !!}
    <p>{!! $user->googleToken !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
     
    <p>{!! $user->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
     
    <p>{!! $user->updated_by !!}</p>
</div>

