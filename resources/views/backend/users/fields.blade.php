<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Remember Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fullname', 'Fullname:') !!}
    {!! Form::text('fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Idrole Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idRole', 'Idrole:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('idRole', false) !!}
        {!! Form::checkbox('idRole', '1', null) !!} 1
    </label>
</div>

<!-- Fbtoken Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fbToken', 'Fbtoken:') !!}
    {!! Form::text('fbToken', null, ['class' => 'form-control']) !!}
</div>

<!-- Twtoken Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twToken', 'Twtoken:') !!}
    {!! Form::text('twToken', null, ['class' => 'form-control']) !!}
</div>

<!-- Googletoken Field -->
<div class="form-group col-sm-6">
    {!! Form::label('googleToken', 'Googletoken:') !!}
    {!! Form::text('googleToken', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
     
     
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('backend.users.index') !!}" class="btn btn-default">Cancel</a>
</div>
