@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{asset('assets_admin/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('assets_admin/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div>
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Beranda
                    <small>Kelola Data Umroh</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Beranda</a></li>
                    <li class="active">Kelola Data Umroh</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <div class="col-lg-3 col-xs-6">
                           @if(Auth::user()->idRole == 1 or  Auth::user()->idRole == 2 ) 
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{$countTravelAgent}}</h3>

                                    <p>Travel Agent <br/>Terdaftar</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bookmark"></i>
                                </div>
                                <a href="{{url('backend/travelagents')}}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                             @elseif(Auth::user()->idRole == 3) 
                            <div class="small-box bg-aqua">
                                <div>
                                    <img src="{{asset('travel/logo'.$travel->fileLogo)}}" width='100' />

                                    <p>{{$travel->nama}}</p>
                                </div>

                                <a href="#" class="small-box-footer">Profil <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                            @endif
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{$countPaket}}</h3>

                                    <p>Paket Umroh <br/>Terdaftar</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{url('backend/pakets')}}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{$countPemesanan}}</h3>

                                    <p>Pemesanan <br/>Terdaftar</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pull-request"></i>
                                </div>
                                <a href="{{url('backend/pemesanans')}}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{$countJamaah}}</h3>

                                    <p>Jamaah <br/>Terdaftar</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{url('backend/jamaahs')}}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-12 col-xs-3">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Pemesanan Terbaru</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table no-margin">
                                        <thead>
                                            <tr>
                                                <th>Paket</th>
                                                <th>User</th>
                                                 <th>Jumlah Jamaah</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($latestPemesanan as $row)
                                            @php
                                                $countJamaah = $row->paket->PaketJamaah()->where('email',$row->user->name)->count();
                                            @endphp
                                            <tr>
                                                <td><a href="{{url('order/paket'.$row->idPaket)}}">{{$row->paket->namaPaket}}</a></td>
                                                <td>{{$row->user->name}}</td>
                                                <td>{{$countJamaah}}</td>
                                                <td><span class="label label-warning"></span>{{$row->mstatuspemesanan->status}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                             <div class="box-footer text-center">
                                <a href="{{url('backend/pemesanans')}}" class="uppercase">Lihat Semua</a>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                    </div>
                    @if(Auth::User()->idRole==1)
                    <div class="col-lg-12 col-xs-3">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Travel Agent Baru Daftar</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <ul class="users-list clearfix">
                                    @foreach($latestTravelAgent as $row)
                                    <li>
                                        <img src="{{asset('travel/logo'.$row->fileLogo)}}" alt="{{$row->fileLogo}}">
                                        <a class="users-list-name" href="{{url('backend/travelagents/'.$row->idTravelAgent)}}">{{$row->fileLogo}}</a>
                                        <span class="users-list-date">{{$row->created_at}}</span>
                                    </li>
                                    @endforeach

                                </ul>
                                <!-- /.users-list -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <a href="{{url('backend/travelagents')}}" class="uppercase">Lihat Semua</a>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                    </div>
                    @endif

                </div>
                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Jadwal Paket Umroh</h3>
                            </div>
                            <div class="box-body no-padding">
                                <!-- THE CALENDAR -->
                                <div id="calendar"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /. box -->
                    </div>
                </div>


        </div>
    </div>
</div>


@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        $(function () {

            /* initialize the external events
             -----------------------------------------------------------------*/
            function init_events(ele) {
                ele.each(function () {

                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    }

                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject)

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 1070,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    })

                })
            }

            init_events($('#external-events div.external-event'))

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date()
            var d = date.getDate(),
                    m = date.getMonth(),
                    y = date.getFullYear()
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    today: 'Hari Ini',
                    month: 'Bulan',
                    week: 'Minggu',
                    day: 'Hari'
                },
                //Random default events
                events: [
                    @foreach($jadwal as $row)
                      @php
                        $start = str_replace('-',',',$row->tglBerangkat);
                        $end = str_replace('-',',',$row->tglPulang);
                      @endphp
                    {
                        title: '{{$row->namaPaket}}',
                        start: new Date({{$start}}),
                        end: new Date({{$end}}),
                        url: "{{url('backend/pakets/'.$row->idPaket)}}",
                        backgroundColor: '#3c8dbc', //Primary (light-blue)
                        borderColor: '#3c8dbc' //Primary (light-blue)
                    },
                    @endforeach
                ],
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject')

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject)

                    // assign it the date that was reported
                    copiedEventObject.start = date
                    copiedEventObject.allDay = allDay
                    copiedEventObject.backgroundColor = $(this).css('background-color')
                    copiedEventObject.borderColor = $(this).css('border-color')

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove()
                    }

                }
            })


        })
    });
</script>
@endsection

