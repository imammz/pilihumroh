<!DOCTYPE html>
<html>
<head>
	<title>Pilih Umroh-Travel Umroh Indonesia</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

	
	<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" >  
	<link href="{{asset('assets/css/normalize.css')}}" rel="stylesheet" >  
	<link href="{{asset('assets/fonts/font-awesome.css')}}" rel="stylesheet" >  
	<link href="{{asset('assets/fonts/stylesheet.css')}}" rel="stylesheet" >
	<link href="{{asset('assets/css/fullcalendar.css')}}" rel="stylesheet" > 
	<link href="{{asset('assets/css/hover.css')}}" rel="stylesheet" > 
	<link href="{{asset('assets/css/animations.css')}}" rel="stylesheet" > 
	<link href="{{asset('assets/datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" > 
        
	<!--[if lte IE 9]>
      <link href='css/animations-ie-fix.css' rel='stylesheet'>
	<![endif]-->
	<link href="{{asset('assets/css/main.css')}}" rel="stylesheet" >  
     @yield('css')
</head>
<body>
    
    
    <div class="bg1"></div>
		<div class="bg2"></div>
		<div class="navbar-wrapper"><!--navbar wrapper-->
			<div class="container">
				<div class="nav-atas">
					<div class="col-sm-6 nav-atas-l">
					<div class="call"> +62 21 2963 3600</div>
					</div>
					<div class="col-sm-6 nav-atas-r">
						<ul class="menu-nav-atas">
							<li><a href="{{url('/konfirmasi-pembayaran')}}" class="hvr-grow">Konfirmasi Pembayaran</a></li>
							<li><a href="{{url('/cara-memesan')}}" class="hvr-grow">Cara Pemesanan</a></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="nav-bwh">
					<div class="col-sm-3 nav-bwh-l">
						<div class="logo">
							<a href="" ></a>
						</div>
						<div class="title">
							<a href="{{url('/')}}"><img src="{{asset('assets/img/Logo_06.jpeg')}}"></a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-4 nav-bwh-c">
						<ul class="menu">
							<li class="act hvr-pop"><a href="">Paket Umroh</a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-3 log-in-out">
						<ul >
							<li ><a href="{{url('/register')}}" class="hvr-grow">Register</a></li>
							<li ><a href="{{url('/login')}}" class="hvr-grow">Login</a></li>
						</ul>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-12">
						<div class="marquee-wrap">
						 <!-- running text  -->
                                                </div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div><!--end navbar wrapper-->
    <div class="clearfix"></div>
	@yield('content')


<div class="clearfix"></div>
	<div class="footer-wrap animatedParent">
		<div class="container footer">	
			<div class="footer-atas  animated fadeIn">
				<div class="footer-list">
				</div>
				<div class="footer-list">
                                    <ul>
                                        <li>&nbsp;</li>
                                    </ul>
				</div>
				<div class="footer-list">
					<ul>
                                                <li><a href="{{url('konfirmasi-pembayaran')}}" class="hvr-grow">Konfirmasi Pembayaran</a></li>
						<li><a href="{{url('cara-memesan')}}" class="hvr-grow">Cara Memesan</a></li>
						<li><a href="{{url('hubungikami')}}" class="hvr-grow">Hubungi Kami</a></li>
						
					
					</ul>
                                    
				</div>
				<div class="footer-list">
					
                                       <ul>
                                           	<li><a href="{{url('syarat')}}" class="hvr-grow">Syarat & Ketentuan</a></li>
						<li><a href="{{url('testi')}}" class="hvr-grow">Testimoni</a></li>
						<li><a href="{{url('about')}}" class="hvr-grow">Tentang Pilih Umroh</a></li>
                                                <li><a href="{{url('faq')}}" class="hvr-grow">FAQ</a></li>

					</ul>
				</div>
				<div class="footer-list cs">
					<ul class="cust-sup">
						<div class="col-sm-7 pl-0"> 
							<h4>Customer Support</h4>
							<li><a href="" class="hvr-grow"><i class="fa fa-phone"></i>+62 212963 3600</a></li>
							<li><a href="" class="hvr-grow"><i class="fa fa-envelope-o"></i>cs@Pilihumroh.com</a></li>
							<ul class="socmed">
								<li class="fb" class="hvr-float"><a href=""><i class="fa fa-facebook"></i></a></li>
								<li class="gp"class="hvr-float"><a href=""><i class="fa fa-google-plus"></i></a></li>
								<li class="tw" class="hvr-float"><a href=""><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</ul>
					
				</div>
			</div>
			<div class="clearfix"></div>
			
			<div class="clearfix"></div>
			<div class="footer-bawah">
				<p class="">Copyright &copy; 2018 PT Pilih Umroh Amanah </p>
			</div>
		</div>
	</div>
	<div class="chat-wrap">
		<div class="cust-button">
			<a href="#">
				<div class="fl cust-button-l">
					<span>Butuh Bantuan?</span>
					<p>Tinggalkan Pesan</p>
				</div>
				<div class="fr cust-button-r"></div>
			</a>
		</div>
		<div class="msg-wrap">
			<div class="msg-head">
				<p>Pilihumroh.com Live Chat</p>
				<a href="#" class="cls-msg fr"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
			</div>
			<div class="msg-isi">
			</div>
		</div>
	</div>	

	





</body>

	<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>   
	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>   
	<script src="{{asset('assets/js/moment.min.js')}}"></script> 
	<script src="{{asset('assets/js/fullcalendar.min.js')}}"></script> 
	<script src="{{asset('assets/js/lang-all.js')}}"></script> 
	<script src="{{asset('assets/js/css3-animate-it.js')}}"></script> 
	<script src="{{asset('assets/js/all.js')}}"></script> 
	<script src="{{asset('assets/js/main.js')}}"></script>   
        <script src="{{asset('assets/datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
</html>