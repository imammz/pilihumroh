@extends('public.part.layout')
@section('content')

<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Montserrat;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-link:"Body Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
span.MsoPlaceholderText
	{color:gray;}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{mso-style-link:"No Spacing Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-link:Footer;}
span.NoSpacingChar
	{mso-style-name:"No Spacing Char";
	mso-style-link:"No Spacing";
	font-family:"Times New Roman","serif";}
span.BodyTextChar
	{mso-style-name:"Body Text Char";
	mso-style-link:"Body Text";
	font-family:"Arial","sans-serif";}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";
	font-family:"Times New Roman","serif";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>


       <div class="big-head"><!--big head-->
			<div class="container">
				<div class="big-isi big-ustad">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div><!--end big head-->

	<div class="detail-wrap"><!--detail-wrap-->
		<div class="container ">
			<div class="ustad-wrapper">
				<div class="ustad-abv">
					<div class="clearfix"></div>
					<div class="ust-detail-wrap">
                                            
                                            
<div class=WordSection1>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='line-height:115%;font-family:Montserrat'>Bismillahirahmanirahim</span></b><span
lang=EN-ID style='line-height:115%;font-family:Montserrat'>,</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=IN style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>LAYANAN
PILIHUMROH</span></b></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
lang=IN style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Sebagai
bagian dari Layanan, PILIHUMROH akan bertindak untuk mempromosikan paket
perjalanan umroh dari TRAVEL UMROH </span><span lang=EN-ID style='font-size:
10.0pt;line-height:115%;font-family:Montserrat'>dan</span><span lang=IN
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> melakukan
transaksi pemesanan. </span><span lang=EN-ID style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>Dalam melakukannya:</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>DISTRIBUSI</span></b><span lang=EN-ID style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>PILIHUMROH
akan mengatur detail mengenai TRAVEL UMROH sebagaimana mestinya (“Informasi”)
sebagaimana yang MITRA berikan dalam format yang sesuai dan atas persetujuan
PILIHUMROH, untuk didistribusikan di website pilihumroh.com dan saluran
marketing lain yang KAMI gunakan untuk kepentingan promosi paket umroh.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>DASHBOARD TRAVEL</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>PILIHUMROH akan menyediakan untuk </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH</span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>,
akses ke fasilitas online yaitu Dashboard Travel, MITRA akan mendapatkan
notifikasi pemesanan online di dalam Dashboard Travel, </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL UMROH </span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>juga dapat
memperbarui tarif dan ketersediaan, serta Informasi paket yang disediakan
TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>NOTIFIKASI</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>PILIHUMROH akan mengirimkan notifikasi ke </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH</span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>
melalui email atau pemberitahuan <i>Dashboard Travel</i></span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>sejak</span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>c</span><span
lang=IN style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>alon </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>j</span><span
lang=IN style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>amaah
telah melakukan</span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'> </span><i><span lang=EN-ID style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>checkout</span></i><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> paket
perjalanan umroh milik TRAVEL UMROH yang dipilih hingga calon jamaah melakukan
pelunasan</span><span style='font-size:10.0pt;line-height:115%;font-family:
Montserrat'>. Jika terjadi perubahan pada alamat email, </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH </span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>bertanggung
jawab untuk segera memberitahukannya kepada PILIHUMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>RUJUKAN KELUHAN</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>PILIHUMROH akan merujuk perselisihan atau keluhan
terkait dengan persiapan dan perjalanan umroh calon jamaah TRAVEL UMROH untuk TRAVEL
UMROH selesaikan.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>KEAKURATAN</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>PILIHUMROH berhak, tanpa pemberitahuan, untuk
mengubah atau menghapus informasi apa pun di situs pilihumroh.com yang</span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> </span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>menurut
pertimbangan kami sepenuhnya, mencemarkan, tidak senonoh, tidak akurat secara
material, melanggar hukum manapun atau kode praktik periklanan, atau merujuk
langsung ke situs web, email, atau nomor telepon </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL UMROH</span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:10.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>PERINGKAT (SISTEM <i>RATING</i>), </span></b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Urutan
pencantuman paket umroh di PILIHUMROH (“Peringkat”) ditentukan secara sepihak
oleh PILIHUMROH memakai algoritma otomatis yang mempertimbangkan berbagai
faktor terutama penilaian dari calon jamaah.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:10.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>REKENING BERSAMA</span></b><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>, Pilihumroh berperan sebagai
perantara bertransaksi “rekber” antara travel umroh dan calon jamaah dalam
pemesanan paket perjalanan umroh sehingga transaksi aman dan terhindar dari
penipuan.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>KEWAJIBAN
TRAVEL UMROH</span></b></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>LEGALITAS PERUSAHAAN</span></b><span lang=EN-ID
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>, </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH wajib mengisi dan melampirkan fotokopi legalitas perusahaan sebagai
berikut:</span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:35.45pt;border-collapse:collapse;border:none'>
 <tr style='height:28.35pt'>
  <td width=1108 colspan=3 style='width:415.35pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Legalitas
  Travel Umroh</span></b></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Nomor
  SIUP</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Nomor
  TDP </span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Kantor
  Pusat*</span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Kantor
  Cabang*</span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Izin
  Umroh</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>PPIH*</span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>PPIU*
  </span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Surat
  Kantor Cabang*</span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Surat
  Konsorsium*</span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Keanggotaan
  Asosiasi Penyelenggara Umroh*</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>AMPHURI     
  </span></b><span lang=EN-ID style='font-size:9.0pt;line-height:115%;
  font-family:Montserrat'> |<b>        HIMPUH       </b> |<b>   </b></span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>ASPHURINDO 
  </span></b><span lang=EN-ID style='font-size:9.0pt;line-height:115%;
  font-family:Montserrat'> |<b>   KESTHURI</b></span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>Nomor
  NPWP Perusahaan</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:9.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:115%'><span lang=EN-ID style='line-height:115%;font-family:
Montserrat'>*coret bila tidak ada</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
text-indent:.55pt;line-height:115%'><span lang=EN-ID style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>INFORMASI PAKET UMROH &amp; TARIF</span></b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>, </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH bertanggung jawab untuk memperbarui informasi di <i>Dashboard Travel</i>
secara berkala, memastikan bahwa semua informasi akurat dan terbaru, harga
dalam mata uang rupiah termasuk pajak terkait, detail tentang ketersediaan
paket umroh, dan informasi relevan lainnya. Jika informasi yang TRAVEL UMROH
sediakan salah atau menyesatkan, TRAVEL UMROH setuju untuk sepenuhnya memberi
ganti rugi kepada PILIHUMROH atau calon jamaah dan membebaskan PILIHUMROH dari
semua kerugian, kewajiban, atau biaya yang harus dikeluarkan sebagai akibatnya.
PILIHUMROH tidak akan bertanggung jawab atas setiap kesalahan atau kelebihan
pemesanan atau tarif yang salah yang disebabkan oleh ketidakakuratan TRAVEL
UMROH dalam memperbarui informasi di <i>Dashboard Travel</i>.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>INFORMASI PEMBAYARAN</span></b><span lang=EN-ID
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>, </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH bertanggung jawab untuk memperbarui <i>Dashboard Travel</i> dengan informasi
akurat dan terbaru mengenai informasi detail pembayaran. Setiap perubahan oleh TRAVEL
UMROH atau dibuat atas nama TRAVEL UMROH yang disampaikan dan diterima oleh <i>Dashboard
Travel</i> mengenai tingkat komisi, metode pembayaran, jadwal pembayaran dan informasi
akuntansi akan menggantikan informasi yang diberikan pada Perjanjian ini,
terutama mengenai tingkat komisi (LAYANAN PILIHUMROH, Klausul KOMISI); dan
informasi akuntansi, jadwal dan metode pembayaran (Klausul PEMBAYARAN, lihat
dibawah) dan diterima sebagai mengikat secara hukum oleh kedua belah pihak.
Jika informasi yang TRAVEL UMROH sediakan salah atau menyesatkan, TRAVEL UMROH
setuju untuk sepenuhnya memberi ganti rugi kepada PILIHUMROH dan membebaskan PILIHUMROH
dari semua kerugian, kewajiban, atau biaya yang harus PILIHUMROH keluarkan
sebagai akibatnya. PILIHUMROH tidak akan bertanggung jawab atas setiap
kesalahan mengenai informasi pembayaran atau tingkat komisi yang disebabkan
oleh ketidakakuratan TRAVEL UMROH dalam memperbarui <i>Dashboard Travel</i>.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>JAMINAN PAKET PERJALANAN UMROH</span></b><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL
UMROH menjamin bahwa paket perjalanan umroh yang dipublikasikan di situs
pilihumroh.com, sebelumnya telah dilakukan pembelian atau pemesanan oleh TRAVEL
UMROH kepada provider jasa (seperti maskapai penerbangan, hotel, catering, bus,
dan lain-lain) atau instrumen terkait lainnya yang dapat memastikan bahwa paket
perjalanan umroh yang dijual adalah valid atau dapat dipertanggungjawabkan.
Selain itu, TRAVEL UMROH wajib memperbaharui informasi pembelian atau pemesanan
tersebut di <i>Dashboard Travel</i>.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>JAMINAN KEBERANGKATAN</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, Jika terjadi
pemesanan yang salah atau pemesanan berlebih yang disebabkan oleh kegagalan TRAVEL
UMROH mematuhi kewajiban tentang INFORMASI PAKET UMROH &amp; TARIF di atas, TRAVEL
UMROH akan mencari PAKET UMROH alternatif untuk calon jamaah dengan jenis paket
umroh yang sama atau lebih baik, dan dengan jarak tanggal keberangkatan yang
wajar dari tanggal keberangkatan awal, dan menanggung selisih tarif paket umroh
diatas tarif bersih yang telah disetujui pada saat pemesanan.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>PEMENUHAN KONTRAK</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH
terikat untuk menerima calon jamaah sebagai pihak di dalam kontrak, untuk
menangani pemesanan sesuai dengan semua informasi yang terkandung di dalamnya,
termasuk informasi tambahan dan keinginan yang disampaikan oleh calon jamaah.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>KESAMAAN TARIF</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH
akan memastikan bahwa tarif jual yang TRAVEL UMROH perbarui di <i>Dashboard
Travel</i> sekurang-kurangnya akan sama menguntungkannya dengan produk yang
sama yang tersedia untuk dijual atau dikomunikasikan melalui media online
lainnya, termasuk situs web TRAVEL UMROH (“Jaminan Kesamaan Tarif”).</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>HARGA INKLUSIF</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH
wajib mencantumkan harga total yang harus dibayarkan oleh calon jamaah beserta
rincian fasilitas yang termasuk dan tidak termasuk yang akan didapatkan calon
jamaah di dalam setiap paket umroh yang dipublikasikan di situs PILIHUMROH.
Bila di kemudian hari PILIHUMROH menemukan ada biaya di luar paket yang
memberatkan calon jamaah, PILIHUMROH berhak memberikan beberapa tahap sanksi
mulai dari teguran hingga penangguhan akses fasilitas <i>Dashboard Travel</i>
bagi TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>KESAMAAN KETERSEDIAAN</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH
akan memastikan bahwa ketersediaan yang TRAVEL UMROH perbarui di <i>Dashboard
Travel</i> akan mewakili semua jenis paket umroh yang tersedia untuk dijual
atau dikomunikasikan melalui media online lainnya, termasuk situs web TRAVEL
UMROH (“Jaminan Kesamaan Ketersediaan”). Sebagai bagian dari Jaminan Kesamaan
Ketersediaan, TRAVEL UMROH akan memastikan bahwa PILIHUMROH setiap saat memiliki
“ketersediaan kuota umroh terakhir” (“KKUT”). KKUT berarti bahwa TRAVEL UMROH
harus menjamin bahwa paket umroh terakhir yang tersedia pada saluran online
lainnya, termasuk situs web TRAVEL UMROH sendiri, juga disediakan bagi PILIHUMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>KESAMAAN PROMOSI</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH
akan memastikan bahwa promosi yang diperbarui di <i>Dashboard Travel</i>, dan
yang dapat didukung oleh sistem <i>Dashboard Travel</i>, akan mewakili semua
promosi TRAVEL UMROH yang tersedia untuk dijual atau dikomunikasikan melalui
media online lainnya, termasuk situs web TRAVEL UMROH (“Jaminan Kesamaan
Promosi”).</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>AKUN JAMAAH</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>, </span><span lang=EN-ID style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>TRAVEL UMROH</span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> di wajibkan
untuk membuatkan akun pilihumroh.com kepada setiap calon jamaah yang mendaftar
secara <i>offline</i> atau mendaftar langsung ke kantor </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH</span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>HAK CIPTA</span></b><span lang=EN-ID style='font-size:
10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH akan memastikan
bahwa TRAVEL UMROH memiliki hak dan wewenang yang diperlukan untuk memakai dan
melisensikan atau mengizinkan penggunaan hak cipta, merek, atau logo apa pun
yang dirujuk di dalam informasi TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>EKSKLUSIVITAS TERJEMAHAN DAN ULASAN JAMAAH</span></b><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, PILIHUMROH
dapat menerjemahkan, mengedit, serta menerbitkan ulasan jamaah dan informasi TRAVEL
UMROH. TRAVEL UMROH tidak akan mengizinkan terjemahan yang telah kami buat
mengenai informasi TRAVEL UMROH atau juga ulasan jamaah untuk digunakan bagi
saluran penjualan lain kecuali saluran penjualan kami. Kami tidak bertanggung
jawab atas isi dari terjemahan atau ulasan jamaah tersebut.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>HAK KEKAYAAN INTELEKTUAL</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, PILIHUMROH
dan perusahaan afiliasinya merupakan pemilik atau pemegang hak atas beberapa
hak kekayaan intelektual, termasuk namun tidak terbatas pada merek dagang, hak
cipta, merek layanan, logo, dan lain-lain. Tidak ada satu pun di dalam
Perjanjian ini yang akan dianggap sebagai pemberian lisensi atau hak apa pun
kepada TRAVEL UMROH, baik itu secara tersirat maupun lainnya, atas hak kekayaan
intelektual PILIHUMROH mana saja. TRAVEL UMROH akan memastikan bahwa TRAVEL
UMROH memiliki hak dan wewenang yang diperlukan untuk memakai dan melisensikan
atau mengizinkan penggunaan hak cipta, merek, atau logo mana pun yang dirujuk
di dalam informasi TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>PENYERTAAN PEMASARAN ONLINE</span></b><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH
mengizinkan PILIHUMROH untuk mempromosikan TRAVEL UMROH menggunakan nama PILIHUMROH
melalui pemasaran online, termasuk pemasaran melalui email dan/atau periklanan
pembayaran per klik atau payperclick (PPC). Kami dapat menjalankan kampanye
promosi atas pertimbangan kami serta kami akan membayar biaya periklanan pada
kampanye pemasaran online kami. TRAVEL UMROH dapat meminta pembatasan yang
wajar atas jenis saluran promosi yang kami pilih namun menerima bahwa, walaupun
kami berupaya mencegahnya, kami tidak memiliki kendali penuh atas tautan dan
referensi yang tidak diminta penyebarannya di internet.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>KEAMANAN</span></b><span lang=EN-ID style='font-size:
10.0pt;line-height:115%;font-family:Montserrat'>, TRAVEL UMROH akan memastikan
bahwa ID Pengguna dan kata sandi/PIN untuk <i>Dashboard Travel</i> dirahasiakan
dan TRAVEL UMROH segera memberitahukan kepada kami atas setiap dugaan
penerobosan keamanan.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=IN style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>SYARAT
dan KETENTUAN</span></b></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
text-indent:.55pt;line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>PEMBAYARAN</span></b><span lang=EN-ID
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>, </span><span
lang=EN-ID style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>PILIHUMROH
akan menyelesaikan pembayaran untuk pemesanan yang telah dikonfirmasikan
melalui transfer bank ke rekening PILIHUMROH. Para pihak setuju dengan
persyaratan pembayaran seperti yang ditunjukkan pada klausul berikut:</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>SISTEM PEMBAYARAN</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>Seluruh transaksi yang dilakukan oleh calon
jamaah dalam memesan paket umroh akan dilakukan melalui rekening PILIHUMROH
sebagai “rekening bersama”, yang selanjutnya akan diserahkan kepada TRAVEL
UMROH sesuai ketentuan yang telah disepakati di poin TERMIN PEMBAYARAN. </span><span
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>  </span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><i><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>DOWN PAYMENT</span></i></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>PILIHUMROH menyediakan sistem <i>down
payment</i> kepada calon jamaah untuk membooking paket yang ingin di pesan,
calon jamaah akan membayarkan sesuai dengan ketentuan <i>down payment</i> yang
ditentukan TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>JATUH TEMPO PELUNASAN</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>Jamaah yang telah mendaftar akan
melunasi pembayaran baik secara kontan maupun angsur sekian hari sebelum
tanggal keberangkatan sesuai dengan aturan yang di tentukan oleh TRAVEL UMROH</span><span
lang=IN style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>. </span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Jika pelunasan
belum dilakukan sampai dengan hari jatuh tempo pelunasan, maka akan dianggap
membatalkan diri secara sepihak dan berlaku KETENTUAN PEMBATALAN.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:35.45pt;border-collapse:collapse;border:none'>
 <tr style='height:28.35pt'>
  <td width=1108 colspan=2 style='width:415.35pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Jatuh
  Tempo Pelunasan</span></b></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=566 style='width:2.95in;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Berapa
  hari sebelum tanggal keberangkatan</span></p>
  </td>
  <td width=541 style='width:202.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
 </tr>
</table>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>TERMIN PEMBAYARAN</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>dana calon jamaah yang telah diterima
PILIHUMROH akan di serahkan kepada TRAVEL UMROH dalam 2 (dua) termin.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Termin
1 (satu): PILIHUMROH akan menyerahkan 50 % dana <i>down payment</i> calon
jamaah kepada TRAVEL UMROH dengan cara transfer dalam waktu </span><span
lang=IN style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>3
(tiga) hari kerja </span><span style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>terhitung sejak dana telah masuk dan di terima di
rekening PILIHUMROH.</span><span style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'> </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Termin
2 (dua): PILIHUMROH akan menyerahkan dana pelunasan jamaah kepada TRAVEL UMROH
sekian hari </span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>kalender </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>sebelum hari keberangkatan jamaah,
sesuai dengan ketentuan JATUH TEMPO PELUNASAN. Dana akan dikirimkan dengan cara
transfer setelah dipotong KOMISI yang disepakati.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=EN-ID style='font-size:7.0pt;
line-height:115%;font-family:Montserrat'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>KOMISI</span></b><span lang=EN-ID style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Perjanjian ini
mengikat TRAVEL UMROH yang tergabung di PILIHUMROH untuk membayar komisi kepada
PILIHUMROH minimal sebesar 4 % (persen) dari harga jual yang tertera di setiap
paket umroh yang</span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'> telah dipesan dan dibayarkan </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>di PILIHUMROH,
komisi akan langsung dipotong dari pelunasan pembayaran calon jamaah.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>TANGGUNG JAWAB DANA JAMAAH</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>dana jamaah yang telah
diserah-terimakan akan menjadi tanggung jawab pihak yang menyimpan dan
mengelola dana tersebut.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>apabila
jamaah telah menyerahkan dana yang berkaitan dengan pemesanan paket dan telah
diterima oleh PILIHUMROH maka segala pengelolaan berkaitan dana tersebut akan
menjadi tanggung jawab PILIHUMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>apabila
dana jamaah yang disimpan oleh pihak PILIHUMROH telah diserahkan dan diterima
oleh pihak TRAVEL UMROH maka segala pengelolaan dana, termasuk didalamnya
kelalaian dalam pengelolaan, akan menjadi tanggung jawab TRAVEL UMROH
sepenuhnya. </span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=IN style='font-size:7.0pt;
line-height:115%;font-family:Montserrat'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>BUKTI TRANSAKSI</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>Ringkasan bukti transaksi akan dicatat dan didata
secara profesional kemudian PARA PIHAK saling melaporkan bukti transaksi
tersebut.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=IN style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>MATA UANG</span></b><span lang=IN style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=IN
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL UMROH
akan menerima pembayaran dari PILIHUMROH dalam mata uang rupiah, dengan
ketentuan bahwa semua tarif paket perjalanan umroh dari TRAVEL UMROH mengikuti
mata uang yang sama.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=EN-ID style='font-size:7.0pt;
line-height:115%;font-family:Montserrat'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>REFUND</span></b><span lang=EN-ID style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>proses
pengembalian dana calon jamaah (“<i>refund</i>”) dari TRAVEL UMROH ke rekening
PILIHUMROH dilakukan paling lambat selama 7 hari kerja, dan proses pengembalian
dana calon jamaah dari rekening PILIHUMROH ke rekening calon jamaah dilakukan
paling lambat selama 7 hari kerja setelah diterimanya dana dari TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=EN-ID style='font-size:7.0pt;
line-height:115%;font-family:Montserrat'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>REKENING</span></b><span lang=EN-ID style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=EN-ID
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>berikut adalah
detil informasi mengenai rekening TRAVEL UMROH:</span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:35.45pt;border-collapse:collapse;border:none'>
 <tr style='height:28.35pt'>
  <td width=1108 colspan=3 style='width:415.35pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Rekening
  Travel Umroh</span></b></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Nama
  Bank</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Nomor
  Rekening</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Mata
  Uang</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Rupiah</span></b></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:28.35pt'>
  <td width=283 style='width:106.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>Nama
  Penerima</span></p>
  </td>
  <td width=401 style='width:150.4pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
  <td width=424 style='width:158.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
  lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>
  </td>
 </tr>
</table>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
text-indent:.35pt;line-height:115%'><b><span lang=EN-ID style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
text-indent:.35pt;line-height:115%'><b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>KETENTUAN PEMBATALAN</span></b><span
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>,</span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> skema
pembatalan dapat terjadi bila dilakukan oleh calon jamaah atau TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>PEMBATALAN DILAKUKAN CALON JAMAAH</span></b><span style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>ketentuan
apabila pembatalan dilakukan oleh calon jamaah adalah sebagai berikut:</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>a.<span style='font:7.0pt "Times New Roman"'>
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Pembatalan
dilakukan sebelum JATUH TEMPO PELUNASAN, <i>down payment</i> yang sudah
dibayarkan dapat dikembalikan (lihat klausul PEMBAYARAN, poin <i>REFUND</i>)
setelah dikurangi biaya administrasi dan biaya yang sudah digunakan TRAVEL
UMROH untuk booking dan lain-lain atau menjadi deposit calon jamaah di akun
calon jamaah di pilihumroh.com setelah ada pemotongan biaya administrasi dari
PILIHUMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>b.<span style='font:7.0pt "Times New Roman"'>
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Pembatalan
yang dilakukan setelah JATUH TEMPO PELUNASAN sampai dengan 14 (empat belas)
hari sebelum tanggal keberangkatan, biaya yang sudah dibayarkan dapat
dikembalikan (lihat klausul PEMBAYARAN, poin <i>REFUND</i>) setelah dikurangi
biaya administrasi dan  biaya yang sudah dipesankan atau dibelanjakan ke
provider jasa (Maskapai, Hotel, Visa, Logistik, komisi PILIHUMROH, dan
sebagainya) oleh TRAVEL UMROH, atau menjadi deposit calon jamaah di
pilihumroh.com setelah ada pemotongan biaya administrasi dari PILIHUMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>c.<span style='font:7.0pt "Times New Roman"'>
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Pembatalan
dilakukan setelah 14 (empat belas) hari sebelum tanggal keberangkatan, semua
biaya yang sudah dibayarkan tidak dapat dikembalikan</span><span lang=IN
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>PEMBATALAN DILAKUKAN </span></b><b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>TRAVEL UMROH</span></b><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, apabila
pembatalan dilakukan oleh TRAVEL UMROH, maka calon jamaah berhak mendapatkan
kembali seluruh dana yang telah bayarkan untuk pemesanan paket perjalanan umroh
dari TRAVEL UMROH tanpa potongan apapun dan TRAVEL UMROH harus mengembalikan dana
calon jamaah ke rekening PILIHUMROH paling lambat 3 (tiga) hari kerja.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>KETENTUAN RESCHEDULE</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>,</span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'> skema reschedule dapat terjadi bila
dilakukan oleh calon jamaah atau TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>RESCHEDULE DILAKUKAN OLEH CALON JAMAAH</span></b><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, ketentuan
apabila reschedule dilakukan oleh calon jamaah adalah sebagai berikut:</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>a.<span style='font:7.0pt "Times New Roman"'>
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Apabila
permintaan reschedule dilakukan sebelum JATUH TEMPO PELUNASAN, maka calon jamaah
akan dikenakan potongan biaya administrasi, biaya booking tiket, dan selisih <i>down
payment</i> paket perjalanan umroh yang dipilih sebagai pengganti oleh calon
jamaah. Selain itu, calon jamaah dapat mengajukan perubahan nama dan berlaku
KETENTUAN PERUBAHAN NAMA CALON JAMAAH.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>b.<span style='font:7.0pt "Times New Roman"'>
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Apabila
permintaan reschedule dilakukan setelah </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>JATUH TEMPO PELUNASAN hingga 14 (empat
belas) hari sebelum tanggal keberangkatan, maka calon jamaah akan dikenakan
potongan biaya yang sudah dibelanjakan oleh TRAVEL UMROH dan selisih harga
paket perjalanan umroh yang dipilih oleh calon jamaah sebagai pengganti. </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>c.<span style='font:7.0pt "Times New Roman"'>
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Apabila
permintaan reschedule dilakukan setelah 14 (empat belas) hari sebelum tanggal
keberangkatan, maka berlaku KETENTUAN PEMBATALAN. </span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>RESCHEDULE DILAKUKAN OLEH TRAVEL UMROH</span></b><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>, apabila
reschedule dilakukan oleh TRAVEL UMROH maka berlaku klausul JAMINAN
KEBERANGKATAN. </span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>KETENTUAN PERUBAHAN NAMA CALON JAMAAH</span></b><span
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>,</span><span
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'> skema perubahan
nama calon jamaah dapat dilakukan sebelum JATUH TEMPO PELUNASAN, dengan
ketentuan seluruh dokumen calon jamaah pengganti harus sudah tersedia dan
memenuhi persyaratan TRAVEL UMROH.</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:7.1pt;text-align:justify;
text-indent:28.9pt;line-height:115%'><b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>PERSELISIHAN</span></b></p>

<p class=MsoBodyText align=left style='margin-left:71.45pt;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>AUDIT</span></b><span style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'> </span><span style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>(bukti lengkap), jika terjadi perselisihan, perbedaan,
atau audit oleh pihak yang berwenang secara hukum, TRAVEL UMROH akan membuat
bukti dengan </span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>standar </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>akuntansi profesional yang telah
disepakati, dan TRAVEL UMROH akan membuat bukti lengkap atas terpenuhi nya
fitur yang dijanjikan kepada jamaah di paket umroh yang dipesan oleh calon jamaah.
</span></p>

<p class=MsoBodyText align=left style='margin-left:71.45pt;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>PENANGGUHAN</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'>jika PILIHUMROH menangguhkan layanan ke TRAVEL
UMROH, ini artinya TRAVEL UMROH tidak bisa menerima pemesanan baru, dan tidak
akan terlihat di Situs. Tanpa mengindahkan hak-hak lain yang mungkin PILIHUMROH
pilih untuk dijalankan, termasuk pemutusan hubungan, PILIHUMROH dapat
menangguhkan layanan yang diberikan kepada TRAVEL UMROH karena alasan apa pun,
termasuk, namun tidak terbatas pada: </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH memajang informasi yang salah atau menyesatkan di <i>Dashboard Travel</i>;
</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH gagal memelihara informasi di <i>Dashboard Travel</i> yang mengakibatkan
pemesanan berlebihan di TRAVEL UMROH; </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH gagal menerima pemesanan berdasarkan harga yang tercantum pada pemesanan;
</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH melebihkan tagihan; </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>e.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>PILIHUMROH
menerima satu atau beberapa keluhan yang sah dan serius dari salah satu atau
beberapa calon jamaah atau jamaah; </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>f.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Penyalahgunaan
proses ulasan jamaah oleh TRAVEL UMROH; </span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>g.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Masalah
tarif, ketersediaan, dan/atau masalah Jaminan Kesamaan Promosi yang berulang
atau berkelanjutan;</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>h.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN-ID style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>P</span><span style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>erilaku yang tidak pantas atau tidak profesional
terhadap calon jamaah atau staf PILIHUMROH;</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH terindikasi tidak sehat secara finansial dan/atau melakukan pelanggaran
hukum yang berlaku di Republik Indonesia yang akan mengancam kepastian
keberangkatan calon jamaah;</span></p>

<p class=MsoBodyText align=left style='margin-left:1.5in;text-align:justify;
text-indent:-15.85pt;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>j.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>TRAVEL
UMROH melanggar ketentuan mana pun dari Perjanjian ini.</span></p>

<p class=MsoBodyText align=left style='margin-left:71.45pt;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>TUNTUTAN HUKUM</span></b><span style='font-size:11.0pt;line-height:
115%;font-family:Montserrat'>,</span><span style='font-size:10.0pt;line-height:
115%;font-family:Montserrat'> PILIHUMROH terbebas dari segala tuntutan hukum
yang diakibatkan oleh kelalaian TRAVEL UMROH dalam pengelolaan dana calon
jamaah. </span></p>

<p class=MsoBodyText align=left style='margin-left:71.45pt;text-align:justify;
text-indent:-.25in;line-height:115%'><span style='font-size:7.0pt;line-height:
115%;font-family:Montserrat'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:115%;font-family:
Montserrat'>PENYELESAIAN MASALAH</span></b><span style='font-size:11.0pt;
line-height:115%;font-family:Montserrat'>, </span><span style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>TRAVEL UMROH bersedia untuk
bertanggung jawab atas segala masalah dan/atau tuntutan hukum yang terjadi akibat
kelalaian TRAVEL UMROH dan melakukan langkah-langkah penyelesaian baik secara
kekeluargan maupun hukum yang berlaku.</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText style='margin-left:35.45pt;line-height:115%'><b><span
lang=EN-ID style='font-size:11.0pt;line-height:115%;font-family:Montserrat'>KEAMANAN
SISTEM</span></b><span lang=EN-ID style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>, </span><span lang=EN-ID style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>PILIHUMROH menggunakan cara-cara
teknis dan keamanan yang tepat dan wajar untuk menjaga Aplikasi aman dan
terbebas dari virus dan kesalahan. Namun demikian, bagaimanapun efektifnya
teknologi ini, tidak ada sistem keamanan yang tidak dapat ditembus. Oleh karena
itu kami tidak dapat menjamin keamanan database kami dan kami juga tidak dapat
menjamin bahwa informasi yang anda berikan tidak akan ditahan/terganggu saat
sedang dikirimkan kepada kami. Aplikasi ini dapat mengalami keterbatasan,
penundaan, dan masalah-masalah lain yang terdapat dalam penggunaan internet dan
komunikasi elektronik, termasuk perangkat yang digunakan oleh anda atau
Penyedia Layanan rusak, tidak terhubung, berada di luar jangkauan, dimatikan
atau tidak berfungsi. Kami tidak bertanggung jawab atas keterlambatan,
kegagalan pengiriman, kerusakan atau kerugian yang diakibatkan oleh
masalah-masalah tersebut.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=IN style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></b></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><b><span lang=IN style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>FORCE MAJEURE</span></b><span lang=IN style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=IN
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Setiap
kegagalan PARA PIHAK untuk melaksanakan setiap kewajibannya berdasarkan Kontrak
Kerjasama ini, tidak akan dianggap sebagai suatu pelanggaran Kontrak Kerjasama
ataupun kelalaian jika kegagalan tersebut disebabkan oleh “keadaan memaksa”,
apabila pihak yang bersangkutan telah melakukan semua langkah pengamanan yang
sesuai, telah betul-betul menjaga dan mengambil langkah-langkah pilihan yang
wajar dengan tujuan untuk menghindarkan kegagalan tersebut dan untuk
melaksanakan kewajiban-kewajibannya berdasarkan Kontrak Kerjasama ini.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=IN style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>Untuk maksud Perjanjian ini, “keadaan memaksa” meliputi
antara lain : peperangan, pemberontakan, kerusuhan sipil, blokade, sabotase,
embargo, pemogokan dan perselisihan perburuhan lainnya, keributan, epidemi, gempa
bumi, angin ribut, banjir atau keadaan cuaca lainnya yang merugikan, ledakan,
kebakaran, perintah atau petunjuk yang merugikan dari setiap pemerintah “<i>de
jure</i>” ataupun “<i>de facto</i>” atau perangkatnya atau sub-divisinya,
kerusakan pada kegiatan Pekerjaan dan setiap sebab lainnya yang secara wajar
tidak dapat dikuasai oleh pihak yang terkena sebab-sebab tersebut, dan yang
sifatnya sedemikian rupa, sehingga mengakibatkan penundaan, pembatasan atau
menghalangi tindakan tepat pada waktunya oleh pihak yang terkena pengaruh</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=IN style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>Pihak yang mengalami “keadaan memaksa” tersebut paling
lama dalam waktu 2 (dua) kali 24 (dua puluh empat) jam sejak “keadaan memaksa”
tersebut terjadi harus memberitahukan kepada Pihak yang dirugikan tersebut.</span></p>

<p class=MsoBodyText align=left style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:115%'><span lang=IN style='font-size:10.0pt;
line-height:115%;font-family:Montserrat'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=IN style='font-size:10.0pt;line-height:115%;
font-family:Montserrat'>Penyelesaian Pekerjaan yang tersisa akibat terjadinya
“keadaan memaksa“ akan dilaksanakan setelah “keadaan memaksa“ tersebut berakhir
yang mana waktu penyelesaian akan dibicarakan dan disepakati oleh Para Pihak.</span></p>

<p class=MsoBodyText align=left style='margin-left:35.45pt;text-align:justify;
line-height:115%'><span lang=IN style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='margin-left:.5in;text-align:justify;
line-height:115%'><b><span lang=IN style='font-size:11.0pt;line-height:115%;
font-family:Montserrat'>LAIN-LAIN</span></b><span lang=IN style='font-size:
11.0pt;line-height:115%;font-family:Montserrat'>, </span><span lang=IN
style='font-size:10.0pt;line-height:115%;font-family:Montserrat'>Segala sesuatu
yang belum diatur dalam Kontrak Kerjasama ini akan dibicarakan dan diselesaikan
secara musyawarah antara <b>PARA PIHAK </b>dan akan dimuat dalam suatu <i>addendum</i>
Perjanjian Kontrak Kerjasama yang disepakati oleh <b>PARA PIHAK </b>yang
merupakan bagian tidak terpisahkan dari Kontrak Kerjasama ini.</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
lang=EN-ID style='line-height:115%;font-family:Montserrat'>&nbsp;</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><b><span
lang=EN-ID style='line-height:115%;font-family:Montserrat'>Alhamdulillahirobbilalamin</span></b><span
lang=EN-ID style='line-height:115%;font-family:Montserrat'>,</span></p>

<p class=MsoBodyText align=left style='text-align:justify;line-height:115%'><span
lang=EN-ID style='line-height:115%;font-family:Montserrat'>&nbsp;</span></p>

</div>

						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				
			 	<div class="clearfix"></div>
			</div>
		</div>
	</div><!--end detail-wrap-->


@endsection