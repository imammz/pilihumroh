@extends('public.part.layout')
@section('content')
<div class="header "><!--header-->

    <div class="clearfix"></div>
    <div class="big-carousel"><!--big carousel-->
        <div id="bigCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#bigCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#bigCarousel" data-slide-to="1"></li>
                <li data-target="#bigCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <a href="">
                        <img title="Biaya umroh 2018 terupdate dari pilihumroh.com" alt="Biaya umroh 2018 terupdate dari pilihumroh.com" src="{{asset('assets/img/slide/1.jpg')}}" type="image">
                    </a>
                </div>

                @for ($i=2; $i<=10; $i++)
                <div class="item">
                    <a href="">
                        <img title="Biaya umroh 2018 terupdate dari pilihumroh.com" alt="Biaya umroh 2018 terupdate dari pilihumroh.com" src="{{asset('assets/img/slide/'.$i.'.jpg')}}" type="image">
                    </a>
                </div>
                @endfor

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#bigCarousel" role="button" data-slide="prev">
                <span class="glyphicon  glyphicon-triangle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#bigCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div><!--end big carousel-->
    <div class="clearfix"></div>
    <div class="container tab-paket"><!--container tab promo & sponsor-->
        <ul id="Tabs1" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home"  id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Paket Umroh</a></li>
            <!--<li role="presentation" ><a href="#profile"  role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Paket Saudi</a></li>-->
        </ul>
        <div id="myTabContent" class="tab-content"><!--tab1-->
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                <div class="title-cari">Dapatkan perbandingan harga paket umroh murah dengan cepat dan mudah</div>
                <div class="isi-paket">
                    <div class="col-sm-5 step1">
                        <div class="step-title">
                            <div class="step-no">1</div>
                            <div class="step-word">Pilih Tanggal
                                <div class="col-xs-12">    <div class="p-step">
                                        Rentang Waktu Keberangkatan
                                        <br/><sub>Format : (bulan)/(tanggal)/(tahun)</sub></div> </div>
                            </div>

                        </div>


                        <div class="well">

                            <div class="row">   
                                <div class="col-xs-12">   </div>
                                <div class="col-xs-5">
                                    <span class="ico-cal"></span><input type="date" name="tgl_dari" class="inp-cal" data-date="DD-MMMM-YYYY" format="DD-MMMM-YYYY" data-date-format="DD-MMMM-YYYY" value="{{date('Y-m-d')}}" >
                                </div>
                                <div class="col-xs-2">
                                    S/D
                                </div>
                                <div class="col-xs-5">
                                    <span class="ico-cal"></span><input type="date" name="tgl_sampai" class="inp-cal" data-date="DD-MMMM-YYYY" format="DD-MMMM-YYYY" data-date-format="DD-MMMM-YYYY" value="{{date('Y-m-d')}}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 step2">
                        <div class="step-title">
                            <div class="step-no">2</div>
                            <div class="step-word">Pilih Kota</div>
                        </div>
                        <div class="col-xs-12"> 
                            <p class="p-step">Berangkat dari:</p>
                        </div>
                        <select class="form-control select-css" name="berangka">

                            @foreach($maskapai as $row)
                            <option value="{{$row->kode}}">{{$row->kode}} - {{$row->namaProv}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 step4">
                        <div class="step-title">
                            <div class="step-no">3</div>
                            <div class="step-word">Pilih  Paket</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12"> 
                            <div class="p-step"><br><sup>Jenis Paket  Umroh</sup></div>
                        </div>
                        <select class="form-control select-css">

                            @foreach($jenispaket as $row)
                            <option value="{{$row->idJenisPaket}}">{{$row->nama}}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="col-sm-12">
                        <div class="col-sm-4" style="text-align: center;"> </div>
                        <div class="col-sm-4" style="text-align: center;">
                            <button class="cari-paket hvr-radial-out">Cari Paket Umroh Terbaik</button>
                        </div>
                        <div class="col-sm-4" style="text-align: center;"> </div>

                    </div>

                </div>
                <!--		      <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                                        <div class="isi-paket">
                                                <div class="no-more-tables">
                                                                
                                                        </div>
                                                        </div>
                                      </div>-->
                <!--end tab-->
            </div>
        </div><!--container tab promo & sponsor-->
    </div><!--end header-->
    <div class="clearfix"></div>
    <div class="home-wrap"><!--home-wrap-->

        <div class="container">

            <div class="clearfix"></div>
            <div class="home-isi animatedParent"><!-- home-isi-->
                <div class="col-sm-5  home-left animated fadeInLeftShort">
                    <h2>Paket Umroh Terbaru</h2>

                    <!-- loop -->
                    @foreach($paketTerbaru as $row)
                    @php
                    $datePaket = explode('-',$row->tglBerangkat);
                    $tglPaket = $date['2'];
                    $blnPaket = $date['1'];
                    $thPaket = $date['0'];


                    $datetime1 = new DateTime($row->tglBerangkat);
                    $datetime2 = new DateTime($row->tglPulang);
                    $totalHari = $datetime1->diff($datetime2);

                    @endphp
                    <div class="paket-list hvr-grow">
                        <div class="list-atas">
                            <div class="col-sm-9 pl-0">
                                <div class="list-tgl">
                                    <p>{{$tglPaket}}</p>
                                    <span>{{$blnPaket}}'{{$thPaket}}</span>
                                </div>
                                <div class="list-ket">
                                    <p>
                                        <a href="{{url('/')}}">
                                            {{$totalHari->days}} Hari
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-3 fr">
                                <div class="list-logo">
                                    <img src="{{asset('travel/logo/'.$row->fileLogo)}}">
                                </div>
                            </div>
                        </div>
                        <div class="list-bwh">
                            <div class="col-sm-9 pl-0">
                                <p class="l-old-price">{{$row->idCurency}} {{$row->hargaPromo}}</p>
                                <p class="l-new-price">{{$row->idCurency}} {{$row->hargaUtama}}</p>
                            </div>
                            <div class="col-sm-3 fr">
                                <p class="tour-name">{{$row->nama}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach 


                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-7 home-right animated fadeInRightShort">
                    <h2>Jadwal Keberangkatan</h2>

                    <div class="clearfix"></div>

                    <div class="clearfix"></div>
                    <div class='calendar'></div>
                </div>
                <div class="clearfix"></div>
            </div><!-- end home-isi-->
        </div>
    </div><!--end home-wrap-->
    <div class="clearfix"></div>
    <div class="ustad-wrap animatedParent"><!-ustad-wrap-->
        <div class="container ustad animated fadeInDownShort">
            <h2>Berita Terkini</h2>	
            <div class="ustad-slide">
                <div id="ustadSlide" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <!-- loop limit 3 -->
                            @foreach($berita as $row)
                            @php
                                $isi = substr($row->isi, 0, 200);
                            @endphp
                            <div class="col-sm-4 pl-0 ustad-list">
                                <div class="ustad-box hvr-float">
                                    <div class="ustad-up">
                                        <div class="ust-blog-img"><img src="{{asset('contens/news/'.$row->thumbnail)}}"></div>
                                        <h3>{{$row->judul}}.</h3>
                                        <div class="clearfix"></div>
                                        <div class="ustad-l">
                                            <h4>{{$row->author}}</h4>
                                        </div>
                                        <div class="ustad-r">
                                            <p>{{$row->tglTerbit}}</p>
                                        </div>
                                    </div>
                                    <div class="ustad-down">
                                        <p>{{$isi}}.</p>
                                        <a href="{{url($idContent.'/detail')}}">Selengkapnya <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>

                    </div>
                    <a class="ustad-lain hvr-grow" href="{{url('berita/list')}}">Lihat Berita Lainnya</a>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#ustadSlide" data-slide-to="0" class="active"></li>
                    </ol>
                </div><!--end test slide-->
            </div>
        </div>
    </div><!--end ustad-wrap-->
    <div class="clearfix"></div>
    <div class="alasan-wrap animatedParent"><!--alasan-wrap-->
        <div class="container alasan animated fadeInUpShort">
            <h2>Keunggulan Pilih Umroh</h2>
            <div class="alasan-point">

                <div class="alasan-col">
                    <div class="alasan-img hvr-push">
                        <img src="{{asset('assets/img/reason2.png')}}">
                    </div>
                    <div class="alasan-no">1</div>
                    <h3>Pencarian Terlengkap</h3>
                    <p style="text-align: justify;">Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
                </div>
                <div class="alasan-col">
                    <div class="alasan-img hvr-push">
                        <img src="{{asset('assets/img/reason3a.png')}}">
                    </div>
                    <div class="alasan-no">2</div>
                    <h3>Harga Transparan</h3>
                    <p style="text-align: justify;">PilihUMROH.com menjamin bahwa harga paket yang tercantum di website adalah harga total (“Harga Transparan”) dari seluruh komponen biaya perjalanan umroh.</p>
                </div>
                <div class="alasan-col">
                    <div class="alasan-img hvr-push">
                        <img src="{{asset('assets/img/reason4a.png')}}">
                    </div>
                    <div class="alasan-no">3</div>
                    <h3>Harga sudah termasuk pajak</h3>
                    <p style="text-align: justify;">Labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
                <div class="alasan-col">
                    <div class="alasan-img hvr-push">
                        <img src="{{asset('assets/img/reason1.png')}}">
                    </div>
                    <div class="alasan-no">4</div>
                    <h3>Transaksi umroh paling aman dan terpercaya</h3>
                    <p style="text-align: justify;">PilihUMROH.com dioperasikan oleh PT PILIH UMROH AMANAH, kami bekerjasama dengan travel beserta afiliasinya yang berizin resmi dan terdaftar di kementrian agama. Dengan seleksi yang ketat, travel yang menjadi partner kami adalah travel yang terpercaya dan sudah memliki reputasi di bidang jasa perjalanan ibadah umroh.</p>
                </div>
                <div class="alasan-col">
                    <div class="alasan-img hvr-push">
                        <img src="{{asset('assets/img/reason5a.png')}}">
                    </div>
                    <div class="alasan-no">5</div>
                    <h3>Mudah Dan Praktis</h3>
                    <p style="text-align: justify;">Mencari dan memesan paket umroh di PilihUMROH.com sangat praktis dan mudah, Anda dapat membandingkan berbagai paket umroh hanya dengan satu, dan Anda bisa langsung melakukan pendaftaran secara online. Call Center kami pun siap membantu Anda selama 24 jam</p>
                </div>
            </div>
        </div>
    </div><!--end alasan-wrap-->
    <div class="clearfix"></div>
    <div class="partner-wrap animatedParent"><!--partner-wrap-->
        <div class="container partner animated fadeIn">
            <h2>Travel Tergabung</h2>
            <div id="partnerSlide" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <div class="item active">  
                        <ul class="partner-list">
                            @foreach($travelagent as $row)
                            <li class="hvr-bob"><img src="{{asset('travel/logo/'.$row->fileLogo)}}"></li>
                            @endforeach
                        </ul> 
                    </div>


                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#partnerSlide" data-slide-to="0" class="active"></li>
                </ol>

            </div>	
            <a href="{{url('travel/list')}}" class="hvr-grow">Lihat Partner Lainnya</a>
        </div>
    </div><!--end partner-wrap-->
    <div class="clearfix"></div>
    <div class="testi-wrap animatedParent"><!--testi-wrap-->
        <div class="container testi animated fadeInDownShort">
            <h2>Apa kata mereka tentang Pilih Umroh?</h2>	
            <div class="test-slide">
                <div id="testSlide" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">

                        <div class="item active">

                            @foreach($testi as $row)
                            <div class="col-sm-4 ">
                                <div class="testi-full">
                                    <p><i class="fa fa-quote-left"></i>{{$row->isi}}<i class="fa fa-quote-right"></i></p>
                                </div>
                                <div class="testi-l">
                                    <img src="{{asset('contents/users/'.$row->fileLogo)}}">
                                </div>
                                <div class="testi-r ">
                                    <span>{{$row->author}}</span>
                                </div>
                            </div>
                             @endforeach   

                        </div>


                    </div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#testSlide" data-slide-to="0" class="active"></li>
                    </ol>

                    <a class="ustad-lain hvr-grow" href="{{url('testi/list')}}">Lihat Testimoni Lainnya</a>
                </div><!--end test slide-->
            </div>
        </div>  
    </div><!--end testi-wrap-->
    <div class="clearfix"></div>
    <div class="rss-wrap animatedParent"><!--end rss-wrap-->
        <div class="container rss animated pulse">
            <h2>Daftarkan email Anda untuk info tiket PROMO, hotel dan semua diskon spesial Pilih Umroh hingga 70%, GRATIS</h2>
            <div class="rss-isi">	
                <input type="text" placeholder="Daftarkan email Anda di sini"  value="" id="textfield"  class="hvr-grow" style="color: #ffffff; margin-right: 30px;">
                <button type="button" class="btn btn-primary btn-sm">Daftar</button>

            </div>
        </div>
    </div><!--end rss-wrap-->
    @endsection

    <script>



        $(function () {
            $("#tgl").datepicker({dateFormat: 'yy-mm-dd'});
        });




    </script>