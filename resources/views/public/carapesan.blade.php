@extends('public.part.layout')
@section('content')

       <div class="big-head"><!--big head-->
			<div class="container">
				<div class="big-isi big-ustad">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div><!--end big head-->

	<div class="home-wrap cwrap"><!--home-wrap-->
            <div class="container" style="margin-top: 140px;">
			<div class="tab-cara">
				<!-- Nav tabs -->
				  <ul class="nav nav-tabs cara-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#phaji" aria-controls="home" role="tab" data-toggle="tab">Cara Pesan Paket Umroh</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="phaji">
				    	<div class="progress">
				    		<div class="scroll-progress-container"></div>
							<div class="scroll-progress"></div>
				    	</div>
				    	<div class="animatedParent" data-sequence='500'>
				    		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  <div class="panel cara">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <div class="cara-l animated fadeInLeftShort" data-id='1'>
							      	<h3>Cari Paket</h3>
							      	<p>Mulai pencarian Anda di halaman muka pilihumroh.com dan masukkan data-data penerbangannya</p>
							      	<div class="clearfix"></div>
							        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="hvr-fade">
							          Lihat Selengkapnya
							        </a>
							      </div>
							      <div class="cara-c"><p>1</p></div>
							      <div class="cara-r animated fadeInRightShort" data-id='2'>
							      	<div class="cara-r-img"><img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-device1.png"></div>
							      </div>
							      <div class="clearfix"></div>
							    </div>
							    <div class="clearfix"></div>
							    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">
							        <div class="cpanel-l">
							        	<img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-explain-1.png">
							        </div>
							        <div class="cpanel-r">
						                <div style="margin-top: 50px;">
						                  <p>
						                      Buka <strong>pilihumroh.com</strong>
						                  </p>
						                </div>
						                <div style="margin-top: 38px; ">
						                    <p>Masukkan data yang diperlukan: kota Asal, Kota Tujuan, Tanggal berangkat, Tanggal Pulang (jika ada)</p>
						                </div>
						                <div style="margin-top: 30px; ">
						                    <ul>
						                        <li><span>Penumpang dapat dikategorikan sebagai:</span>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon adult"></div>
						                                    <div class="floatListLabel">Dewasa (&gt;12 tahun)</div>
						                                </li>
						                                <li>
						                                    <div class="floatListIcon child"></div>
						                                    <div class="floatListLabel">Anak (2-12 tahun)</div>
						                                </li>
						                            </ul>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon infant"></div>
						                                    <div class="floatListLabel">Bayi (0-2 tahun)</div>
						                                </li>
						                            </ul>
						                        </li>
						                    </ul>
						                </div>

							        </div>
							      </div>
							    </div>
							  </div>
							  <div class="clearfix"></div>
							  <div class="panel  cara c-inv">
							    <div class="panel-heading" role="tab" id="headingTwo">
							      <div class="cara-r animated fadeInLeftShort" data-id='3'>
							      	<div class="cara-r-img"><img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-device1.png"></div>
							      </div>
							      <div class="cara-c"><p>2</p></div>
							      <div class="cara-l animated fadeInRightShort" data-id='4'>
							      	<h3>Pilih dan Pesan Paket</h3>
							      	<p>Di halaman hasil pencarian, Anda bisa langsung memilih penerbangan yang diinginkan. Tekan tombol pesan untuk memesan penerbangan itu</p>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          Lihat Selengkapnya
							        </a>
							      </div>
							    </div>
							    <div class="clearfix"></div>
							    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							      <div class="panel-body">
							        <div class="cpanel-l">
							        	<img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-explain-1.png">
							        </div>
							        <div class="cpanel-r">
						                <div style="margin-top: 50px;">
						                  <p>
						                      Buka <strong>pilihumroh.com</strong>
						                  </p>
						                </div>
						                <div style="margin-top: 38px; ">
						                    <p>Masukkan data yang diperlukan: kota Asal, Kota Tujuan, Tanggal berangkat, Tanggal Pulang (jika ada)</p>
						                </div>
						                <div style="margin-top: 30px; ">
						                    <ul>
						                        <li><span>Penumpang dapat dikategorikan sebagai:</span>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon adult"></div>
						                                    <div class="floatListLabel">Dewasa (&gt;12 tahun)</div>
						                                </li>
						                                <li>
						                                    <div class="floatListIcon child"></div>
						                                    <div class="floatListLabel">Anak (2-12 tahun)</div>
						                                </li>
						                            </ul>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon infant"></div>
						                                    <div class="floatListLabel">Bayi (0-2 tahun)</div>
						                                </li>
						                            </ul>
						                        </li>
						                    </ul>
						                </div>

							        </div>
							      </div>
							    </div>
							  </div>
							  <div class="clearfix"></div>
							  <div class="panel  cara">
							    <div class="panel-heading" role="tab" id="headingThree">
							      <div class="cara-l animated fadeInLeftShort" data-id='5'>
							      	<h3>Isi data kontak dan penumpang</h3>
							      	<p>Setelah memilih penerbangan, kini Anda harus mengisi data pemesan yang dapat dihubungi serta data dari penumpang yang berangkat</p>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          Lihat Selengkapnya
							        </a>
							      </div>
							      <div class="cara-c"><p>3</p></div>
							      <div class="cara-r animated fadeInRightShort" data-id='6'>
							      	<div class="cara-r-img"><img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-device1.png"></div>
							      </div>
							    </div>
							    <div class="clearfix"></div>
							    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							      <div class="panel-body">
							        <div class="cpanel-l">
							        	<img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-explain-1.png">
							        </div>
							        <div class="cpanel-r">
						                <div style="margin-top: 50px;">
						                  <p>
						                      Buka <strong>pilihumroh.com</strong>
						                  </p>
						                </div>
						                <div style="margin-top: 38px; ">
						                    <p>Masukkan data yang diperlukan: kota Asal, Kota Tujuan, Tanggal berangkat, Tanggal Pulang (jika ada)</p>
						                </div>
						                <div style="margin-top: 30px; ">
						                    <ul>
						                        <li><span>Penumpang dapat dikategorikan sebagai:</span>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon adult"></div>
						                                    <div class="floatListLabel">Dewasa (&gt;12 tahun)</div>
						                                </li>
						                                <li>
						                                    <div class="floatListIcon child"></div>
						                                    <div class="floatListLabel">Anak (2-12 tahun)</div>
						                                </li>
						                            </ul>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon infant"></div>
						                                    <div class="floatListLabel">Bayi (0-2 tahun)</div>
						                                </li>
						                            </ul>
						                        </li>
						                    </ul>
						                </div>

							        </div>
							      </div>
							    </div>
							  </div>
							  <div class="clearfix"></div>
							  <div class="panel  cara c-inv">
							    <div class="panel-heading" role="tab" id="headingFour">
							      <div class="cara-r animated fadeInLeftShort" data-id='7'>
							      	<div class="cara-r-img"><img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-device1.png"></div>
							      </div>
							      <div class="cara-c"><p>4</p></div>
							      <div class="cara-l animated fadeInRightShort" data-id='8'>
							      	<h3>Pilih dan Pesan Paket</h3>
							      	<p>Di halaman hasil pencarian, Anda bisa langsung memilih penerbangan yang diinginkan. Tekan tombol pesan untuk memesan penerbangan itu</p>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseTwo">
							          Lihat Selengkapnya
							        </a>
							      </div>
							    </div>
							    <div class="clearfix"></div>
							    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
							      <div class="panel-body">
							        <div class="cpanel-l">
							        	<img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-explain-1.png">
							        </div>
							        <div class="cpanel-r">
						                <div style="margin-top: 50px;">
						                  <p>
						                      Buka <strong>pilihumroh.com</strong>
						                  </p>
						                </div>
						                <div style="margin-top: 38px; ">
						                    <p>Masukkan data yang diperlukan: kota Asal, Kota Tujuan, Tanggal berangkat, Tanggal Pulang (jika ada)</p>
						                </div>
						                <div style="margin-top: 30px; ">
						                    <ul>
						                        <li><span>Penumpang dapat dikategorikan sebagai:</span>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon adult"></div>
						                                    <div class="floatListLabel">Dewasa (&gt;12 tahun)</div>
						                                </li>
						                                <li>
						                                    <div class="floatListIcon child"></div>
						                                    <div class="floatListLabel">Anak (2-12 tahun)</div>
						                                </li>
						                            </ul>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon infant"></div>
						                                    <div class="floatListLabel">Bayi (0-2 tahun)</div>
						                                </li>
						                            </ul>
						                        </li>
						                    </ul>
						                </div>

							        </div>
							      </div>
							    </div>
							  </div>
							  <div class="clearfix"></div>
							  <div class="panel  cara">
							    <div class="panel-heading" role="tab" id="headingFive">
							      <div class="cara-l animated fadeInLeftShort" data-id='9'>
							      	<h3>Isi data kontak dan penumpang</h3>
							      	<p>Setelah memilih penerbangan, kini Anda harus mengisi data pemesan yang dapat dihubungi serta data dari penumpang yang berangkat</p>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
							          Lihat Selengkapnya
							        </a>
							      </div>
							      <div class="cara-c"><p>5</p></div>
							      <div class="cara-r animated fadeInRightShort" data-id='10'>
							      	<div class="cara-r-img"><img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-device1.png"></div>
							      </div>
							    </div>
							    <div class="clearfix"></div>
							    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
							      <div class="panel-body">
							        <div class="cpanel-l">
							        	<img src="http://da8hvrloj7e7d.cloudfront.net/asset/images/HowtoPage/HowtoFlight/flight-explain-1.png">
							        </div>
							        <div class="cpanel-r">
						                <div style="margin-top: 50px;">
						                  <p>
						                      Buka <strong>pilihumroh.com</strong>
						                  </p>
						                </div>
						                <div style="margin-top: 38px; ">
						                    <p>Masukkan data yang diperlukan: kota Asal, Kota Tujuan, Tanggal berangkat, Tanggal Pulang (jika ada)</p>
						                </div>
						                <div style="margin-top: 30px; ">
						                    <ul>
						                        <li><span>Penumpang dapat dikategorikan sebagai:</span>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon adult"></div>
						                                    <div class="floatListLabel">Dewasa (&gt;12 tahun)</div>
						                                </li>
						                                <li>
						                                    <div class="floatListIcon child"></div>
						                                    <div class="floatListLabel">Anak (2-12 tahun)</div>
						                                </li>
						                            </ul>
						                            <ul class="floatListCntr passengerList">
						                                <li>
						                                    <div class="floatListIcon infant"></div>
						                                    <div class="floatListLabel">Bayi (0-2 tahun)</div>
						                                </li>
						                            </ul>
						                        </li>
						                    </ul>
						                </div>

							        </div>
							      </div>
							    </div>
							  </div>
							</div>
				    		
				    	</div>
				    </div>
				  </div>
				  <div class="cust-gate">
				  	<p>Jika Anda mengalami kendala dalam pemesanan, atau jika e-tiket belum diterima dalam waktu lebih dari 60 menit, silakan hubungi Customer Service kami yang siap melayani Anda selama 24 jam nonstop.</p>
				  	<a href="{{url('hubungikami')}}" class="hvr-fade">Hubungi Customer Service Kami</a>
				  </div>
			</div>
		</div>
	</div><!--end home-wrap-->


@endsection