@extends('public.part.layout')
@section('content')

       <div class="big-head"><!--big head-->
			<div class="container">
				<div class="big-isi big-ustad">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div><!--end big head-->

	<div class="home-wrap"><!--home-wrap-->
		<div class="container">
                    <div class="detail-wrap"><!--detail-wrap-->
		<div class="container ">
			<div class="ustad-wrapper">
				
				<div class="scroll">
				 	<div class="col-sm-12 testimon-list">
				 		<div class="testi-pg-br hvr-grow">
					 		<div class="testimon-l">
					      			<div class="testimon-img"><img src="http://www.multivisual.nl/img/avatar.jpg"/></div>
					      			<div class="testi-ket">@TikaKoes</div>
				      		</div>
				      		<div class="testimon-r">
				      			<h5>Tika Koesmanto Purnamasari</h5>
				      			<div class="testimon-ket">
				      				<p class="paket-testi">Paket Perjalanan Ridho, Travel Maktour</p>
				      				<span> <i class="fa fa-plane"></i>17 Agustus - <i class="fa fa-plane fa-ylw"></i> 22 Agustus 2015</span>
				      			</div>
				      			<div class="testimon-isi">
				      				
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
					      			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>

				      			</div>
				      		</div>
					 		<div class="clearfix"></div>
				 		</div>
				 		<div class="testi-pg-br hvr-grow">
					 		<div class="testimon-l">
					      			<div class="testimon-img"><img src="http://www.multivisual.nl/img/avatar.jpg"/></div>
					      			<div class="testi-ket">@TikaKoes</div>
				      		</div>
				      		<div class="testimon-r">
				      			<h5>Tika Koesmanto Purnamasari</h5>
				      			<div class="testimon-ket">
				      				<p class="paket-testi">Paket Perjalanan Ridho, Travel Maktour</p>
				      				<span> <i class="fa fa-plane"></i>17 Agustus - <i class="fa fa-plane fa-ylw"></i> 22 Agustus 2015</span>
				      			</div>
				      			<div class="testimon-isi">
				      				
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
					      			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      			
				      			</div>
				      		</div>
					 		<div class="clearfix"></div>
				 		</div>
				 		<div class="testi-pg-br hvr-grow">
					 		<div class="testimon-l">
					      			<div class="testimon-img"><img src="http://www.multivisual.nl/img/avatar.jpg"/></div>
					      			<div class="testi-ket">@TikaKoes</div>
				      		</div>
				      		<div class="testimon-r">
				      			<h5>Tika Koesmanto Purnamasari</h5>
				      			<div class="testimon-ket">
				      				<p class="paket-testi">Paket Perjalanan Ridho, Travel Maktour</p>
				      				<span> <i class="fa fa-plane"></i>17 Agustus - <i class="fa fa-plane fa-ylw"></i> 22 Agustus 2015</span>
				      			</div>
				      			<div class="testimon-isi">
				      				
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
					      			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      			
				      			</div>
				      		</div>
					 		<div class="clearfix"></div>
				 		</div>
				 		<div class="testi-pg-br hvr-grow">
					 		<div class="testimon-l">
					      			<div class="testimon-img"><img src="http://www.multivisual.nl/img/avatar.jpg"/></div>
					      			<div class="testi-ket">@TikaKoes</div>
				      		</div>
				      		<div class="testimon-r">
				      			<h5>Tika Koesmanto Purnamasari</h5>
				      			<div class="testimon-ket">
				      				<p class="paket-testi">Paket Perjalanan Ridho, Travel Maktour</p>
				      				<span> <i class="fa fa-plane"></i>17 Agustus - <i class="fa fa-plane fa-ylw"></i> 22 Agustus 2015</span>
				      			</div>
				      			<div class="testimon-isi">
				      				
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
					      			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      			
				      			</div>
				      		</div>
					 		<div class="clearfix"></div>
				 		</div>
				 		<div class="testi-pg-br hvr-grow">
					 		<div class="testimon-l">
					      			<div class="testimon-img"><img src="http://www.multivisual.nl/img/avatar.jpg"/></div>
					      			<div class="testi-ket">@TikaKoes</div>
				      		</div>
				      		<div class="testimon-r">
				      			<h5>Tika Koesmanto Purnamasari</h5>
				      			<div class="testimon-ket">
				      				<p class="paket-testi">Paket Perjalanan Ridho, Travel Maktour</p>
				      				<span> <i class="fa fa-plane"></i>17 Agustus - <i class="fa fa-plane fa-ylw"></i> 22 Agustus 2015</span>
				      			</div>
				      			<div class="testimon-isi">
				      				
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
					      			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      			
				      			</div>
				      		</div>
					 		<div class="clearfix"></div>
				 		</div>
				 		<div class="clearfix"></div>
				 	</div>

				 </div>
			 	<div class="clearfix"></div>
			</div>
		</div>
	</div><!--end detail-wrap-->

                </div>
	</div><!--end home-wrap-->

@endsection