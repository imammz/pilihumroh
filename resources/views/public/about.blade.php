@extends('public.part.layout')
@section('content')

       <div class="big-head"><!--big head-->
			<div class="container">
				<div class="big-isi big-ustad">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div><!--end big head-->

	<div class="home-wrap"><!--home-wrap-->
		<div class="container">
                    <div class="clearfix"></div>
	<div class="detail-wrap"><!--detail-wrap-->
		
		<div class="clearfix"></div>
		<div class="container animatedParent" data-sequence='500'>
			<div class="ustad-wrapper">
				
				<div class="clearfix"></div>
				<div class="about-wrap">
                                    <div class="animated fadeInLeftShort" data-id='1' style="text-align: justify">
                                        <p>PilihUMROH.com merupakan layanan pemesanan paket perjalanan umroh online. PilihUMROH.com menyediakan fitur pencarian paket perjalanan umroh yang cepat dan mudah. Selain itu, anda dapat memesan paket perjalanan umroh yang anda pilih, kami akan membantu anda agar dapat berangkat ke Tanah Suci dengan mudah dan aman</p>
                                    </div>
				
				</div>
			 	<div class="clearfix"></div>
			 	<div class="about-icon">
			 		<div class="alasan-wrap animatedParent"><!--alasan-wrap-->
		<div class="container alasan animated fadeInUpShort">
			<h2>Keunggulan Pilih Umroh</h2>
			<div class="alasan-point">
				
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="{{asset('assets/img/Home---Umroh_05.png')}}">
					</div>
					<div class="alasan-no">1</div>
					<h3>Pencarian Terlengkap</h3>
					<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="{{asset('assets/img/reason3a.png')}}">
					</div>
					<div class="alasan-no">2</div>
					<h3>Dijamin Harga Terbaik</h3>
					<p>Amet, consectetu adipisicing elit, sed do eiusmod tempor</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="{{asset('assets/img/reason4a.png')}}">
					</div>
					<div class="alasan-no">3</div>
					<h3>Harga sudah termasuk pajak</h3>
					<p>Labore et dolore magna aliqua. Ut enim ad minim veniam</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="{{asset('assets/img/reason5a.png')}}">
					</div>
					<div class="alasan-no">4</div>
					<h3>Transaksi Online Aman</h3>
					<p>Quis nostrud exercitation ullamcolaboris nisi ut aliquip ex</p>
				</div>
			</div>
		</div>
	</div><!--end alasan-wrap-->
			 	</div>
			 	<div class="clearfix"></div>
			</div>
		</div>

	</div><!--end detail-wrap-->

                </div>
	</div><!--end home-wrap-->

@endsection