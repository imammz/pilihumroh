@extends('public.part.layout')
@section('content')

       <div class="big-head"><!--big head-->
			<div class="container">
				<div class="big-isi big-ustad">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div><!--end big head-->

	<div class="home-wrap"><!--home-wrap-->
		<div class="container">
                    <div class="detail-wrap"><!--detail-wrap-->
		<div class="container ">
			<div class="ustad-wrapper">
				<div class="ustad-abv">
					<div class="clearfix"></div>
					<div class="ust-detail-wrap animatedParent"data-sequence='500'>
						<div class="animated fadeInDownShort kontak-isi" data-id='1'>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pellentesque odio quis quam egestas, ut condimentum quam tincidunt. Vestibulum pharetra nibh ipsum, non bibendum odio tincidunt eu. Sed suscipit porta facilisis.</p>
						</div>
						<div class="animated fadeInLeftShort kontak-detail" data-id='2'>
							<div class="ust-det-br">
								<h3><i class="fa fa-phone"></i>Hotline : 0856 9202 0540</h3>
								<div class="clearfix"></div>
								<div class="det-ust-prof">
					    			<h4>Alamat</h4>
					    			<p> Jl. Tentara Pelajar No.4, Ciwaringin, Bogor 16124</p>
					    			<div class="clearfix"></div>
				    			</div>
				    			
				    			<div class="det-ust-prof">
					    			<h4>Email</h4>
					    			<p>info@pilihrumroh.com</p>
					    			<div class="clearfix"></div>
				    			</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="animated fadeInRightShort mapsnya" data-id='3'>
							<div class="maps-cari">
		    					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4851.636124745005!2d106.78656971700545!3d-6.574363892314703!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c44680bf7eab%3A0xd6c311f690ccae98!2sJl.+Tentara+Pelajar+Cimanggu+Blok+Kompleks+Pertanian+No.4%2C+Ciwaringin%2C+Bogor+Tengah%2C+Kota+Bogor%2C+Jawa+Barat+16124!5e0!3m2!1sid!2sid!4v1520223617380" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		    				</div>
		    			</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				
			 	<div class="clearfix"></div>
			</div>
		</div>
	</div><!--end detail-wrap-->

                </div>
	</div><!--end home-wrap-->

@endsection


