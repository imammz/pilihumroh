@extends('public.part.layout')
@section('content')

       <div class="big-head"><!--big head-->
			<div class="container">
				<div class="big-isi big-ustad">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div><!--end big head-->

	<div class="home-wrap"><!--home-wrap-->
		<div class="container">
                    <div class="detail-wrap"><!--detail-wrap-->
		<div class="container ">
			<div class="ustad-wrapper">
				<div class="ustad-abv">
					<div class="clearfix"></div>
					<div class="ust-detail-wrap">
						<div class="faq-isi hvr-grow">
							<h3>Mengapa harus memesan paket umroh di PilihUMROH.com?</h3>
                                                        <p>
                                                        <h4> Transaksi umroh paling aman dan terpercaya </h4>
PilihUMROH.com dioperasikan oleh PT PILIH UMROH AMANAH, kami bekerjasama dengan travel beserta afiliasinya yang berizin resmi dan terdaftar di kementrian agama. Dengan seleksi yang ketat, travel yang menjadi partner kami adalah travel yang terpercaya dan sudah memliki reputasi di bidang jasa perjalanan ibadah umroh.
<h4> Harga Transparan </h4>
PilihUMROH.com menjamin bahwa harga paket yang tercantum di website adalah harga total (“Harga Transparan”) dari seluruh komponen biaya perjalanan umroh. 

<h4>Mudah dan Praktis </h4>
Mencari dan memesan paket umroh di PilihUMROH.com sangat praktis dan mudah, Anda dapat membandingkan berbagai paket umroh hanya dengan satu, dan Anda bisa langsung melakukan pendaftaran secara online. Call Center kami pun siap membantu Anda selama 24 jam

                                                            
                                                        </p>
						</div>
						
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
                                        <div class="ust-detail-wrap">
						<div class="faq-isi hvr-grow">
							<h3>Harga dan Promosi</h3>
                                                        <p>
                                                        <h4>   1.	Berapa biaya transaksi atau transfer fee di PilihUMROH.com? </h4>
Biaya transfer/transfer fee mengikuti ketentuan transfer antar bank yang berlaku di Indonesia.
<h4>2.	Apakah yang tertera sudah harga all in/harga total? adakah biaya tambahan setelahnya? </h4>
Harga yang tertera adalah harga paket yang di sediakan oleh travel penyedia jasa perjalanan umroh, adapun informasi tentang biaya tambahan yang dicantumkan dalam list “Tidak Termasuk”

<h4>3.	Apakah sewaktu-waktu harga paket umroh dapat berubah? </h4>
Penentuan harga sepenuhnya merupakan hak dari masing-masing travel penyedia jasa perjalanan umroh, terkadang harga dapat berubah dikarenakan perubahan nilai tukar mata uang

                                                        </p>
						</div>
						
						<div class="clearfix"></div>
					</div>
                                        <div class="clearfix"></div>
                                        <div class="ust-detail-wrap">
						<div class="faq-isi hvr-grow">
							<h3>Cara memesan dan membayar paket</h3>
                                                        <p>
                                                        <h4>     1.	Cari paket umroh anda </h4>
Mulai pencarian anda dengan mengisi rincian di kolom pencarian paket umroh yang tertera di halaman beranda, atau anda dapat memilih Paket Unggulan yang juga ada di halaman beranda.
<h4> 2.	Pilih dan Pesan Paket Umroh </h4>
Anda dapat membandingkan paket, jadwal, dan harga yang sesuai dengan rencana perjalanan Anda, lalu Anda dapat langsung pilih paket yang sesuai.
<h4>3.	Isi Informasi Kontak dan Detail Jamaah </h4>
Setelah memilih paket umroh Anda, Anda dapat login dan mengisi formulir pendaftaran.
<h4>4.	Selesaikan Pembayaran Anda </h4>
Anda akan mendapatkan kode pembayaran yang selanjutnya dapat dilakukan pembayaran melalui transfer via ATM/Mobile Banking/Setoran Bank. Pastikan Anda hanya melakukan transfer ke rekening beratas namakan PT PILIH UMROH AMANAH.
<h4>5.	Menerima E-Ticket anda </h4>
Setelah pembayaran Anda di konfirmasi, kami akan mengirimkan e-ticket Anda ke email Anda. Anda pun dapat langsung mengakses histori transaksi di PilihUMROH.com 

                                                        </p>
						</div>
						
						<div class="clearfix"></div>
					</div>
                                        <div class="clearfix"></div>
                                        <div class="ust-detail-wrap">
						<div class="faq-isi hvr-grow">
							<h3>Pembatalan/Perubahan Data</h3>
                                                        <p>
                                                        <h4> 1.	Bagaimana cara membatalkan paket umroh yang sudah saya pesan? </h4>
Anda dapat melakukan pembatalan paket umroh dengan menghubungi Call Center PilihUMROH.com dengan tetap memperhatikan syarat dan ketentuan pembatalan paket umroh dari travel penyedia jasa perjalanan umroh yang bersangkutan
<h4>2.	Bagaimana cara menghubungi PilihUMROH.com? </h4>
Anda dapat menghubungi PilihUMROH.com melalui berbagai jalur komunikasi resmi, untuk lebih lengkapnya dapat dilihat di halaman Hubungi Kami

   
                                                        </p>
						</div>
						
						<div class="clearfix"></div>
					</div>
                                        <div class="clearfix"></div>
                                        
				</div>
				
			 	<div class="clearfix"></div>
			</div>
		</div>
	</div><!--end detail-wrap-->

                </div>
	</div><!--end home-wrap-->

@endsection